*****************************************************************************
# class V5Client

V5Client のFACADE クラス。  
View 側への API を用意する。

##Public member types

| Member Type                       | Definition                             |
|-----------------------------------|----------------------------------------|
| IChatEventSink                    | IV5ChatEventSink                       |
| Participant                       | V5Participant                          |
| ParticipantList                   | V5ParticipantList                      |



##V5Client::V5Initialie

`V5CODE V5Inialize(V5WindowId parentWindo, V5Data* v5Data, const char* logPath, const char* logPriority, V5Rect const* videoRect);`

###引数

####parentWindow
Video をレンダリングするウィンドウのハンドルを渡します。  
ハンドルは各 OS のウィンドウハンドル表現を渡します（例：Win32:HWND / Cocoa:UIWindow*)。

####v5Data

####logPath
ログを保存するファイルパスを指定します。

####logPriority


####videoRect

parentWindow 上のどの位置に Video をレンダリングするかを指定します。  
null を指定するとレンダリングは行われません。

###戻り値
エラーコード。詳細は未定。

##V5Client::V5JoinConference

`V5CODE V5JoinConference(const char* token, const char* vcubeURL);`

###引数
####token

####vcubeURL
Vcube ポータルのURLを指定します。

###戻り値
エラーコード。詳細は未定。

##V5Client::getParticipants
`ParticipantList const& getParticipants();`

###戻り値
参加者リスト。
リスト中には自身を含まない。


##V5Client::getSelfParticipant
`Participant const& getSelfParticipant();`

###戻り値
自身の参加者情報


##V5Client::getCameraMuted
`bool getCameraMuted();`

###戻り値
カメラのミュート状態。

##V5Client::getMicrophoneMuted
`bool getMicrophoneMuted();`

###戻り値
マイクのミュート状態。

##V5Client::getSpeakerMuted
`bool getSpeakerMuted();`

###戻り値
スピーカーのミュート状態。

##V5Client::setCameraMuted
`void setCameraMuted(bool muted);`

###引数
####muted
設定するミュート状態。真の場合、ミュート。

###例外
詳細は未定。

##V5Client::setMicrophoneMuted
`void setMicrophoneMuted(bool muted);`

###引数
####muted
設定するミュート状態。真の場合、ミュート。

###例外
詳細は未定。

##V5Client::setSpeakerMuted
`void setSpeakerMuted(bool muted);`

###引数
####muted
設定するミュート状態。真の場合、ミュート。

###例外
詳細は未定。

##V5Client::sendChatMessage
`void sendChatMessage(const char * message);`

ルームチャットへの発言を行います。
同一ルーム内の全員へ通知されます。

###引数
####message
発言内容。
名前や発言時刻を不可する必要ありません。

###例外
詳細は未定。

##V5Client::setChatEventSink
`void setChatEventSink(IChatEventSink * sink);`

チャットイベントのリスナーを登録します。  

このメソッドは前回の設定値を上書きします。

###引数
####sink
リスナー。
型は std::shared_ptr<IChatEventSink> のほうがよいかもしれない・・・。


###例外
詳細は未定。


*****************************************************************************
# class IV5ChatEventSink

ルームチャットのリスナー Interface です。  

##Public member types

| Member Type                       | Definition                             |
|-----------------------------------|----------------------------------------|
| ChatMessage                       | V5ChatMessage                          |

##IV5ChatEventSink::~IV5ChatEventSink
`virtual void ~IV5ChatEventSink();`

リスナーが消滅した際に呼ばれます。  

##IV5ChatEventSink::onChatMessageAdded
`virtual void onChatMessageAdded(const ChatMessage messages[], std::size_t length) = 0;`

ルームチャットへの発言通知を受信します。
自身の発言も通知されます。

###引数
####messages
新規に追加された発言内容の配列。

####length
新規に追加された発言内容の配列。


##IV5ChatEventSink::onClearChatMessages
`virtual void onClearChatMessages() = 0;`

ルームチャットの内容がクリアされた旨、通知を受信します。


*****************************************************************************
# struct V5ChatMessage

ルームチャットのリスナー Interface です。  

##Public member properties

| Member Name      | Type                        | Description                  |
|------------------|-----------------------------|------------------------------|
| text             | const char *                | 発言内容の文字列               |
| from             | const char *                | 発言者の参加者 ID             |
| timestamp        | long long                   | 発言時刻。GMT の UNIXTIME     |
| isSelfMessage    | bool                        | 自分自身による発言であるか       |

*****************************************************************************
# struct V5Participant

ルームへの参加者を表現します  

##Public member properties

| Member Name      | Type                        | Description                  |
|------------------|-----------------------------|------------------------------|
| id               | const char *                | 参加者 ID                     |
| displayName      | const char *                | 参加者の表示名                |

*****************************************************************************
# typedefs
`typedef std::vector<V5Participant> V5ParticipantList;`

| Type Name                         | Definition                             |
|-----------------------------------|----------------------------------------|
| V5ParticipantList                 | std::vector&lt;V5Participant&gt;       |


*****************************************************************************


















*****************************************************************************
