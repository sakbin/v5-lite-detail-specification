# MTG5 WEB Portal Setup Manual

### WEB Portalのセットアップ手順書

### 更新履歴
日付           | 更新者         | 内容
------------- | ------------- | -------------
2016/04/20    | 濱名           | 新規作成

## WEB Portal設定 (仮想マシン名:v5-portal)
### 1. WEB PortalサーバにSSH接続  
ssh -l account WEBProtalDowain   
作業ユーザーに変更  
\$ sudo su - mtg-user  
以下のパッケージが設置しているディレクトリに移動  
\$ cd /home/project/v5Portal/config/  
・設定ファイルの変更  
\$ vi config.inc.php  
以下の箇所を変更

```
define('N2MY_BASE_URL',      'http://WEB Domain or IP/');
define('N2MY_LOCAL_URL',     'http://WEB Domain or IP/');
```

ConferenceサーバとSSL接続する場合は以下を変更

```
#---------
# コンファレンスサーバ設定
#---------
#通信セキュアフラッグ
define("CONFERENCE_SERVER_SECURE_FLAG", 1);
```

デフォルトでは0になっている

### 2. ブラウザからadmin_toolに入り設定変更
URL : http://WEBProtalDowain/admin_tool/
ID : onpre_admin
PW : SIのパスワード
※ログイン後は案件ごとにパスワードの再設定を実施してください

### 3. Conferenceサーバ設定
Account Database Management -> Conference Server List  
conference server key 1Modifyを押下し、設定を変更する  
![Vidyo Router Login](images/v5-portal-cf-setting-01.png)
conference server address : 設定を今回のConferenceサーバのFQDNに変更
例) v5-cf.vcube.net  
(プロトコルは含まない)  
![Vidyo Router Login](images/v5-portal-cf-setting-02.png)
変更が反映されたことを確認する
![Vidyo Router Login](images/v5-portal-cf-setting-03.png)

### 4. Vidyo Portal設定
Account Database Management -> Vidyo Portal List  
Modifyを押下し、設定を変更する  
![Vidyo Router Login](images/v5-portal-vp-setting-01.png)  
vidyo portal address : 設定を今回のVidyo PortalのFQDNに変更  
例) http://v5-vp-test.vcube.net  
(プロトコルから指定してください)  
vidyo admin id : V5OnprePortal  
vidyo admin pw : Admin PWをVidyo Portalで実際に設定した値に変更  
![Vidyo Router Login](images/v5-portal-vp-setting-02.png)
変更が反映されたことを確認する
![Vidyo Router Login](images/v5-portal-vp-setting-03.png)

### 5. Video Manager設定 ※録画を利用する場合
Account Database Management -> Video Server List
![Vidyo Router Login](images/v5-portal-video-setting-01.png)
vidyo server key 1 のModifyを押下  
video server address : 今回利用するVideo ManagerのFQDNを入力  
例) http://v5-videomgr.vcube.net  
(プロトコルから指定してください)  
video server ip : Video ManagerのIP  
auth account id : onpre  
auth account pw : 任意のパスワード (Vidyo Manager側のConfigファイルにも設定箇所あり)  
![Vidyo Router Login](images/v5-portal-video-setting-02.png)
変更が反映されたことを確認する
![Vidyo Router Login](images/v5-portal-video-setting-03.png)

### 6. Vidyo Proxy List設定※SSL利用の場合のみ  
Account Database Management -> Vidyo Proxy List
![Vidyo Router Login](images/v5-portal-proxy-setting-01.png)
Entryタブを押下  
country code : 国コード (例 : jp)
city code : 都市コード (例 : tyo)
vidyo locationTag : Vidyo Portalで設定したTag (例 : TYO)
vidyo proxy name	: Vidyo Proxyで登録したName (例 : TYO_PROXY_001)
![Vidyo Router Login](images/v5-portal-proxy-setting-02.png)
変更が反映されたことを確認する
![Vidyo Router Login](images/v5-portal-proxy-setting-03.png)

### 7. Vidyo Pool登録
※この作業はVidyoサーバ側の設定も完了した後で実行してください。
連携用のVidyo UserとVidyo Roomの登録を行います。
#### WebサーバにSSH接続
ssh -l account WEBProtalDowain   
作業ユーザーに変更  
\$ sudo su - mtg-user  
以下のパッケージが設置しているディレクトリに移動  
\$ cd /home/project/v5Portal/
ユーザーのPool作成
\$ php bin/v5_vidyo_pool_add.php add_user
一回の実行で25ユーザーVidyoPortal側に発行されます
\$ php bin/v5_vidyo_pool_add.php add_room
一回の実行で25部屋VidyoPortal側に発行されます
利用数に併せて実行お願いします。


## Conference設定 (仮想マシン名:v5-conference)
### 1. ConferenceサーバにSSH接続  
ssh -l account ConferenceDowain   

### 2. Conferenceサーバに設定変更
作業ユーザーに変更  
\$ sudo su - node-user  

### 3. Conferenceサーバが起動しているかの確認

```
	# pm2 list	> ┌──────────┬────┬──────┬──────┬────────┬───────────┬────────┬─────────────┬─────────────┐	> │ App name │ id │ mode │ PID  │ status │ restarted │ uptime │      memory │    watching │	> ├──────────┼────┼──────┼──────┼────────┼───────────┼────────┼─────────────┼─────────────┤	> │ main     │ 1  │ fork │ 3385 │ online │         0 │ 0s     │ 19.656 MB   │ unactivated │	> └──────────┴────┴──────┴──────┴────────┴───────────┴────────┴─────────────┴─────────────┘	>  Use `pm2 desc[ribe] <id>` to get more details

```
mainが起動している場合は以下を実行し、一旦停止する

プロセス停止

```
	# pm2 stop main	> [PM2] Stopping main	> [PM2] stopProcessId process id 1	> ┌──────────┬────┬──────┬──────┬─────────┬───────────┬────────┬────────┬─────────────┐	> │ App name │ id │ mode │ PID  │ status  │ restarted │ uptime │ memory │    watching │	> ├──────────┼────┼──────┼──────┼─────────┼───────────┼────────┼────────┼─────────────┤	> │ main     │ 1  │ fork │ 5550 │ stopped │         0 │ 0      │ 0 B    │ unactivated │	> └──────────┴────┴──────┴──────┴─────────┴───────────┴────────┴────────┴─────────────┘	>  Use `pm2 desc[ribe] <id>` to get more details
```

プロセス削除

```
	# pm2 delete main	> [PM2] Deleting main process	> [PM2] deleteProcessId process id 1	> ┌──────────┬────┬──────┬─────┬────────┬───────────┬────────┬────────┬──────────┐	> │ App name │ id │ mode │ PID │ status │ restarted │ uptime │ memory │ watching │	> └──────────┴────┴──────┴─────┴────────┴───────────┴────────┴────────┴──────────┘	>  Use `pm2 desc[ribe] <id>` to get more details```


### 4. 設定ファイルの変更  
以下のパッケージが設置しているディレクトリに移動  
\$ cd /home/project/v5Conference/config/
\$ vi local.json  
以下の箇所を変更  
conference箇所
Port : 80 or SSL利用時は443  
addr : ConferenceサーバのFQDN  
SSL : OFF or SSL利用時はON  

redis設定情報
別サーバを利用の際には、
addr : redisサーバのIP
基本は変更なし

portal箇所
addr : WEBサーバのFQDN  

```
        // conference設定情報
        "conference": {
                "port": 80,                                                    // conference-ポート
                "addr": "v5-cf.vcube.net", // conference-アドレス

                // 認証キー
                "secret" : {
                        "key"  : "/etc/ssl/private/customer.key",,
                        "ca"   : "/etc/ssl/certs/customer.ca-bundle",
                        "cert" : "/etc/ssl/certs/customer.pem"
                },

                // 認証切り替えフラグ
                "SSL": "OFF"     // "ON":SSL認証あり "OFF":SSL認証なし
        },

        // cluster設定情報
        "cluster": {
                "start": ""     // worker起動数(手動調整用)
        },

        // redis設定情報
        "redis": {
                "master": {
                        "port": 6379,                   // redis-Master-ポート
                        "addr": "127.0.0.1" // redis-Master-アドレス
                },
                "slave": {
                        "port": 6379,                   // redis-Slave-ポート
                        "addr": "127.0.0.1" // redis-Slave-アドレス
                }
        },

        // portal設定情報
        "portal": {
                "addr": "v5-web.vcube.net"   // v5Portal-アドレス
        },
```

### 5. Conferenceサーバ起動

```
	# pm2 start bin/main.js	> [PM2] Process bin/main.js launched	> ┌──────────┬────┬──────┬──────┬────────┬───────────┬────────┬─────────────┬─────────────┐	> │ App name │ id │ mode │ PID  │ status │ restarted │ uptime │      memory │    watching │	> ├──────────┼────┼──────┼──────┼────────┼───────────┼────────┼─────────────┼─────────────┤	> │ main     │ 1  │ fork │ 3385 │ online │         0 │ 0s     │ 19.656 MB   │ unactivated │	> └──────────┴────┴──────┴──────┴────────┴───────────┴────────┴─────────────┴─────────────┘	>  Use `pm2 desc[ribe] <id>` to get more details
```

### 6. Conferenceサーバが起動した事を確認
pm2 listの確認


```
	# pm2 list	> ┌──────────┬────┬──────┬──────┬────────┬───────────┬────────┬─────────────┬─────────────┐	> │ App name │ id │ mode │ PID  │ status │ restarted │ uptime │      memory │    watching │	> ├──────────┼────┼──────┼──────┼────────┼───────────┼────────┼─────────────┼─────────────┤	> │ main     │ 1  │ fork │ 3385 │ online │         0 │ 0s     │ 19.656 MB   │ unactivated │	> └──────────┴────┴──────┴──────┴────────┴───────────┴────────┴─────────────┴─────────────┘	>  Use `pm2 desc[ribe] <id>` to get more details
 以下の内容を確認
	"status"が"online"となっていること
	"restarted"の数が増えていない事
```

logを確認

```
[DEBUG] node - portal send [action_Register]
[DEBUG] node - URL:http://v5-portal.vcube.net/api/v5lite/conference/
[INFO] node - portal receive [action_Register] SUCCESS
[DEBUG] node - -----------------------------
[DEBUG] node - { result: 'OK', detail: { status: '200', description: '' } }
[DEBUG] node - -----------------------------
```

起動時にWeb Portalのadmin_toolでconferenceサーバの設定が完了していない場合は、  
logs/node-datetime.logで以下のエラー  

```
 [DEBUG] node - { result: 'ERROR', detail: { status: 100, description: '' }, serverAddress: 'INVALID' }
```

が発生するので、先にadmin_toolでの設定を行う
設定済みでも同じエラーが出る場合は、admin_toolの設定に間違えがあるか、hostsでの設定、local.jsonでの設定が間違えている可能性があるので再度確認

## Video Manager設定 (仮想マシン名:v5-portal)
### 1. Video Manager (WEB Portalサーバと同じ)にSSH接続  
ssh -l account VideoManagerDomain (またはWEBProtalDowain)   
作業ユーザーに変更  
\$ sudo su - mtg-user  
以下のパッケージが設置しているディレクトリに移動  
\$ cd /home/project/v5VideoMgr/config/  
・設定ファイルの変更  
\$ vi config.inc.php  
以下の箇所を変更

```
# アプリケーションドメインアドレス
define('VIDEO_LOCAL_URL', 'http://v5-videomgr.vcube.net/');
# アプリケーションIPアドレス(hostsにより)
define('VIDEO_SERVER_IP', '172.16.0.108');
```

### 2. ブラウザからadmin_toolに入り設定変更　※※※以下からは調整中※※※
URL : http://VideoManagerDowain/admin_tool/  
ID : onpre_admin  
PW : SIのパスワード  
※ログイン後は案件ごとにパスワードの再設定を実施してください

### 3. Vidyo Portal設定
Account Database Management -> Vidyo Portal List  
Modifyを押下し、設定を変更する  
![Vidyo Router Login](images/v5-videomanager-vp-setting-01.png)  
vidyo portal address : 設定を今回のVidyo PortalのFQDNに変更  
例) http://v5-vp-test.vcube.net  
(プロトコルから指定してください)  
vidyo admin id : V5OnprePortal  
vidyo admin pw : Admin PWをVidyo Portalで実際に設定した値に変更  
![Vidyo Router Login](images/v5-videomanager-vp-setting-02.png)
変更が反映されたことを確認する
![Vidyo Router Login](images/v5-videomanager-vp-setting-03.png)

### 4. Vidyo Record Portal設定
Account Database Management -> Vidyo Portal List  
Modifyを押下し、設定を変更する  
![Vidyo Router Login](images/v5-videomanager-rp-setting-01.png)  
vidyo portal address : 設定を今回のVidyo ReplayのFQDNに変更  
例) http://v5-vp-test.vcube.net  
(プロトコルから指定してください)  
vidyo admin id : V5OnprePortal  
vidyo admin pw : Admin PWをVidyo Portalで実際に設定した値に変更  
![Vidyo Router Login](images/v5-videomanager-rp-setting-02.png)
変更が反映されたことを確認する
![Vidyo Router Login](images/v5-videomanager-rp-setting-03.png)

※複数のReplayサーバを利用する場合はEntryタブより、追加する

### 5. Vidyo Recorder設定
※複数のReplayサーバを利用する場合のみ追加作業が必要

### 6. Web Portal設定
Account Database Management -> Web Portal List  
Modifyを押下し、設定を変更する  
![Vidyo Router Login](images/v5-videomanager-portal-setting-01.png)  
vidyo portal address : 設定を今回のVidyo PortalのFQDNに変更  
例) http://v5-vp-test.vcube.net  
(プロトコルから指定してください)  
vidyo admin id : V5OnprePortal  
vidyo admin pw : Admin PWをVidyo Portalで実際に設定した値に変更  
![Vidyo Router Login](images/v5-videomanager-portal-setting-02.png)
変更が反映されたことを確認する
![Vidyo Router Login](images/v5-videomanager-portal-setting-03.png)