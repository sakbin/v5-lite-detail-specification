# V-cube Meeting Version 5 On-Premise Server Infrastructure Configuration Manual

- Manual version: 2016-0200-0001
- Package version: 2016-0200
- to@ctu.vcube.co.jp

--------------------------------------------------------------------------------

## Overview

V-cube Meeting Version 5 (Meeting5) is based on Vidyo(r) software appliances and LAPP(Linux, Apache, PHP, PostgreSQL) Servers. Vidyo software appliances are provided as VMware(r) compatible Virtual Machine(VM) images. To deploy all functions on single (or multi) VMware ESXi Hypervisor (ESXi) server(s), we made LAPP VM's.

This manual is step-by-step instructions to configure Meeting5 On-Premise (Meeting5OnPremise) server(s).

## What's new in 2016-0200

- Update VidyoReplay version 3.0.0 to 3.0.1 to improve stability

## System requirements

### Hardware

Hardware requirement is based on customer's requirement. See:Meeting5 On-premise Server requirement.

### Hypervisor

Since Vidyo software are bring as VMware compatible VM image, necessary to use VMware ESXi based environment. Vidyo's requirement is "VMware vSphere ESXi Hypervisor software version 5.0 or later", however we determine to use ESXi 5.5 or later. If your server vendor is DELL, you should use ESXi DELL customized image. (<http://www.dell.com/support/home/us/en/19/Drivers/DriversDetails?driverId=YGH6K>)

### Software (VM images)

- Web Portal Server VM image (v5-portal-2016-0202.ova)
- Conference Server VM image (v5-conference-2016-0200.ova)
- VidyoPortal (v5-vidyoportal-2016-0200.ova)
- VidyoRouter (v5-vidyorouter-2016-0200.ova)
- VidyoReplay (v5-vidyoreplay-2016-0200.ova)

## System configuration

### Basic configuration

Server               | OS(Image)         | Major software                            | Notes
-------------------- | ----------------- | ----------------------------------------- | -------------------------------------------------------------------------------------------------
Web Portal Server    | Ubuntu 14.04LTS   | Apache HTTP Server, PostgreSQL, Memcached |
Video Manager Server |                   |                                           | *Optional implementation for recording feature, deployed as Web Portal Server's HTTPd VirtualHost
Conference Server    | Ubuntu 14.04LTS   | Node, Redis                               |
VidyoPortal          | VidyoPortal Image | -                                         |
VidyoRouter          | VidyoRouter Image | -                                         |
VidyoReplay          | VidyoReplay Image | -                                         | *Optional VM for recording feature

## Configuring the server

### Configuring ESXi and deploying VM images

1. Connect your private network to the **oldest** number of the server physical port for configuration
2. Install ESXi on your server
3. Configure hostname of ESXi

  1. Press F2 and login to System Customization menu as root via **ESXi Server's console**
  2. System Customization -> Configure Management Network -> DNS Configuration
  3. Tick "Use the following DNS server addresses and hostname"
  4. Delete the value of Primary DNS Server/Alternate DNS Server
  5. Input Hostname that as directed by configuration sheet, and press Enter
  6. Press y on "Configure Management Network: Confirm"
  7. Press ESC to exit System Customization menu

4. Configure vSwitch

  1. Login as root (or privileged user) via **vSphere Client**
  2. Configuration -> Networking
  3. Remove "VM Network (Virtual Machine Port Group)" from vSwitch0 if exist. (vSwitch0 -> Properties..)
  4. Create new vSwitch (vSwitch1) which connect with the **youngest** number of the server physical port and attach "Virtual Machine Port Group".

    1. Add Networking...
    2. Connection Types: Virtual Machine
    3. Choose vmnic0 (This logical interface may be binded the youngest number of physical port of your server)
    4. Change Network Label "VM Network" to "Service Network"
    5. Finish
    6. Connect your network to the youngest number of physical port of your server

5. Configure Date and Time

  1. Login as root (or privileged user) via **vSphere Client**
  2. Configuration -> Time Configuration -> click **Properties...**
  3. Set current date and time manually and click **OK**.

6. Deploy all VM images (OVA files) on ESXi connect with vSwitch1.

  - If FQDN based configuration, input FQDN(_package_version_) as VM Name. (e.g. `meeting.example.com(2016-0200)`)
  - Disk Format must be **"Thin Provision"**. ![ESXi Networking](./images/esxi-networking.png)

7. Configuring VM Startup automatically

  1. Login as root (or privileged user) via **vSphere Client**
  2. Configuration -> Virtual Machine Startup/Shutdown -> click **Properties...**
  3. Tick **Allow virtual machines to start and stop automatically with the system**
  4. Input "5" to **Default Startup Delay** and tick **Continue immediately if the VMware Tools start**
  5. Input "5" to **Default Shutdown Delay** and Choose Shutdown Action **Guest Shutdown**.
  6. In Startup Order section, move all VMs Manual Startup to Any Order. Depends on entire system configuration, VidyoPortal must startup at first and VidyoRouter and VidyoReplay at last. ![ESXi Virtual Machines Startup and Shutdown](./images/esxi-vm-startup-and-shutdown.png)

### Common settings for Web Portal and Conference server

#### Changing IP address and DNS nameserver

1. Log in to the Console using "vadmin" account.
2. Get root privillage. `$ sudo su -`
3. Edit /etc/network/interfaces. `# vim /etc/network/interfaces` (Change address, netmask, gateway and dns-nameservers)and apply settings `# ifdown eth0 & ifup eth0`

```
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet static
      address 172.16.0.96
      netmask 255.255.254.0
      gateway 172.16.1.254
      dns-nameservers 172.16.0.5 172.16.0.6
```

#### Configuring NTP daemon (If customer specify NTP server(s)) and timezone

1. Edit ntp.conf `# vim /etc/ntp.conf`

  - Change "server" lines. If customer does not have 4 NTP servers, comment-out it.
```
server ntp0.customer.example.com
server ntp1.customer.example.com
#server 2.ubuntu.pool.ntp.org
#server 3.ubuntu.pool.ntp.org
```

Notes: If the server can reach global address with name resolution, you do not need to change ntpd.conf.

1. Use `# dpkg-reconfigure tzdata` to change timezone setting of the server.

2. (ONLY Web Portal server) Edit php.ini to change timezone settings of php. `# vim /etc/php5/apache2/php.ini`
```
[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
date.timezone = Asia/Tokyo
```

#### Configuring hostname(FQDN) and hosts file

All Inter-Server communications (VM to VM) on Meeting5OnPremise are FQDN based, **regardless of customer (client node) use IP address to connect Meeting 5 On-Premise server**.

- Edit /etc/hostname `# vim /etc/hostname` and write hostname(**Do not enter FQDN**)
```
hostname
```

- Edit /etc/hosts `# vim /etc/hosts`

  1. Comment out the line start from '127.0.1.1, and insert ip address, FQDN and hostname of the host you are configuring.
```
#127.0.1.1       v5-portal.vcube.net v5-portal
ipaddress_of_host    FQDN_of_host    hostname_of_host
```

- Append all host to hosts file **EXCLUDE the host you are configuring**.
```
Web_Portal_IP_address     Web_Portal_IP_FQDN
VideoManager_IP_address      VideoManager_IP_FQDN  # Optional
Conference_IP_address      Conference_IP_FQDN
Vidyo_Portal_IP_address      Vidyo_Portal_IP_FQDN
Vidyo_Router_IP_address      Vidyo_Router_IP_FQDN
Vidyo_Replay_IP_address      Vidyo_Router_IP_FQDN  # Optional
```

NOTE: on Conference server, you do not need to append all host **but Web Portal**.

### Web Portal Server

#### Configuring HTTPd server

This Web Portal VM is based on Utuntu 14.04LTS, thus all configurations (e.g. web server's configuration) are written in Ubuntu style (For more information, See: <https://help.ubuntu.com/lts/serverguide/httpd.html>). Web Portal offers the web interface for end-users and controls conference records and so on. If customer use SSL, we should install server certificate issued by public CA.

1. Install SSL certificate. (Optional)

  1. Store server certificate file (SSLCertificateFile; server CA) as arbitrary name to `/etc/ssl/certs/`. (e.g. `/etc/ssl/certs/customername_YYYY.pem`)
  2. Make symlink to the server certificate file as /etc/ssl/certs/customer.pem.<br>
    `# ln -s /etc/ssl/certs/customername_YYYY.pem /etc/ssl/certs/customer.pem`
  3. Store private key File (SSLCertificateKeyFile; privete key) as arbitrary name to `/etc/ssl/private/` with **non-passphrase**. (e.g. `/etc/ssl/private/customername_YYYY.key`)
  4. Make symlink to the private key file as /etc/ssl/private/customer.key.<br>
    `# ln -s /etc/ssl/private/customername_YYYY.key /etc/ssl/private/customer.key`
  5. Store certificate chain file (SSLCertificateChainFile; intermediate CA) as arbitrary name to `/etc/apache2/ssl.crt/`. (e.g. `/etc/apache2/ssl.crt/customername_YYYY.ca-bundle`)
  6. Make symlink to the private key file as /etc/apache2/ssl.crt/customer.ca-bundle.<br>
    `# ln -s /etc/apache2/ssl.crt/customername_YYYY.ca-bundle /etc/apache2/ssl.crt/customer.ca-bundle`

2. Edit VirtualHost configuration files.

  1. Change **ServerName** directive for FQDN of Web Portal Server. (If needed, also change ServerAlias) `# vim /etc/apache2/sites-available/v5-portal.local.conf`
```
#### CHANGE HERE ####
ServerName v5-portal.vcube.net
#### CHANGE HERE ####
```

3. (Optional) if SSL connectivity needed, change **ServerName** directive for FQDN of Web Portal Server. (If needed, also change ServerAlias) `# vim /etc/apache2/sites-available/v5-portal.local-ssl.conf`
```
#### CHANGE HERE ####
ServerName v5-portal.vcube.net
#### CHANGE HERE ####
```

4. Enable configuration both v5-portal.local and (optional) v5-portal.local-ssl.
```
# a2ensite v5portal.local v5portal.local-ssl
```

5. Reboot the server (to apply all configurations on the Web Portal Server).

#### Configuring Video Manager (Optional)

1. Edit VirtualHost configuration files.

  1. Change **ServerName** directive for FQDN of Video Manager Server. (If needed, also change ServerAlias) `# vim /etc/apache2/sites-available/v5-videomgr.local.conf`
```
#### CHANGE HERE ####
ServerName v5-videomgr.vcube.net
#### CHANGE HERE ####
```

2. (Optional) if SSL connectivity needed, change **ServerName** directive for FQDN of Video Manager Server. (If needed, also change ServerAlias) `# vim /etc/apache2/sites-available/v5-videomgr.local-ssl.conf`
```
#### CHANGE HERE ####
ServerName v5-videomgr.vcube.net
#### CHANGE HERE ####
```

3. Enable configuration both v5-videomgr.local and (optional) v5-videomgr.local-ssl.
```
# a2ensite v5-videomgr.local.conf v5-videomgr.local-ssl.conf
```

4. Reboot the server (to apply all configurations on the Web Portal Server).

### Conference Server

1. Install SSL certificate. (Optional)

  1. Store server certificate file (SSLCertificateFile; server CA) as arbitrary name to `/etc/ssl/certs/`. (e.g. `/etc/ssl/certs/customername_YYYY.pem`)
  2. Make symlink to the server certificate file as /etc/ssl/certs/customer.pem.
    `# ln -s /etc/ssl/certs/customername_YYYY.pem /etc/ssl/certs/customer.pem`
  3. Store private key File (SSLCertificateKeyFile; privete key) as arbitrary name to `/etc/ssl/private/` with **non-passphrase**. (e.g. `/etc/ssl/private/customername_YYYY.key`)
  4. Change owner and permission.
```
# chmod 640 /etc/ssl/private/customername_YYYY.key
# chgrp ssl-cert /etc/ssl/private/customername_YYYY.key
```

  5. Make symlink to the private key file as /etc/ssl/private/customer.key.
    `# ln -s /etc/ssl/private/customername_YYYY.key /etc/ssl/private/customer.key`

  6. Store certificate chain file (SSLCertificateChainFile; intermediate CA) as arbitrary name to `/etc/ssl/certs/`. (e.g. `/etc/ssl/certs/customername_YYYY.ca-bundle`)

  7. Make symlink to the private key file as /etc/ssl/certs/customer.ca-bundle.
    `# ln -s /etc/ssl/certs/customername_YYYY.ca-bundle /etc/ssl/certs/customer.ca-bundle`

2. Edit node.js configuration file(/home/project/v5Conference/config/local.json). (For more details refer to "MTG5 WEB Portal Setup Manual")

### VidyoPortal

- Changing IP address

  1. Log in to the System Console using "admin" account via console on vSphere Client.
  2. Select 1\. Configure IP Address.
  3. Select the PRODUCTION INTERFACE.
  4. Select IPv4 (Static) to set the server IP address, subnet mask, and default gateway, hostname, domain name, and FQDN. Press Enter after providing each value.
  5. Once you have entered the required information, select y and press Enter.
  6. Press x to back main menu.
  7. Select 14\. Reboot system.

- Configure hosts and DNS Nameserver

  1. Log in to the System Console using "admin" account via console on vSphere Client.
  2. Enter m for more options.
  3. Enter A for Advanced Options.
  4. Enter 5 for Hostname Management.

    1. Enter 1.
    2. Enter FQDN.
    3. Enter IP address.
    4. Enter y
    5. Continue to add **ALL VidyoRouter and VidyoReplay**.
```
1>  10.0.0.2           vidyorouter01.example.com
2>  10.0.0.3           vidyorouter02.example.com
3>  10.0.0.4           vidyorouter03.example.com
4>  10.0.0.5           vidyoreplay01.example.com
5>  10.0.0.6           vidyoreplay02.example.com
```

    6. Enter X to exit Hostname Management.

    7. Enter x to exit Advanced Options.

    8. Enter b to back main menu.

  5. Select 2 Configure DNS Nameserver.

  6. Enter two DNS server IP addresses. If you have only one DNS server, use the same one twice.

  7. Once you have entered the required information, select y and press Enter.The System Console main menu appears.

  8. Select 14\. Reboot system.

- Configure NTP time server and timezone

  1. Select 3\. Configure NTP Time Servers to set the NTP (Network Time Protocol) time server.
  2. Select 14\. Reboot system.
  3. Select 4\. Configure Time Zone to specify the time zone you are working in.
  4. Select 14\. Reboot system.

- Applying System License Keys to VidyoPortal

  1. Logging in to the Super Admin Portal using web browser. `http://[IP or FQDN address]/super`
  2. Click the Settings tab.
  3. Click System License on the left menu.
  4. Click the Select File icon, which is located to the right of the Upload System License File field.
  5. Choose your unzipped v3 license file.
  6. Click Upload to apply the VidyoManager license.

- Install SSL certificate. (Optional)

  1. Importing an SSL Private Key

    1. Logging in to the Super Admin Portal using web browser. `http://[IP or FQDN address]/super`
    2. Click the Settings tab.
    3. Click Security on the left menu.
    4. Click the SSL Private Key tab.
    5. Click Import **Private Key**.
    6. In the Select Private Key dialog box, click the Select File icon.
    7. In the System File Selection dialog box, locate and select your private key file.
    8. Click Open.
    9. In the Select Private Key dialog box, enter your password in the Password field.
    10. Click **Upload**. (If the upload is successful, the File Upload Success dialog box displays. A hash of the private key you imported displays in the **SHA256** field.)

  2. Deploying the Server Certificate

    1. Click the Server Certificate tab.
    2. Click Upload.
    3. In the Select Server Certificate dialog box, click the Select File icon.
    4. Select your server certificate file on your computer and click **Upload**.

  3. Deploying Your Server CA Certificates (Intermediates)

    1. Click the Server CA Certificates tab.
    2. Click Upload.
    3. In the Select Server CA Certificate dialog box, click Select File icon.
    4. Select your server CA certificate file on your computer (may also be referred to as the Intermediate Certificate by your Certificate Authority) or local network and click **Upload**.

  4. Importing Client Root CA Certificates

    1. Click the Advanced tab.
    2. Click **Import Client CA Certificates....**
    3. In the Select file dialog box, choose **Append to existing**.
    4. Click the Select File icon to locate the **CA Certificate** file on your computer.
    5. Click **Upload** to upload the CA certificate file.
    6. Click **Yes** to reboot server.

  5. Enable SSL

    1. Logging in to the Super Admin Portal using web browser. `http://[IP or FQDN address]/super`
    2. Click the Settings tab.
    3. Click Security on the left menu.
    4. Click **Enable SSL** and click **Yes** and **OK**.

### VidyoRouter

- Changing IP address

  1. Log in to the System Console using "admin" account via console on vSphere Client.
  2. Select 1\. Configure IP Address.
  3. Select the PRODUCTION INTERFACE.
  4. Select IPv4 (Static) to set the server IP address, subnet mask, and default gateway, hostname, domain name, and FQDN. Press Enter after providing each value.
  5. Once you have entered the required information, select y and press Enter.
  6. Press x to back main menu.
  7. Select 14\. Reboot system.

- Configure hosts and DNS Nameserver

  1. Log in to the System Console using "admin" account via console on vSphere Client.
  2. Enter m for more options.
  3. Enter A for Advanced Options.
  4. Enter 5 for Hostname Management.

    1. Enter 1.
    2. Enter FQDN of VidyoPortal.
    3. Enter IP address of VidyoPortal.
    4. Enter y
```
1>  10.0.0.1           vidyoportal.example.com
```
    5. Enter X to exit Hostname Management.

  5. Enter x to exit Advanced Options.

  6. Enter b to back main menu.

  7. Select 2\. Configure DNS Nameserver.

  8. Enter two DNS server IP addresses. If you have only one DNS server, use the same one twice.

  9. Once you have entered the required information, select y and press Enter.The System Console main menu appears.

  10. Select 14\. Reboot system.

- Configure NTP time server and timezone

  1. Select 3\. Configure NTP Time Servers to set the NTP (Network Time Protocol) time server.
  2. Select 14\. Reboot system.
  3. Select 4\. Configure Time Zone to specify the time zone you are working in.
  4. Select 14\. Reboot system.

- Install SSL certificate. (Optional)

  1. Importing an SSL Private Key

    1. Logging in to the VidyoRouter Configuration using web browser. `http://[IP or FQDN address]/vr2conf`
    2. Click Security on the left menu.
    3. Click the SSL Private Key tab.
    4. Click Import Private Key.
    5. In the Select Private Key dialog box, click the Select File icon.
    6. Click Open.
    7. In the Select Private Key dialog box, enter your password in the Password field.
    8. Click Upload. (If the upload is successful, the File Upload Success dialog box displays. A hash of the private key you imported displays in the SHA256 field.)

  2. Deploying the Server Certificate

    1. Click the Server Certificate tab.
    2. Click Upload.
    3. In the Select Server Certificate dialog box, click the Select File icon.
    4. Select your server certificate file on your computer and click **Upload**.

  3. Deploying Your Server CA Certificates (Intermediates)

    1. Click the Server CA Certificates tab.
    2. Click Upload.
    3. In the Select Server CA Certificate dialog box, click Select File icon.
    4. Select your server CA certificate file on your computer (may also be referred to as the Intermediate Certificate by your Certificate Authority) or local network and click **Upload**.

  4. Importing Client Root CA Certificates

    1. Click the Advanced tab.
    2. Click **Import Client CA Certificates....**
    3. In the Select file dialog box, choose **Append to existing**.
    4. Click the Select File icon to locate the **CA Certificate** file on your computer.
    5. Click **Upload** to upload the CA certificate file.
    6. Click **Yes** to reboot server.

  5. Enable SSL

    1. Logging in to the VidyoRouter Configuration using web browser. `http://[IP or FQDN address]/vr2conf`
    2. Click Security on the left menu.
    3. Click **Enable SSL** and click **Yes** and **OK**.

### VidyoReplay (optional)

- Changing IP address

  1. Log in to the System Console using "admin" account via console on vSphere Client.
  2. Select 1\. Configure IP Address.
  3. Select the PRODUCTION INTERFACE.
  4. Select IPv4 (Static) to set the server IP address, subnet mask, and default gateway, hostname, domain name, and FQDN. Press Enter after providing each value.
  5. Once you have entered the required information, select y and press Enter.
  6. Press x to back main menu.
  7. Select 14\. Reboot system.

- Configure hosts and DNS Nameserver

  1. Log in to the System Console using "admin" account via console on vSphere Client.
  2. Enter m for more options.
  3. Enter A for Advanced Options.
  4. Enter 5 for Hostname Management.

    1. Enter 1.
    2. Enter FQDN of VidyoPortal.
    3. Enter IP address of VidyoPortal.
    4. Enter y
    5. Continue to add **ALL VidyoPortal and VidyoRouter**.
```
1>  10.0.0.1           vidyoportal.example.com
2>  10.0.0.2           VidyoRouter.example.com
```

    6. Enter X to exit Hostname Management.

  5. Enter x to exit Advanced Options.

  6. Enter b to back main menu.

  7. Select 2\. Configure DNS Nameserver.

  8. Enter two DNS server IP addresses. If you have only one DNS server, use the same one twice.

  9. Once you have entered the required information, select y and press Enter.The System Console main menu appears.

  10. Select 14\. Reboot system.

- Configure NTP time server and timezone

  1. Select 3\. Configure NTP Time Servers to set the NTP (Network Time Protocol) time server.
  2. Select 4\. Configure Time Zone to specify the time zone you are working in.

- Install SSL certificate. (Optional)

  1. Importing an SSL Private Key

    1. Logging in to the Super Admin Portal using web browser. `http://[IP or FQDN address]/`
    2. Click Settings on top right.
    3. Click Security tab.
    4. Click the SSL Private Key tab.
    5. Click Import **Private Key**.
    6. In the Select Private Key dialog box, click the Select File icon.
    7. In the System File Selection dialog box, locate and select your private key file.
    8. Click Open.
    9. In the Select Private Key dialog box, enter your password in the Password field.
    10. Click **Upload**. (If the upload is successful, the File Upload Success dialog box displays. A hash of the private key you imported displays in the **SHA256** field.)

  2. Deploying the Server Certificate

    1. Click the Server Certificate tab.
    2. Click Upload.
    3. In the Select Server Certificate dialog box, click the Select File icon.
    4. Select your server certificate file on your computer and click **Upload**.

  3. Deploying Your Server CA Certificates (Intermediates)

    1. Click the Server CA Certificates tab.
    2. Click Upload.
    3. In the Select Server CA Certificate dialog box, click Select File icon.
    4. Select your server CA certificate file on your computer (may also be referred to as the Intermediate Certificate by your Certificate Authority) or local network and click **Upload**.

  4. Importing Client Root CA Certificates

    1. Click the Advanced tab.
    2. Click **Import Client CA Certificates....**
    3. In the Select file dialog box, choose **Append to existing**.
    4. Click the Select File icon to locate the **CA Certificate** file on your computer.
    5. Click **Upload** to upload the CA certificate file.
    6. Click **Yes** to reboot server.

  5. Enable SSL

    1. Logging in to the Super Admin Portal using web browser. `http://[IP or FQDN address]/`
    2. Click Settings on top right.
    3. Click Security tab.
    4. Click **Enable SSL** and click **Yes** and **OK**.
