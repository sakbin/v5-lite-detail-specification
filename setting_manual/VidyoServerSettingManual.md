# Vidyo Portal Setting Manual

### Vidyoサーバ群のセットアップ手順書

### 更新履歴
日付           | 更新者         | 内容
------------- | ------------- | -------------
2016/04/22    | 濱名           | 情報更新
2016/01/20    | 濱名           | 新規作成

## Vidyo Routerの設定 (仮想マシン名:v5-vidyorouter)
### URL : http://v5-vr-domain/vr2conf/
#### 1. Vidyo Routerの設定ページにログイン  
User Name : admin  
Password : 別途設定シートを参照
![Vidyo Router ](images/v5-vr-login.png)
#### 2. MaintenanceメニューからBasic箇所のConfiguration Server:に設定をいれる  
例）http://v5-vp-test.vcube.net  
SSL有りの場合は、httpsにする
![Vidyo Router ](images/v5-vr-config.png)
#### 3. Securityメニューから、server certificate 確認  
・SSL利用がない場合は空のまま
![Vidyo Router ](images/v5-vr-security_off.png)
・SSL利用の場合は証明書が入っていることを確認し、「Enanle SSLを押下」
![Vidyo Router ](images/v5-vr-security_on.png)


## Vidyo Proxyの設定(仮想マシン名:v5-vidyorouter)
### URL : http://v5-vr-domain/vp2conf/
#### 1. Vidyo Proxyの設定ページにログイン  
User Name : admin  
Password : 別途設定シートを参照
![Vidyo Proxy ](images/v5-vr-vp-login.png)
#### 2. Basic Settings箇所のConfig Server:に設定をいれる  
例）http://v5-vp-test.vcube.net  
SSL有りの場合は、httpsにする
![Vidyo Router ](images/v5-vr-vp-config.png)

## Vidyo managerの設定 (仮想マシン名:v5-vidyoportal)
### URL : http://v5-vp-domain/vm2conf/
Vidyo Portalサーバと同じ

#### 1. Vidyo Managerの設定ページにログイン  
User Name : admin  
Password : 別途設定シートを参照
![Vidyo Manager ](images/v5-vp-vm-login.png)
#### 2. Basic Settings箇所のConfig Server:に設定をいれる  
例）http://v5-vp-test.vcube.net  
SSL有りの場合は、httpsにする
![Vidyo Manager ](images/v5-vp-vm-config.png)

## Vidyo Portalの設定
### URL : http://v5-vp-domain/super/
Vidyo Super Admin ページにログイン  
User Name : super  
Password : 別途設定シートを参照

### 事前確認
#### 1. SettingsタブのSecurityメニューから、server certificate 確認  
・SSL利用がない場合は空のまま
![Vidyo Portal ](images/v5-vp-security_off.png)
・SSL利用の場合は証明書が入っていることを確認し、「Enanle SSLを押下」


#### 2. Componentsタブでに設定したVidyo RouterとVidyo Proxyが表示されていることを確認する
![Vidyo Router](images/v5-vp-components-001.png)

#### Vidyo Manager Setting
#### 1. SettingsタブのSecurityメニューから、server certificate 確認  
・SSL利用がない場合は空のまま
![Vidyo Manager Setting](images/v5-vp-security_off.png)
・SSL利用の場合は証明書が入っていることを確認し、「Enanle SSLを押下」


#### 2. Componentsタブでに設定したVidyo RouterとVidyo Proxyが表示されていることを確認する
![Vidyo Manager Setting](images/v5-vp-components-001.png)

#### 3. ComponentsのType：VideoManager箇所をダブルクリックし、Vidyo Managerの設定に遷移
![Vidyo Manager Setting](images/v5-vp-components-vm-001.png)
![Vidyo Manager Setting](images/v5-vp-components-vm-general-001.png)

#### 4. Generalタブの設定
Name：Manager\_001  
※識別する名前なので、別の名前でも可
Listen Address (EMCP)：Vidyo PortalのFQDNが入っていることを確認
※0.0.0.0になっている場合は、ライセンスがきちんと登録されていないなどの問題がある
![Vidyo Manager Setting](images/v5-vp-components-vm-general-002.png)

#### 5. Securityタブの設定　※SSL利用がある場合のみ設定
Encrypttion : Enableにチェックをいれる
※利用できない場合はグレーアウトでチェックできません
![Vidyo Manager Setting](images/v5-vp-components-vm-security.png)

#### 6. Advancedタブの設定　
Vidyo Portal URL : Vidyo PortalのFQDNになっていることを確認  
例) http://v5-vp-test.vcube.net  
SORP : IPがVidyo PortalのFQDNになっていることを確認  
例) v5-vp-test.vcube.net  
RMCP : IPがVidyo PortalのFQDNになっていることを確認  
例) v5-vp-test.vcube.net  
![Vidyo Manager Setting](images/v5-vp-components-vm-advanced.png)
Manage VidyoCloud : SSLを利用する場合にはチェックをいれる

#### 7. Saveボタン押下で設定を保存
Saveボタン押下で設定を保存し、
ComponentsのType：VideoManager箇所を再度確認し、  
Config Version箇所が分母と分子が同じ数値になっていることを確認する
![Vidyo Router Setting](images/v5-vp-components-vm-002.png)

##### メモ
※Vidyo ManagerのListen Addressが0.0.0.0になる問題  
1. Setting > Platform Network Settings > FQDN( eth0 ) の項目は マシンが自分で名乗ってるFQDNとライセンスで取得したFQDNと  
Setting > System License > FQDN の項目は ライセンスされたFQDN
この２つがあっていないと正常に動作しないので確認する。
（Reason:41000 , Status:4のエラーで入室できないなど）
2. CUI画面から再IP設定
ssh -l admin vidyo-portalドメイン
1のNW設定を行い、すでに入力されているものが表示されていても、再度同じもの入力して設定を行う。入力を行わないと上書きされないため。

### Vidyo Router Setting
#### 1. ComponentsのType：VideoRouter箇所をダブルクリックし、Vidyo Routerの設定に遷移
![Vidyo Router Setting](images/v5-vp-components-vr-001.png)
![Vidyo Router Setting](images/v5-vp-components-vr-general-001.png)

#### 2. Generalタブの設定
Name：Router\_001  
※識別する名前なので、別の名前でも可。複数台構成の場合は_002などにする
Listen Address (SCCP)：Vidyo RouterのFQDNを指定
例) v5-vr.vcube.net
![Vidyo Router Setting](images/v5-vp-components-vr-general-002.png)

#### 3. Vidyo Managerタブの設定
VidyoManager：IP箇所にVidyo PortalのFQDN設定
例) v5-vp-test.vcube.net
![Vidyo Router Setting](images/v5-vp-components-vr-manager-001.png)

#### 4. Securityタブの設定　※SSL利用がある場合のみ設定
Encrypttion : Enableにチェックをいれる
※利用できない場合はグレーアウトでチェックできません
![Vidyo Router Setting](images/v5-vp-components-vr-security.png)

#### 5. Saveボタン押下で設定を保存
Saveボタン押下で設定を保存し、ComponentsのType：VideoRouter箇所を再度確認し、Config Version箇所が分母と分子が同じ数値になっていることを確認する
![Vidyo Router Setting](images/v5-vp-components-vr-002.png)


### Vidyo Proxy Setting  ※SSL利用の場合のみ利用
#### 1. ComponentsのType：VideoProxy箇所をダブルクリックし、Vidyo Routerの設定に遷移
![Vidyo Proxy Setting](images/v5-vp-components-vp-001.png)
![Vidyo Proxy Setting](images/v5-vp-components-vp-general-001.png)

#### 2. Generalタブの設定
Component Name：Proxy\_001  
※識別する名前なので、別の名前でも可。複数台構成の場合は_002などにする
URL：Vidyo RouterのFQDNを指定＋:443
例) v5-vr.vcube.net:443
※プロトコルは指定しない
![Vidyo Proxy Setting](images/v5-vp-components-vp-general-002.png)

#### 3. Saveボタン押下で設定を保存
Saveボタン押下で設定を保存し、ComponentsのType：VideoRouter箇所を再度確認し、Config Version箇所が分母と分子が同じ数値になっていることを確認する
![Vidyo Router Setting](images/v5-vp-components-vr-002.png)


### VidyoCloud Setting  ※SSL利用のみ　調整中
※Routerを複数台での利用の場合。Managerの設定でManage VidyoCloudにチェックを入れた場合のみ
#### 1. 左メニューのManage VidyoCloudを押下し設定画面に遷移
![Vidyo Router Setting](images/v5-vp-vidyocloud-001.png)

#### 2. VidyoRouter Poolsの設定
VidyoRouter PoolsタブからAdd Poolを実行
![Vidyo Router Setting](images/v5-vp-vidyocloud-001.png)
Router Pool Name : Pool\_001
※識別する名前なので、別の名前でも可。複数台構成の場合は_002などにし、所在地で分ける場合は
TYO\_001などにする

Modyfiedをチェックしてから、add poolを実行
AvaiableからRouterを選択して、Poolにドラッグアンドドロップ
Location TagをAdd
endpoint
add rule でlocation tagを追加
利用するPoolを追加する
最後にActivateしないと有効にならないので、必ずする

### VidyoReplay Recorders Setting  ※録画利用時
#### 1. 左メニューのManage VidyoReplay Recordersを押下し設定画面に遷移
![Vidyo Replay Setting](images/v5-vp-rp-001.png)
#### 2. Addを押してRecorderを追加画面へ遷移
![Vidyo Replay Setting](images/v5-vp-rp-002.png)
#### 3. Recorder情報を入力  
Component Name : Recorder01  
Login Name : recorder01  
Password : 任意のパスワード  
Verify Password : 上で入力したパスワード  
※ Replayが複数の場合には、Recorder01、Recorder02...のようにして複数登録  
※ Login Name、Passwordで設定したものはVidyo Replayのセットアップをする際に必要になります
![Vidyo Replay Setting](images/v5-vp-rp-003.png)
#### 4. Saveを押下し、リストに表示されることを確認する
![Vidyo Replay Setting](images/v5-vp-rp-004.png)

### VidyoReplay Setting  ※録画利用時
#### 1. 左メニューのManage VidyoReplaysを押下し設定画面に遷移
![Vidyo Replay Setting](images/v5-vp-rp-005.png)
#### 2. Addを押してRecorderを追加画面へ遷移
![Vidyo Replay Setting](images/v5-vp-rp-006.png)
#### 3. Recorder情報を入力  
Component Name : Replay01  
Login Name : replay01  
Password : 任意のパスワード  
Verify Password : 上で入力したパスワード  
※ Replayが複数の場合には、Recorder01、Recorder02...のようにして複数登録  
※ Login Name、Passwordで設定したものはVidyo Replayのセットアップをする際に必要になります
![Vidyo Replay Setting](images/v5-vp-rp-007.png)
#### 4. Saveを押下し、リストに表示されることを確認する
![Vidyo Replay Setting](images/v5-vp-rp-008.png)

### Tenants Setting
#### 1. 上部のTenantsタブを押下し設定画面に遷移し、Tenant Name箇所のDefaultをクリックし、編集画面に遷移
![Vidyo Router Setting](images/v5-vp-tenants-main-start.png)
![Vidyo Router Setting](images/v5-vp-tenants-modify-001.png)

#### 2. Tenant Name箇所のDefaultをクリックし、編集画面に遷移
Tenant Name : FQDNの一部など  
例) v5-vp-test  
※識別する任意の名前  
Tenant URL : Vidyo PortalのFQDN
Extention : 001
例) v5-vp-test.vcube.net  
*# of Installs  
*# of Seats  
*# of Lines  
の数値を赤字で表示されている「max」の値を入力し、Nextボタンを押下  
![Vidyo Router Setting](images/v5-vp-tenants-modify-002.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-001.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-002.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-003.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-004.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-005.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-006.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-007.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-008.png)
Nextボタンを押下
![Vidyo Router Setting](images/v5-vp-tenants-next-009.png)
Saveボタン押下
![Vidyo Router Setting](images/v5-vp-tenants-save.png)
Tenant Name、Tenant URLが間違えていないか確認
![Vidyo Router Setting](images/v5-vp-tenants-main-end.png)

## Vidyo Adminの設定
### URL : http://v5-vp-domain/admin/
#### 1. Vidyo Adminページにログイン
User Name : super  
Password : 別途設定シートを参照
![Vidyo Manager ](images/v5-vp-admin-login.png)
#### 2. Groupsタブ押下でGroup設定ページに遷移
![Vidyo Manager ](images/v5-vp-admin-main.png)
#### 3. Default箇所押下し、変更ページヘ遷移
![Vidyo Manager ](images/v5-vp-admin-group-001.png)
#### 4. *Max Number of Participants箇所に入室最大人数を入力して、Saveを押下
![Vidyo Manager ](images/v5-vp-admin-group-modify.png)
#### 5. Max Participantsが設定した値に変わっていることを確認
![Vidyo Manager ](images/v5-vp-admin-group-002.png)
#### 6. Usersタブ押下で、Manage Usersページへ遷移
![Vidyo Manager ](images/v5-vp-admin-user-001.png)
#### 7. Add User押下で、Web PortalとのAPI連携用ユーザーを作成する  
以下のCaptureを参考に入力  
User Type : Admin  
User Name : V5OnprePortal  
Password : 任意の推測されにくいパスワード  
Verify Password : Passwordで入力したもの  
Display Name : V5Portal  
E-Mail Address : 案件のメーリスや任意のもの（実際にメールが飛ぶことはありません）  
Extension : 2192  
![Vidyo Manager ](images/v5-vp-admin-user-002.png)
入力完了したら、Saveを押下し、リストに表示されていることを確認する
![Vidyo Manager ](images/v5-vp-admin-user-003.png)


## Vidyo Replay設定 (仮想マシン名:v5-vidyoreplay)
### URL : http://v5-replay-domain/
#### 1. Vidyo Replayの設定ページにログイン  
User Name : super  
Password : 別途設定シートを参照
![Vidyo Replay ](images/v5-rp-login.png)
#### 2. 上部メニューのSettingsを押下し、設定画面に遷移する  
![Vidyo Replay ](images/v5-rp-setting01.png)
#### 3. Settings画面に遷移したら、Generalタブで以下の項目を入力する  
・VidyoReplay Registration  
VidyoPortal Address	: Vidyo Portalのアドレス  例) http://v5-vp-test.vcube.net  
VidyoReplay UserName  
VidyoReplay Password  
VidyoReplay Password Confirm  
こちらについては、Vidyo Portalの設定でのVidyoReplay Settingで設定したものを入力
・Recorder Controller Registration  
UserName  
Password  
Password Confirm  
こちらについては、Vidyo Portalの設定でのVidyoReplay Recorders Settingで設定したものを入力  
![Vidyo Replay ](images/v5-rp-setting02.png)
入力が完了したら、ページ下部まで移動し、Save & Applyを押下
![Vidyo Replay ](images/v5-rp-setting03.png)
押下後、Settings ware updated successfullyと表示されることを確認
![Vidyo Replay ](images/v5-rp-setting04.png)
少し時間をおいて、再度Settingsページを確認し、Registration StatusがRegisteredになっていることを確認
![Vidyo Replay ](images/v5-rp-setting05.png)
※ここでステータスがRegisteredになっていない場合は、Vidyo Portalで設定したUserNameと違っている可能性があるので、再度確認
#### 4. Recorderタブに遷移し、Recorderの設定を行う
![Vidyo Replay ](images/v5-rp-setting06.png)
SSLを利用する際には、Configration箇所の[Encrypt Medeia Traffic]にチェックをいれて、[Save & apply]を押す

#### 5. Profilesを押下し、現在登録されているProfileを削除する
Atcions箇所のDeleteを実行し、削除
![Vidyo Replay ](images/v5-rp-setting07.png)

#### 6. 新規プロファイルを追加する
Add Profileを押下し、追加画面に遷移する  
![Vidyo Replay ](images/v5-rp-setting08.png)
以下のルールで利用する録画画質によって、Profileを入力し、Saveを実行  
##### SD
Prefix : 10101  
Description : SD1  
Type : Video and Audio  
Resolution : SD  
※Replayサーバ1台につき同時録画数が10なので、10001から10010まで追加  
複数台の場合は、10201の用に変更する

##### HD720
Prefix : 20101  
Description : HD1  
Type : Video and Audio  
Resolution : HD720  
※Replayサーバ1台につき同時録画数が5なので、10101から10105まで追加  

##### HD1080
Prefix : 30101  
Description : FULLHD1  
Type : Video and Audio  
Resolution : HD1080  
※Replayサーバ1台につき同時録画数が1なので、10201から10202まで追加 

##### CIF
Prefix : 40101  
Description : CIF  
Type : Video and Audio  
Resolution : CIF  
※Replayサーバ1台につき同時録画数が20なので、40001から40020まで追加 

##### AUDIO
Prefix : 50101  
Description : AUDIO  
Type : Audio Only 
※Replayサーバ1台につき同時録画数が20なので、40001から40020まで追加 

###### 複数台のReplayサーバを利用する場合は20001のようにはじめの桁数を変更する

#### 7. Profileの登録が完了したら、 Apply Profile Changesを実行
![Vidyo Replay ](images/v5-rp-setting10.png)

実行し、[Prodile changes have been applied.]を表示されたことを確認
Applayを実行することにより、Vidyo Portal側に同期がされます
![Vidyo Replay ](images/v5-rp-setting11.png)

#### 8. Vidyo PortalにRecorderが登録されたことを確認する
Vidyo Portalにはいり、CompornentsタブのManage VidyoReplay Recordersから登録したRecorder箇所より、以下のキャプチャのように追加したProfileが表示されていることを確認する
![Vidyo Replay ](images/v5-rp-setting12.png)