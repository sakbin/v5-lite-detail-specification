# class IV5DeviceEventSink

メディアのリスナー Interface です。

##IV5DeviceEventSink::~IV5DeviceEventSink
`virtual ~IV5DeviceEventSink();`

デストラクタです。
リスナーが消滅した際に呼ばれます。

## IV5DeviceEventSink::onAudioInDeviceListChanged
`virtual void onAudioInDeviceListChanged(int index) = 0;`

音声入力デバイスの抜き差しを検出し、リストが変更されたことを通知します。


###引数
| Member Type | Definition   | Description                  |
|-------------|--------------|------------------------------|
| int         | index        | 追加されたマイクデバイスの参照値   |


## IV5DeviceEventSink::onAudioOutDeviceListChanged
`virtual void onAudioOutDeviceListChanged(int index) = 0;`

音声出力デバイスの抜き差しを検出し、リストが変更されたことを通知します。

###引数
| Member Type | Definition   | Description                        |
|-------------|--------------|------------------------------------|
| int         | index        | 現在参照している音声出力デバイスの参照値  |


## IV5DeviceEventSink::onVideoDeviceListChanged
`virtual void onVideoDeviceListChanged(int index) = 0;`

映像デバイスの抜き差しを検出し、リストが変更されたことを通知します。

###引数
| Member Type | Definition   | Description                     |
|-------------|--------------|---------------------------------|
| int         | index        | 現在参照している映像デバイスの参照値  |


## IV5DeviceEventSink::onSelectedSystemAudioInDevicesChanged
`virtual void onSelectedSystemAudioInDevicesChanged() = 0;`

OSで選択された既定の音声入力デバイスが変更されたことを通知します。

###引数
| Member Type | Definition   | Description                  |
|-------------|--------------|------------------------------|
| int         | index        | 追加されたマイクデバイスの参照値   |


## IV5DeviceEventSink::onSelectedSystemAudioOutDevicesChanged
`virtual void onSelectedSystemAudioOutDevicesChanged() = 0;`

OSで選択された既定の音声出力デバイスが変更されたことを通知します。

###引数
| Member Type | Definition   | Description                        |
|-------------|--------------|------------------------------------|
| int         | index        | 現在参照している音声出力デバイスの参照値  |

