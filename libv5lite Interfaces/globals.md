*****************************************************************************
# struct V5ChatMessage

ルームチャットのリスナー Interface です。  

##Public member properties

| Member Name      | Type                        | Description                  |
|------------------|-----------------------------|------------------------------|
| text             | const char \*               | 発言内容の文字列               |
| from             | const char \*               | 発言者の参加者 ID             |
| timestamp        | unsigned long               | 発言時刻。GMT の UNIXTIME     |
| isSelfMessage    | bool                        | 自分自身による発言であるか       |

*****************************************************************************
# struct V5Participant

ルームへの参加者を表現します。

##Public member properties

| Member Name      | Type                        | Description                  |
|------------------|-----------------------------|------------------------------|
| id               | const char \*                | 参加者 ID                    |
| displayName      | const char \*                | 参加者の表示名                |

*****************************************************************************
# struct V5ConferenceStatus

ルームの設定情報を表現します。

##Public member properties

| Member Name         | Type            | Description                  |
|---------------------|-----------------|------------------------------|
| ConferenceName      | const char \*   | 会議名                        |
| RoomName            | const char \*   | 会議室名                      |
| ConferenceURL       | const char \*   | 招待URL                      |
| ConferencePinCode   | const char \*   | 招待Pinコード                 |
| DocumentSender      | const char \*   | 資料共有実施者のID             |
| ConferenceStartTime | long long       | 会議開始時間                   |
| ConferenceEndTime   | long long       | 会議終了時間                   |
| RemainTime          | long long       | 予約会議終了までの残り時間       |
| PastTime            | long long       | 会議経過時間                   |
| isReserved          | bool            | 予約状態                      |
| isRecoading         | bool            | 現在の録画実施状態              |
| isLocked            | bool            | 現在のルーム参加可否状態 　      |


*****************************************************************************
# struct V5ConferenceInfo

ルームへの参加者を表現します。

##Public member properties

| Member Name      | Type                   | Description        |
|------------------|------------------------|--------------------|
| ConfSts          | V5ConferenceStatus \*  | 会議情報            |
| participants     | V5Participant \*       | 参加者リスト         |
| length           | unsinged long          | 参加者リスト数       |
| selfParticipant  | V5Participant \*       | 自分の情報          |

*****************************************************************************
# struct V5ErrorInfo

エラー情報を表現します。

##Public member properties

| Member Name      | Type      | Description        |
|------------------|-----------|--------------------|
| unsigned int     | category  | 理由　              |
| unsigned int     | reason    | 詳細                |

※categoryは切断理由等
詳細は切断条件に関連した詳細情報
設定値については現在整理中。

*****************************************************************************
# typedefs
`typedef std::vector<V5Participant> V5ParticipantList;`

| Type Name                         | Definition                             |
|-----------------------------------|----------------------------------------|
| V5ParticipantList                 | std::vector&lt;V5Participant&gt;       |


*****************************************************************************
