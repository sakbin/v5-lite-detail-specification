# class V5Client

V5Client のFACADE クラス。  
View 側への API を用意する。

##Public member types

| Member Type           | Definition              |
|-----------------------|-------------------------|
| IChatEventSink        | IV5ChatEventSink        |
| IConferenceEventSink  | IV5ConferenceEventSink  |
| IMediaEventSink       | IV5MediaEventSink       |

##V5Client::initialize
`static V5CODE initialize(const char *logpath, const char *log_priority);`

クライアント初期化処理を行います。

###引数
| Member Type           | Definition   | Description                     |
|-----------------------|--------------|---------------------------------|
| const char *          | logpath      | ログを保存するファイルパスを指定します。  |
| const char *          | log_priority | ログの出力レベルを指定します。         |

###戻り値
V5CODE型のエラーコードが戻ります。

##V5Client::finalize
`static V5CODE finalize();`

クライアント終了処理を行います。

###戻り値
V5CODE型のエラーコードが戻ります。

##V5Client::createInstance
`static instance_ptr_t createInstance();`

インスタンスを生成します。

##V5Client::~V5Client
`virtual ~V5Client();`

デストラクタです。
クラスが消滅した際に呼ばれます。

## V5Client::startLogic
`virtual V5CODE startLogic(V5WindowId parentWindow, V5Rect *videoRect, const char *fontpath, const char *certificatepath, const char *configKeyPath) = 0;`
`virtual V5CODE startLogic(V5WindowId parentWindow, V5Rect *videoRect, const char *fontpath, const char *certificatepath) = 0;`

ライブラリ起動後の初期処理を実施します。

###引数
| Member Type           | Definition      | Description  |
|-----------------------|-----------------|--------------|
| V5WindowId            | parentWindow    | Video をレンダリングするウィンドウのハンドルを渡します。ハンドルは各 OS のウィンドウハンドル表現を渡します。（例：Win32:HWND / Cocoa:UIWindow*) |
| V5Rect *              | videoRect       | 各種サーバへの接続に必要な情報を格納する領域のアドレスを渡します。|
| const char *          | fontpath        | フォントをファイルパスで指定します。|
| const char *          | certificatepath | 証明書をファイルパスで指定します。|
| const char *          | configKeyPath   | ユーザー設定をファイルパスで指定します。|

###戻り値
V5CODE型のエラーコードが戻ります。

##V5Client::stopLogic
`virtual V5CODE stopLogic() = 0;`

アプリケーションを終了する際のライブラリの後始末を実施します。

###引数
なし。

###戻り値
V5CODE型のエラーコードが戻ります。

##V5Client::isBusy
`virtual bool isBusy() const = 0;`

ライブラリの待機状態を返します。

###引数
なし。

###戻り値
bool型のビジー状態フラグが戻ります。

##V5Client::beginSession
`virtual V5CODE beginSession(const char *vcubeURL, const char *token, const char *version ) = 0;`

会議入室の準備処理を実施します。


###引数
| Member Type           | Definition   | Description                               |
|-----------------------|--------------|-------------------------------------------|
| const char *          | vcubeURL     | Vcube ポータルのURLを指定します。               |
| const char *          | token        | ルームへ入室する際に使用するTOKENを指定します。   |
| const char *          | version      | バージョンを指定します。                        |

###戻り値
V5CODE型のエラーコードが戻ります。

## V5Client::joinConference
`virtual V5CODE joinConference(const char *displayName, const PasswdInfoPtrT& psswd) = 0;`

会議参加処理を実行します。

###引数
| Member Type            | Definition   | Description                               |
|------------------------|--------------|-------------------------------------------|
| const char *           | displayName  | ルームで他の参加者に対して表示する名称を指定します。|
| const PasswdInfoPtrT\& | psswd        | ルームに入室する際に使用するパスワードを指定します。 |

###戻り値
V5CODE型のエラーコードが戻ります。

## V5Client::leaveConferenceSync
`virtual void leaveConferenceSync() = 0;`

会議から退室します。退室処理が完了するまで待ち合わせを行います。

###引数
なし。

###戻り値
戻り値はありません。

## V5Client::leaveConferenceAsync
`virtual AsyncResult leaveConferenceAsync() = 0;`

会議から退室します。非同期処理にて退室処理を実施するため、起動後すぐに処理をかえします。

###引数
なし。

###戻り値
AsyncResult型の状態コードが戻ります。

## V5Client::sendInviteMail
`virtual V5CODE sendInviteMail(const char* mailAddress) = 0;`

招待メールを送信します。

###引数
| Member Type            | Definition   | Description                               |
|------------------------|--------------|-------------------------------------------|
| const char *           | mailAddress  | 招待メール送るメールアドレスを指定します。          |

###戻り値
V5CODE型のエラーコードが戻ります。

## V5Client::onReceiveJoinConferenceRequest
`virtual void onReceiveJoinConferenceRequest(const char *token, const char *V5LiteURL) = 0;`

会議参加要求処理を受信した際の通知に使用されます。<br>
本APIの通知を受けた後、JoinConference-APIを起動してください。

###引数
| Member Type           | Definition   | Description                               |
|-----------------------|--------------|-------------------------------------------|
| const char *          | token        | ルームへ入室する際に使用するTOKENを指定します。   |
| const char *          | V5LiteURL    | V5Lite ポータルのURLを指定します。              |

###戻り値
なし。

## V5Client::isGuest
`virtual bool isGuest() = 0;`

ゲスト状態かどうかを確認します。<br>
（**true**ならゲスト、**false**ならゲスト以外）<br>
※重説では本APIではなく、他の実行可能権限を参照するAPIがあるため、別項目参照。

###引数
なし。

###戻り値
| Member Type           | Definition   | Description                        |
|-----------------------|--------------|------------------------------------|
| bool                  | -            | ゲスト状態                           |

## V5Client::isCameraMuted
`virtual bool isCameraMuted();`

現在の映像入力(カメラ)のミュート状態を返します。

###引数
なし。

###戻り値
| Member Type           | Definition   | Description                        |
|-----------------------|--------------|------------------------------------|
| bool                  | -            | 現在の映像入力(カメラ)のミュート状態      |

## V5Client::isMicrophoneMuted
`virtual bool isMicrophoneMuted();`

現在の音声入力(マイク)のミュート状態を返します。

###引数
なし。

###戻り値
| Member Type           | Definition   | Description                    |
|-----------------------|--------------|--------------------------------|
| bool                  | -            | 現在の音声入力(マイク)のミュート状態  |

## V5Client::isSpeakerMuted
`virtual bool isSpeakerMuted();`

現在の音声出力(スピーカー)のミュート状態を返します。

###戻り値
| Member Type           | Definition   | Description                        |
|-----------------------|--------------|------------------------------------|
| bool                  | -            | 現在の音声出力(スピーカー)のミュート状態   |

## V5Client::muteCamera
`virtual void muteCamera(bool muted, bool force=false) = 0;`

映像入力(カメラ)のミュート状態を変更します。

###引数
| Member Type  | Definition   | Description                                         |
|--------------|--------------|-----------------------------------------------------|
| bool         | muted        | 設定するミュート状態。trueを設定するとミュート状態になります。    |
| bool         | force        | 設定するミュート状態。trueを設定すると強制的にミュート状態になります。|

###戻り値
なし。

## V5Client::muteMicrophone
`virtual void muteMicrophone(bool muted, bool force=false) = 0;`

音声入力(マイク)のミュート状態を変更します。

###引数
| Member Type  | Definition   | Description                                         |
|--------------|--------------|-----------------------------------------------------|
| bool         | muted        | 設定するミュート状態。trueを設定するとミュート状態になります。    |
| bool         | force        | 設定するミュート状態。trueを設定すると強制的にミュート状態になります。|

###戻り値
なし。

## V5Client::muteSpeaker
`virtual void muteSpeaker(bool muted, bool force=false) = 0;`

音声出力(スピーカー)のミュート状態を変更します。

###引数
| Member Type  | Definition   | Description                                         |
|--------------|--------------|-----------------------------------------------------|
| bool         | muted        | 設定するミュート状態。trueを設定するとミュート状態になります。    |
| bool         | force        | 設定するミュート状態。trueを設定すると強制的にミュート状態になります。|

###戻り値
なし。

## V5Client::setFrameDirection
`virtual V5CODE setFrameDirection(V5Rect rect) = 0;`

画面表示位置を設定します。

###引数
| Member Type   | Definition   | Description                                         |
|---------------|--------------|-----------------------------------------------------|
| V5Rect        | rect         | 画面表示位置を指定します。                              |

###戻り値
V5CODE型のエラーコードが戻ります。

## V5Client::setOrientation
`virtual V5CODE setOrientation(V5Orientation orie) = 0;`

映像の表示レイアウトの向きの変更を行います。
※RawFrameの場合は本I/Fを起動しても表示方向は変更されません。

###引数
| Member Type   | Definition   | Description                                         |
|---------------|--------------|-----------------------------------------------------|
| V5Orientation | orie         | 表示レイアウトの向きを指定します。                          |

###戻り値
V5CODE型のエラーコードが戻ります。

## V5Client::putShareImage
`virtual void putShareImage(uint8_t const* data, int width, int height, int size) = 0;`

画像共有を実行する際に使用します。

###引数
| Member Type     | Definition   | Description                                         |
|-----------------|--------------|-----------------------------------------------------|
| const uint8_t * | data         | データ内容                                           |
| int             | width        | データの幅                                           |
| int             | height       | データの高さ                                          |
| int             | size         | データのサイズ                                         |

###戻り値
なし。

## V5Client::setConferenceEventSink
`virtual void setConferenceEventSink(std::nullptr_t) = 0;`
`virtual void setConferenceEventSink(std::weak_ptr<IV5ConferenceEventSink> const& sink) = 0;`

会議イベントのリスナーを登録します。<br>
※このメソッドは前回の設定値を上書きします。<br>
登録を実施しない場合、会議内のイベント通知が行われません。

###引数
| Member Type                                    | Definition   | Description                                         |
|------------------------------------------------|--------------|-----------------------------------------------------|
| const std::weak_ptr<IV5ConferenceEventSink> \& | sink         | Conference関連のリスナーを指定します。                    |
 
###戻り値
なし。

## V5Client::setChatEventSink
`virtual void setChatEventSink(std::nullptr_t) = 0;`
`virtual void setChatEventSink(std::weak_ptr<IV5ChatEventSink> const& chatSink) = 0;`

チャットイベントのリスナーを登録します。<br>
※このメソッドは前回の設定値を上書きします。<br>
登録を実施しない場合、チャット関連のイベント通知が行われません。

###引数
| Member Type                              | Definition   | Description                                         |
|------------------------------------------|--------------|-----------------------------------------------------|
| const std::weak_ptr<IV5ChatEventSink> \& | chatSink     | チャット関連のリスナーを指定します。                         |

###戻り値
なし。

## V5Client::setDeviceEventSink
`virtual void setDeviceEventSink(std::nullptr_t) = 0;`
`virtual void setDeviceEventSink(std::weak_ptr<IV5DeviceEventSink> const& deviceSink) = 0;`

デバイスに関するイベントのリスナーを登録します。<br>
※このメソッドは前回の設定値を上書きします。<br>
登録を実施しない場合、デバイス関連のイベント通知が行われません。

###引数
| Member Type                                | Definition   | Description                                         |
|--------------------------------------------|--------------|-----------------------------------------------------|
| const std::weak_ptr<IV5DeviceEventSink> \& | sink         | Device関連のリスナーを指定します。                        |

###戻り値
なし。

## V5Client::sendChatMessage
`virtual void sendChatMessage(const char* SendMessage) = 0;`

ルームチャットへの発言を行います。<br>
同一ルーム内の全員へ通知されます。

###引数
| Member Type   | Definition   | Description                                                                   |
|---------------|--------------|-------------------------------------------------------------------------------|
| const char \* | SnedMessage  | ルームチャットに発信する発言の内容を指定します。指定情報に名前、発言時刻の設定は必要ありません。|

###戻り値
なし。

## V5Client::startHTTPD
`virtual bool startHTTPD(unsigned short port = 21920) = 0;`

HTTPDを開始します。<br>
※デフォルト:21920<br>
重説、クラウド等で接続先サーバを切り分ける必要がある場合は重複していないポート番号の指定が必要



###引数
| Member Type           | Definition   | Description                              |
|-----------------------|--------------|------------------------------------------|
| unsigned short        | port         | 使用するポート番号を指定します。               |

###戻り値
| Member Type           | Definition   | Description                        |
|-----------------------|--------------|------------------------------------|
| bool                  | -            | HTTPD開始に成功したかどうか             |

## V5Client:: stopHTTPD
`bool stopHTTPD();`

HTTPDを停止します。

###引数
なし。

###戻り値
| Member Type           | Definition   | Description                        |
|-----------------------|--------------|------------------------------------|
| bool                  | -            | HTTPD停止に成功したかどうか             |

## V5Client::getDeviceConfiguration
`virtual DeviceConfigurationPtrT getDeviceConfiguration() = 0;`

デバイス(カメラ、マイク、スピーカー)情報を取得します。

###引数
なし。

###戻り値
| Member Type             | Definition   | Description                    |
|-------------------------|--------------|--------------------------------|
| DeviceConfigurationPtrT | -            | デバイスの設定情報を返します。       |

## V5Client::lockConference
`virtual void lockConference(bool LockStatus) = 0;`

入室中の会議の入室可否(Lock)状態変更を行います。

###引数
| Member Type     | Definition   | Description                                             |
|-----------------|--------------|---------------------------------------------------------|
| bool            | LockStatus   | ルームの入室可否状態を指定します。trueを指定で入室拒否状態になります。 |

###戻り値
なし。

## V5Client::rejectParticipant
`virtual void rejectParticipant(const char *RejectPID) = 0;`

指定した参加者をルームから強制退出させます。<br>
※招待参加者または強制退室の実行権限がないユーザはこの機能を使用することはできません。

###引数
| Member Type     | Definition   | Description                      |
|-----------------|--------------|----------------------------------|
| const char *    | RejectPID    | 強制退室対象の参加者のIDを指定します。 |

###戻り値
なし。

## V5Client::updateConferenceName
`virtual void updateConferenceName(const char *ConferenceName) = 0;`

参加中の会議名称の変更を実施します。

###引数
| Member Type     | Definition        | Description                      |
|-----------------|-------------------|----------------------------------|
| const char *    | ConferenceName    | ルーム情報を指定します。              |

###戻り値
なし。

## V5Client::getConferenceStatus
`virtual std::shared_ptr<const V5ConferenceStatus> getConferenceStatus() const = 0;`

現在参加中の会議情報の取得します。

###引数
なし。

###戻り値
| Member Type                               | Definition   | Description                      |
|-------------------------------------------|--------------|----------------------------------|
| std::shared_ptr<const V5ConferenceStatus> | -            | 現在のルーム情報を返します。          |

## V5Client::getSelfParticipant
`virtual std::shared_ptr<const V5Participant> getSelfParticipant() = 0;`

会議内での自ユーザ情報を取得します。

###引数
なし。

###戻り値
| Member Type                          | Definition   | Description                      |
|--------------------------------------|--------------|----------------------------------|
| std::shared_ptr<const V5Participant> | -            | 現在の参加者情報を返します。          |

## V5Client::enableActiveSpeakerLayout
`virtual void enableActiveSpeakerLayout(bool showActiveSpeaker) = 0;`

会議内での映像表示を行う際、レイアウトを音声による順位変動表示にするかどうかを設定する。

###引数
| Member Type  | Definition           | Description                                  |
|--------------|----------------------|----------------------------------------------|
| bool         | showActiveSpeaker    | システム標準のオーディオ設定を表示するかどうか          |

###戻り値
なし。

## V5Client::changeSelfPreviewMode
`virtual void changeSelfPreviewMode(V5ClientPreviewMode previewMode) = 0;`

自映像のプレビュー表示状態の変更を行います。

###引数
| Member Type          | Definition     | Description               |
|----------------------|----------------|---------------------------|
| V5ClientPreviewMode  | previewMode    | プレビュー形式の設定          |

###戻り値
なし。

## V5Client::setSelfViewLoopbackPolicy
`virtual void setSelfViewLoopbackPolicy(V5ClientSelfViewLoopbackPolicy viewPolicy) = 0;`

自映像表示の映像取得先の切り替えを行います。

###引数
| Member Type                     | Definition    | Description               |
|---------------------------------|---------------|---------------------------|
| V5ClientSelfViewLoopbackPolicy  | viewPolicy    | カメラ設定                  |

###戻り値
なし。

# V5Client::shareAppWindow
`virtual void shareAppWindow(V5WindowCapturerWindowId windowId, int width, int height) = 0;`

アプリケーション画面共有機能を設定します。

###引数
| Member Type               | Definition    | Description               |
|---------------------------|---------------|---------------------------|
| V5WindowCapturerWindowId  | windowId      | ウィンドウID                  |
| int                       | width         | 幅                        |
| int                       | height        | 高さ                       |

###戻り値
なし。

# V5Client::shareSysDesktop
`virtual void shareSysDesktop(V5Uint desktopID, int width, int height, bool pointable) = 0;`

PC画面共有機能を設定します。

###引数
| Member Type               | Definition    | Description               |
|---------------------------|---------------|---------------------------|
| V5Uint                    | desktopID     | ウィンドウID                  |
| int                       | width         | 幅                        |
| int                       | height        | 高さ                       |
| bool                      | pointable     | ポインタ表示有無             |

###戻り値
なし。

# V5Client::unshare
`virtual void unshare() = 0;`

画面共有を終了します。

###引数
なし。

###戻り値
なし。

# V5Client::startWatchShared
`virtual void startWatchShared(const char * uri) = 0;`

指定された画面共有の参照を開始します。

###引数
| Member Type               | Definition    | Description               |
|---------------------------|---------------|---------------------------|
| const char *              | uri           | 画面共有URI               |

###戻り値
なし。

# V5Client::stopWatchShared
`virtual void stopWatchShared(const char * uri) = 0;`

画面共有の参照を終了します。

###引数
| Member Type               | Definition    | Description               |
|---------------------------|---------------|---------------------------|
| const char *              | uri           | 画面共有URI               |

###戻り値
なし。

# V5Client::changeParticipantVideoFrame
`virtual V5CODE changeParticipantVideoFrame(const char* participantURI, int width, int height, int frameRate, int minFrameInterval) = 0;`

指定したparticipantURIの画面表示切り替えを変更します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| const char *              | participantURI     | 画面共有URI               |
| int                       | width              | 幅                       |
| int                       | height             | 高さ                      |
| int                       | frameRate          | フレームレート             |
| int                       | minFrameInterval   | フレームインターバル           |

###戻り値
V5CODE型のエラーコードが戻ります。

# V5Client::DebugFuncCall
`virtual void DebugFuncCall() = 0;`

カンファレンスサーバに対するデバッグメセージの送信処理を実施します。<br>
※このAPIはテストコンパイル時にしか実行できません。

###引数
なし。

###戻り値
なし。

# V5Client::isRawFrameMode
`virtual bool isRawFrameMode() = 0;`

RawFrameモードかどうかを取得します。

###引数
なし。

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| bool                      | -                  | RawFrameモードかどうか       |

# V5Client::SendWorkerCrash
`virtual void SendWorkerCrash() = 0;`

接続中のカンファレンスサーバのワーカーをクラッシュさせるメッセージを送信します。<br>
※このAPIはテストコンパイル時にしか実行できません。

###引数
なし。

###戻り値
なし。

# V5Client::getWindowsAndDesktops
`virtual WindowAndDesktopsPtrT getWindowsAndDesktops() = 0;`

共有可能なPC画面およびアプリケーション一覧を取得します。

###引数
なし。

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| WindowAndDesktopsPtrT     | -                  | 画面状態                  |

# V5Client::setMaxParticipants
`virtual void setMaxParticipants(unsigned int maxParticipants) = 0;`

会議への参加者最大人数を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| unsigned int              | maxParticipants    | 参加者最大人数            |

###戻り値
なし。

# V5Client::getMaxParticipants
`virtual unsigned int getMaxParticipants() = 0;`

会議の参加者最大人数を取得します。

###引数
なし。

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| unsigned int              | -                  | 参加者最大人数            |

# V5Client::forceRendering
`virtual void forceRendering(std::shared_ptr<std::string> const& pid) = 0;`

指定したpidのレンダリングを強制的に開始します。<br>
（Raw Frame 専用機能）

###引数
| Member Type                           | Definition         | Description              |
|---------------------------------------|--------------------|--------------------------|
| const std::shared_ptr\<std::string\> \& | pid                | 参加者最大人数            |

###戻り値
なし。

# V5Client::removeForceRendering
`virtual void removeForceRendering() = 0;`

強制的に表示中のレンダリングを終了します。<br>
（Raw Frame 専用機能）

###引数
なし。

###戻り値
なし。

# V5Client::renderSelfView
`virtual void renderSelfView(bool render) = 0;`

自映像のレンダリングを実施するかどうかの設定変更を行います。

###引数
| Member Type       | Definition         | Description              |
|-------------------|--------------------|--------------------------|
| bool              | render             | ユーザー設定               |

###戻り値
なし。

# V5Client::setVideoFrameSize
`virtual void setVideoFrameSize(int width, int height, int frameRate, int minFrameInterval) = 0;`

RawFrameでの映像表示を行う際に使用する共通的なビデオ関連情報を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| int                       | width              | 幅                       |
| int                       | height             | 高さ                      |
| int                       | frameRate          | フレームレート              |
| int                       | minFrameInterval   | フレームインターバル         |

###戻り値
なし。

# V5Client::setShareFrameSize
`virtual void setShareFrameSize(int width, int height, int frameRate, int minFrameInterval) = 0;`

RawFrameでの画面共有映像の表示を行う際のビデオ共有情報を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| int                       | width              | 幅                       |
| int                       | height             | 高さ                      |
| int                       | frameRate          | フレームレート               |
| int                       | minFrameInterval   | フレームインターバル           |

###戻り値
なし。

# V5Client::setSelectDeviceReserve
`virtual void setSelectDeviceReserve(unsigned int type, unsigned int index ) = 0;`

指定されたデバイスタイプと参照値に従いデバイス変更を実施します。


###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| unsigned int              | type               | デバイス種別               |
| unsigned int              | index              | 番号                     |

###戻り値
なし。

# V5Client::isCameraMuteChangeState
`virtual void isCameraMuteChangeState() = 0;`

カメラのミュート状態を取得します。<br>
※このAPIは使用しないでください。

###引数
なし。

###戻り値
なし。

# V5Client::getMicrophoneVolume
`virtual unsigned int getMicrophoneVolume() = 0;`

マイクデバイスの音量を取得します。

###引数
なし。

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| unsigned int              | -                  | マイク                    |

# V5Client::setMicrophoneVolume
`virtual void setMicrophoneVolume( unsigned int volume ) = 0;`

マイクデバイスの音量を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| unsigned int              | volume             | マイク音量                 |

###戻り値
なし。

# V5Client::getSpeakerVolume
`virtual unsigned int getSpeakerVolume() = 0;`

スピーカーデバイスの音量を取得します。

###引数
なし。

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| unsigned int              | -                  | スピーカー音量              |

# V5Client::setSpeakerVolume
`virtual void setSpeakerVolume( unsigned int volume ) = 0;`

スピーカーデバイスの音量を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| unsigned int              | volume             | スピーカー音量              |

###戻り値
なし。

# V5Client::setProxySetting
`virtual void setProxySetting(std::string const &address, long port, std::string const &id, std::string const &password) = 0;`

Proxyの各種情報を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| const std::string \&      | address            | IPアドレス　　　　　　　　　　　　　　|
| long                      | port               | ポート番号                 |
| const std::string \&      | id                 | ID                       |
| const std::string \&      | password           | パスワード                  |

###戻り値
なし。

# V5Client::setVideoPreferences
`virtual void setVideoPreferences(V5VideoPreferences preferences) = 0;`

会議参加中のビデオの映像品質を変更します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| V5VideoPreferences        | preferences        | ビデオ情報　　　　　　　　　　　　　　|

###戻り値
なし。

# V5Client::getVideoPreferences
`virtual V5VideoPreferences getVideoPreferences() = 0;`

会議参加中のビデオの映像品質状態を取得します。

###引数
なし。

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| V5VideoPreferences        | -                  | ビデオ情報　　　　　　　　　　　　　　|

# V5Client::setSelfDisplayLabel
`virtual void setSelfDisplayLabel(const char *aLavel) = 0;`

ルームで他の参加者に対して表示する名称を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| const char *              | aLavel             | 名称    　　　　　　　　　　　　　　|

###戻り値
なし。

# V5Client::setProxyForce
`virtual void setProxyForce( bool force ) = 0;`

ストリームサーバに対する接続にProxyを使用するかどうかを設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| bool                      | force              | Proxy強制設定有無　　　　　　　|

###戻り値
なし。

# V5Client::setProxyIEOption
`virtual void setProxyIEOption( V5ProxyVidyoIEType option ) = 0;`

Proxy情報の取得方法を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| V5ProxyVidyoIEType        | option             | Proxy情報取得方法   　　　　|

###戻り値
なし。

# V5Client::startStatisticsInfo
`virtual void startStatisticsInfo( int millisecs ) = 0;`<BR>
`virtual void startStatisticsInfo() = 0;`

統計情報の取得を開始します。<br>
※引数を指定しない場合5秒ごとにコールバックにて統計情報を通知します。


###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| int                       | millisecs          | タイムアウト時間　　　　　　　　　　|

###戻り値
なし。

# V5Client::stopStatisticsInfo
`virtual void stopStatisticsInfo() = 0;`

統計情報の取得を終了します。

###引数
なし。

###戻り値
なし。

# V5Client::setSendMaxKBPS
`virtual bool setSendMaxKBPS(int kbps) = 0;`

送信データ伝送速度の最大値を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| int                       | kbps               | 送信データ伝送速度 　　　　　　　|

###戻り値
なし。

# V5Client::getSendMaxKBPS
`virtual int getSendMaxKBPS() = 0;`

送信データ伝送速度の最大値を取得します。

###引数
なし。

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| int                       | -                  | 送信データ伝送速度 　　　　　　　|

# V5Client::setRecvMaxKBPS
`virtual bool setRecvMaxKBPS(int kbps) = 0;`

受信データ伝送速度の最大値を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| int                       | kbps               | 受信データ伝送速度 　　　　　　　|

###戻り値
なし。

# V5Client::getRecvMaxKBPS
`virtual int getRecvMaxKBPS() = 0;`

受信データ伝送速度の最大値を取得します。

###引数
なし。

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| int                       | -                  | 受信データ伝送速度 　　　　　　　|

# V5Client::setSendBandWidth
`virtual bool setSendBandWidth( V5SLayer layer, V5SendBandWidth bandWidth ) = 0;`

帯域最大値を設定します。

###引数
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| V5SLayer                  | layer              | レイヤ　　　　　　　　　　　　|
| V5SendBandWidth           | bandWidth          | 帯域最大値        　　　　　|

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| bool                      | -                  | 設定結果         　　　　　　　|

# V5Client::requestStartRecord
`virtual bool requestStartRecord(std::shared_ptr<IAsyncRequest> const& request) = 0;`

カンファレンスサーバに対して録画開始要求を送信します。

###引数
| Member Type                             | Definition  | Description              |
|-----------------------------------------|-------------|--------------------------|
| const std::shared_ptr\<IAsyncRequest\> \& | request   | 送信結果(Ack/Nack)受信用クロージャ　|

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| bool                      | -                  | 設定結果         　 　　　　|

# V5Client::stopRecord
`virtual bool stopRecord(std::shared_ptr<IAsyncRequest> const& request) = 0;`

カンファレンスサーバに対して録画停止要求を送信します。

###引数
| Member Type                                | Definition      | Description           |
|--------------------------------------------|-----------------|-----------------------|
|| const std::shared_ptr\<IAsyncRequest\> \& | request         | 送信結果(Ack/Nack)受信用クロージャ|

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| bool                      | -                  | 設定結果         　　 　　　|

# V5Client::cancelRecord
`virtual bool cancelRecord(std::shared_ptr<IAsyncRequest> const& request) = 0;`

録画をキャンセルします。

###引数
| Member Type                               | Definition     | Description              |
|-------------------------------------------|----------------|--------------------------|
| const std::shared_ptr\<IAsyncRequest\> \& | request        | 送信結果(Ack/Nack)受信用クロージャ |

###戻り値
| Member Type               | Definition         | Description              |
|---------------------------|--------------------|--------------------------|
| bool                      | -                  | 設定結果                   |

# V5Client::requestRemotePointing
`virtual void requestRemotePointing( std::shared_ptr<IAsyncRequest> const& request ) = 0;`

自分以外のユーザが画面共有時、共有中の画面に対しての指差し機能(リモートポインター)の開始要求を送信します。

###引数
| Member Type                             | Definition         | Description              |
|-----------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \& | request            | ポインタ情報　　　　　　 　　　　　　|

###戻り値
なし。

# V5Client::remotePointing
`virtual void remotePointing( V5PointingInfo const& pointing ) = 0;`<BR>
`virtual void remotePointing( std::shared_ptr<IAsyncRequest> const& request, V5PointingInfo const& pointing ) = 0;`<BR>

リモートポインターを設定します。

###引数
| Member Type                             | Definition         | Description              |
|-----------------------------------------|--------------------|--------------------------|
| const V5PointingInfo \&                 | pointing           | ポインタ　　　  　　　 　　　　　　　|
| const std::shared_ptr<IAsyncRequest> \& | request            | ポインタ情報　　　　　 　　　　　　　|

###戻り値
なし。

# V5Client::releaseRemotePointing
`virtual void releaseRemotePointing( std::shared_ptr<IAsyncRequest> const& request ) = 0;`

リモートポインターを開放します。

###引数
| Member Type                             | Definition         | Description              |
|-----------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \& | request            | ポインタ情報　　　　　 　　　　　　　|

###戻り値
なし。

# V5Client::getFeaturePermissions
`virtual FeatureFlagsPtrT getFeaturePermissions() const = 0;`

デバイスの権限を取得します。

###引数
なし。

###戻り値
| Member Type      | Definition         | Description              |
|------------------|--------------------|--------------------------|
| FeatureFlagsPtrT | -                  | デバイス権限情報 　 　　　　　　　|

# V5Client::getQuickEnqueteList
`virtual V5CODE getQuickEnqueteList(V5EnqueteGetOption const& option) = 0;`

クイックアンケートリストを取得します。

###引数
| Member Type                 | Definition         | Description              |
|-----------------------------|--------------------|--------------------------|
| const V5EnqueteGetOption \& | option             | アンケート情報   　 　　　　　　　|

###戻り値
V5CODE型のエラーコードが戻ります。

# V5Client::getQuickEnqueteDetail
`virtual V5CODE getQuickEnqueteDetail( std::string const& enqueteID ) = 0;`

クイックアンケートリストの結果を取得します。

###引数
| Member Type                 | Definition         | Description              |
|-----------------------------|--------------------|--------------------------|
| const std::string \&        | enqueteID          | アンケートID     　 　　　　　　　|

###戻り値
V5CODE型のエラーコードが戻ります。

# V5Client::createQuickEnquete
`virtual V5CODE createQuickEnquete( V5EnqueteInfo const& enquete, unsigned int attrCount, std::vector<std::string> const& attrList, bool save) = 0;`

クイックアンケートリストを作成します。

###引数
| Member Type                       | Definition         | Description              |
|-----------------------------------|--------------------|--------------------------|
| const V5EnqueteInfo \&            | enquete            | アンケート情報   　 　　　　　　　|
| unsigned int                      | attrCount          | 属性カウンタ     　 　　　　　　　|
| const std::vector<std::string> \& | attrList           | アンケート内容リスト配列  　　　　　　　|
| bool                              | save               | 保存するかどうか  　 　　　　　　　|

###戻り値
V5CODE型のエラーコードが戻ります。

# V5Client::requestStartEnquete
`virtual bool requestStartEnquete( std::shared_ptr<IAsyncRequest> const& request, std::string const& enqueteID, V5EnqueteInfo const& enqueteInfo, unsigned int attributeCount, std::vector<V5EnqueteAttribute> const& attrList, bool save ) = 0;`

クイックアンケートリストを作成します。

###引数
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \&  | request            | 動画情報       　 　　　　　　　|
| const std::string \&                     | enqueteID          | アンケートID     　 　　　　　　　|
| const V5EnqueteInfo \&                   | enqueteInfo        | アンケート情報     　　　　　　　|
| unsigned int                             | attributeCount     | 属性カウンタ     　 　　　　　　　|
| const std::vector<V5EnqueteAttribute> \& | attrList           | アンケート内容リスト配列  　 　　|
| bool                                     | save               | 保存するかどうか  　 　　　　　　　|

###戻り値
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| bool                                     | -                  | 結果          　 　　　　　　　|

# V5Client::answerEnquete
`virtual bool answerEnquete( std::shared_ptr<IAsyncRequest> const& request, std::string const& enqueteID, V5EnqueteAnswerRequest const& answerEnquete ) = 0;`

アンケートの回答を取得します。

###引数
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \&  | request            | 動画情報       　 　　　　　　　|
| const std::string \&                     | enqueteID          | アンケートID     　 　　　　　　　|
| const V5EnqueteAnswerRequest \&          | answerEnquete      | アンケート回答   　 　　　　　　　|

###戻り値
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| bool                                     | -                  | 結果          　 　　　　　　　|

# V5Client::pauseEnquete
`virtual bool pauseEnquete( std::shared_ptr<IAsyncRequest> const& request, std::string const& enqueteID ) = 0;`

アンケートを中断します。

###引数
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \&  | request            | 動画情報       　 　　　　　　　|
| const std::string \&                     | enqueteID          | アンケートID     　 　　　　　　　|

###戻り値
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| bool                                     | -                  | 結果          　 　　　　　　　|

# V5Client::resumeEnquete
`virtual bool resumeEnquete( std::shared_ptr<IAsyncRequest> const& request, std::string const& enqueteID ) = 0;`

アンケートを再開します。

###引数
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \&  | request            | 動画情報       　 　　　　　　　|
| const std::string \&                     | enqueteID          | アンケートID     　 　　　　　　　|

###戻り値
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| bool                                     | -                  | 結果          　 　　　　　　　|

# V5Client::finishEnquete
`virtual bool finishEnquete( std::shared_ptr<IAsyncRequest> const& request, std::string const& enqueteID, bool save ) = 0;`

アンケートを終了します。

###引数
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \&  | request            | 動画情報       　 　　　　　　　|
| const std::string \&                     | enqueteID          | アンケートID     　 　　　　　　　|
| bool                                     | save               | 保存するかどうか  　 　　　　　　　|

###戻り値
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| bool                                     | -                  | 結果          　 　　　　　　　|

# V5Client::noticeAnswerEnquete
`virtual bool noticeAnswerEnquete( std::shared_ptr<IAsyncRequest> const& request, std::string const& enqueteID ) = 0;`

アンケートの回答を通知します。

###引数
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \&  | request            | 動画情報       　 　　　　　　　|
| const std::string \&                     | enqueteID          | アンケートID     　 　　　　　　　|

###戻り値
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| bool                                     | -                  | 結果          　 　　　　　　　|

# V5Client::noticeAnswerClose
`virtual bool noticeAnswerEnquete( std::shared_ptr<IAsyncRequest> const& request, std::string const& enqueteID ) = 0;`

アンケートの回答終了を通知します。

###引数
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| const std::shared_ptr<IAsyncRequest> \&  | request            | 動画情報       　 　　　　　　　|
| const std::string \&                     | enqueteID          | アンケートID     　 　　　　　　　|

###戻り値
| Member Type                              | Definition         | Description              |
|------------------------------------------|--------------------|--------------------------|
| bool                                     | -                  | 結果          　 　　　　　　　|