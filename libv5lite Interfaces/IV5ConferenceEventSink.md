# class IV5ConferenceEventSink

カンファレンスのリスナー Interface です。  
View 側で IV5ConferenceEventSink インターフェイスを
実装したクラスを V5Client に渡すことで通知を受け取ります。

##Public member types

| Member Type                | Definition       |
|----------------------------|------------------|
| V5Participant\*            | Participant      |
| V5ConferenceInfo\*         | conferenceInfo   |
| VideoRawFrame\&            | rawFrame         |
| V5ConferenceStatus\*       | conferenceStatus |
| V5ErrorInfo                | ErrorInfo        |

※詳細は「globals.md」資料を参照。

## IV5ConferenceEventSink::~IV5ConferenceEventSink
`virtual ~IV5ConferenceEventSink();`

デストラクタです。  
リスナーが消滅した際に呼ばれます。 

## IV5ConferenceEventSink::onSessionLinked
`virtual void onSessionLinked(const char *displayName, any_arg logoUrl) = 0;`

ポータルサーバーとの接続に成功した際に通知します。

###引数

| Member Type    | Definition   | Description      |
|----------------|--------------|------------------|
| const char *   | displayName  | 表示名　　         |
| any_arg        | logoUrl      | ロゴURI           |

## IV5ConferenceEventSink::onBeginSessionFailed
`virtual void onBeginSessionFailed(V5ErrorInfo const& errInfo) = 0;`

ポータルサーバーとの接続に失敗した際に通知します。

###引数

| Member Type          | Definition   | Description      |
|----------------------|--------------|------------------|
| const V5ErrorInfo\&  | errInfo      | エラー情報         |

## IV5ConferenceEventSink::onVersionUnmatched
`virtual void onVersionUnmatched( const char *requireVersion ) = 0;`

起動しているアプリのバージョン番号が最低バージョン番号以下の場合に通知します。

###引数

| Member Type   | Definition      | Description      |
|---------------|-----------------|------------------|
| const char *  | requireVersion  | バージョン　         |

## IV5ConferenceEventSink::onParticipantJoined
`virtual void onParticipantJoined(V5Participant const& participant) = 0;`

会議室に入室してきた参加者を通知します。

###引数

| Member Type      | Definition  | Description  |
|------------------|-------------|--------------|
| V5Participant\&  | participant | 入室者情報    |

## IV5ConferenceEventSink::onParticipantLeaved
`virtual void onParticipantLeaved(V5Participant const& participant) = 0;`

会議室から退室した参加者を通知します。

###引数

| Member Type      | Definition    | Description |
|------------------|---------------|-------------|
| V5Participant\&  | participant   | 退室者情報   |

## IV5ConferenceEventSink::onReceiveJoinConferenceRequest
`virtual void onReceiveJoinConferenceRequest(const char *token, const char *V5LiteURL) = 0;`

action_joinConferenceの送信タイミングで通知します。

###引数

| Member Type  | Definition   | Description      |
|--------------|--------------|------------------|
| const char * | token        | 入室時使用トークン   |
| const char * | V5LiteURL    | 接続先URI         |


## IV5ConferenceEventSink::onJoinConferenceFailed
`virtual void onJoinConferenceFailed(V5ErrorInfo const& errInfo) = 0;`

ルームへの入室に失敗した場合に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const V5ErrorInfo\&   | errInfo      | エラー情報         |

## IV5ConferenceEventSink::onReceiveVideoFrame
`virtual void onReceiveVideoFrame(VideoRawFrame const& rawFrame) = 0;`

会議入室中のユーザ映像および会議内で画面共有されている映像のRawFrameデータを通知します。

###引数

| Member Type      | Definition | Description    |
|------------------|------------|----------------|
| VideoRawFrame\&  | rawFrame   | RawFrameデータ  |

## IV5ConferenceEventSink::onJoinedConference
`virtual void onJoinedConference(V5ConferenceInfo const& conferenceInfo) = 0;`

会議への入室が完了した際に通知します。

###引数

| Member Type                | Definition       | Description                   |
|----------------------------|------------------|-------------------------------|
| const V5ConferenceInfo\&   | conferenceInfo   | 会議情報、参加者リスト、自分の情報  |

## IV5ConferenceEventSink::onLeavedConference
`virtual void onLeavedConference(V5ErrorInfo const& errInfo) = 0;`

会議からの退室が完了した際に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const V5ErrorInfo\&   | errInfo      | エラー情報         |

## IV5ConferenceEventSink::onParticipantListUpdated
`virtual void onParticipantListUpdated( const V5Participant * const participants[], unsigned long length ) = 0;`

会議の参加者リストが更新された際に通知します。

###引数

| Member Type                | Definition         | Description     |
|----------------------------|--------------------|-----------------|
| const V5Participant *      | const participants | 参加者リスト配列   |
| unsigned long              | length             | 参加者リスト数     |

## IV5ConferenceEventSink::onConferenceStatusUpdated
`virtual void onConferenceStatusUpdated(V5ConferenceStatus const& conferenceStatus) = 0;`

会議室情報が更新された際に通知します。

###引数

| Member Type                | Definition        | Description |
|----------------------------|-------------------|-------------|
| const V5ConferenceStatus\& | conferenceStatus  | ルーム情報    |

## IV5ConferenceEventSink::onAddShare
`virtual void onAddShare(const char *uri, bool isSelfShare) = 0;`

会議内で画面/画面共有が実行された際に通知します。

###引数

| Member Type      | Definition  | Description      |
|------------------|-------------|------------------|
| const char *     | uri         | 資料共有元URI     |
| bool 　　　　　　     | isSelfShare | 共有元が自分かどうか |

## IV5ConferenceEventSink::onRemoveShare
`virtual void onRemoveShare(const char *uri, bool isSelfShare) = 0;`

会議内での表示されている画面/画像共有が終了された際に通知します。

###引数

| Member Type      | Definition  | Description      |
|------------------|-------------|------------------|
| const char *     | uri         | 資料共有元URI     |
| bool 　　　　　　     | isSelfShare | 共有元が自分かどうか |

## IV5ConferenceEventSink::onWatchShare
`virtual void onWatchShare(std::string const& uri) = 0;`

PC画面共有が開始された際に通知します。

###引数

| Member Type            | Definition  | Description      |
|------------------------|-------------|------------------|
| const std::string\&    | uri         | 画面共有元URI     |

## IV5ConferenceEventSink::onUnwatchShare
`virtual void onUnwatchShare() = 0;`

PC画面共有を終了した際に通知します。

## IV5ConferenceEventSink::onRemoteSourceAdded
`virtual void onRemoteSourceAdded(V5RemoteSourceChanged const& change) = 0;`

会議内で画面/資料共有が追加された際に通知します。

###引数

| Member Type              | Definition  | Description      |
|--------------------------|-------------|------------------|
| V5RemoteSourceChanged\&  | change      | 共有資料情報      |

## IV5ConferenceEventSink::onRemoteSourceRemoved
`virtual void onRemoteSourceRemoved(V5RemoteSourceChanged const& change) = 0;`

会議内で画面/資料共有が終了された際に通知します。

###引数

| Member Type              | Definition  | Description      |
|--------------------------|-------------|------------------|
| V5RemoteSourceChanged\&  | change      | 共有資料情報      |

## IV5ConferenceEventSink::onSelectedParticipantsUpdated
`virtual void onSelectedParticipantsUpdated(std::vector<std::string> const& selectedURI) = 0;`

会議入室中に人数に変更があった際に通知します。

###引数

| Member Type                        | Definition     | Description      |
|------------------------------------|----------------|------------------|
| const std::vector<std::string> \&  | selectedURI    | ルームURI情報      |

## IV5ConferenceEventSink::onReconnectConference
`virtual void onReconnectConference( V5ConferenceInfo const& conferenceInfo ) = 0;`

ルームに再接続を開始した際に通知します。

###引数

| Member Type                | Definition        | Description      |
|----------------------------|-------------------|------------------|
| const V5ConferenceInfo \&  | conferenceInfo    | ルーム情報         |

## IV5ConferenceEventSink::onAsyncError
`virtual void onAsyncError(V5ErrorInfo const& errInfo) = 0;`

会議への参加中に発生したエラーを通知します。<br>
※このAPIでエラーを通知した場合でも会議への参加は継続します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const V5ErrorInfo \&  | errInfo      | エラー情報         |

## IV5ConferenceEventSink::onInviteMailIsSent
`virtual void onInviteMailIsSent(V5ErrorInfo const& errInfo, const char* status, const char* mailAddress) = 0;`

メールによる会議招待URL送信の結果を通知します。

###引数

| Member Type          | Definition   | Description      |
|----------------------|--------------|------------------|
| const V5ErrorInfo \& | errInfo      | エラー情報         |
| const char *         | status       | 状態   　         |
| const char *         | mailAddress  | メールアドレス       |

## IV5ConferenceEventSink::onFloatingWindow
`virtual void onFloatingWindow(V5WindowId window) = 0;`

PC画面共有を開始および終了した際に、表示に使用していたWindowのIDを通知します。

###引数

| Member Type | Definition   | Description      |
|-------------|--------------|------------------|
| V5WindowId  | window       | windowId         |

## IV5ConferenceEventSink::onRcvRejectParticipant
`virtual void onRcvRejectParticipant( V5Participant const& participant ) = 0;`

自身が会議から強制退室させられた際に強制退室を実行したユーザ情報を通知します。

###引数

| Member Type             | Definition   | Description      |
|-------------------------|--------------|------------------|
| const V5Participant \&  | participant  | 参加者情報        |

## IV5ConferenceEventSink::onPastTimeStart
`virtual void onPastTimeStart() = 0;`

参加中会議の時間計測開始を通知します。

## IV5ConferenceEventSink::onPastTimeStop
`virtual void onPastTimeStop() = 0;`

参加中会議の時間計測停止を通知します。

## IV5ConferenceEventSink::onConferenceNameChangeFailed
`virtual void onConferenceNameChangeFailed( const char* nowConferenceName ) = 0;`

参加中会議の会議名が変更された際に通知します。

###引数

| Member Type   | Definition            | Description      |
|---------------|-----------------------|------------------|
| const char*   | nowConferenceName     | 現在のルーム名　　　　　|

## IV5ConferenceEventSink::onNowMaintenance
`virtual void onNowMaintenance( long long maintenanceStartTime, long long maintenanceEndTime ) = 0;`

会議入室を実行した際にメンテナンス実施中状態である事を通知します。<br>
※引数にメンテナンスの開始と終了時間を設定します。

###引数

| Member Type   | Definition            | Description      |
|---------------|-----------------------|------------------|
| long long     | maintenanceStartTime  | 開始時間          |
| long long     | maintenanceEndTime    | 終了時間          |

## IV5ConferenceEventSink::onConferenceStatisticsNotice
`virtual void onConferenceStatisticsNotice( V5ConferenceStatisticsAllInfo const& confStatInfo ) = 0;`

ルーム統計情報を通知します。

###引数

| Member Type                           | Definition    | Description      |
|---------------------------------------|---------------|------------------|
| const V5ConferenceStatisticsAllInfo\& | confStatInfo  | ルーム統計情報     |

## IV5ConferenceEventSink::onConferenceEndTimeOverNotice
`virtual void onConferenceEndTimeOverNotice(long minutes) = 0;`

参加中の会議の終了時間を超過した際に超過時間を5分区切りで通知します。

###引数

| Member Type   | Definition  | Description      |
|---------------|-------------|------------------|
| long          | minutes     | 経過時間          |

## IV5ConferenceEventSink::onLibFatalError
`virtual void onLibFatalError(V5ErrorInfo const& errInfo) = 0;`

ライブラリ内にて入室処理状態の変更に失敗した際に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const V5ErrorInfo \&  | errInfo      | エラー情報         |

## IV5ConferenceEventSink::onResultRecordStatus
`virtual void onResultRecordStatus(V5ResultRecordStatusInfo const& recStatus) = 0;`

現在の録画関連(録画開始/中止/終了処理実施)の状態を通知します。

###引数

| Member Type                        | Definition   | Description      |
|------------------------------------|--------------|------------------|
| const V5ResultRecordStatusInfo \&  | recStatus    | 録画状態          |

## IV5ConferenceEventSink::onSharingWindowPointingApproval
`virtual void onSharingWindowPointingApproval( std::string const& uri, V5Size const& info ) = 0;`

自分以外のユーザによるPC画面共有が実行された際の共有画面に対する指差し機能の実行が許可されている場合に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const std::string \&  | uri          | 画面共有元URI     |
| const V5Size \&       | info         | 画面サイズ情報 　　　　|

## IV5ConferenceEventSink::onSharingWindowPointingDisapproval
`virtual void onSharingWindowPointingDisapproval( std::string const& uri ) = 0;`

自分以外のユーザによるPC画面共有が実行された際の共有画面に対する指差し機能の実行が許可されていない場合に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const std::string \&  | uri          | 画面共有元URI     |

## IV5ConferenceEventSink::onRemotePointerObjectAdded
`virtual void onRemotePointerObjectAdded( std::shared_ptr<IRemotePointer> pointer, std::string const& displayName ) = 0;`

自分が実行しているPC画面共有の共有画面に対する他ユーザの指差し機能が実行され、<br>
自PC画面上に指差しポインタを生成する必要がある場合に通知します。

###引数

| Member Type                      | Definition   | Description      |
|----------------------------------|--------------|------------------|
| std::shared_ptr<IRemotePointer>  | pointer      | ポインタ            |
| const std::string \&             | displayName  | 表示名            |

## IV5ConferenceEventSink::onEnqueteListIsFailed
`virtual void onEnqueteListIsFailed(V5ErrorInfo const& errInfo) = 0;`

ポータルサーバへのアンケートリストデータ取得要求送受信処理に失敗した際に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const V5ErrorInfo \&  | errInfo      | エラー情報         |

## IV5ConferenceEventSink::onEnqueteListIsSuccess
`virtual void onEnqueteListIsSuccess(std::vector<V5Enquete> const& enqueteList) = 0;`

ポータルサーバへのアンケートリストデータ取得要求の送受信処理に成功した際に通知します。

###引数

| Member Type                      | Definition   | Description      |
|----------------------------------|--------------|------------------|
| const std::vector<V5Enquete> \&  | enqueteList  | アンケート情報リスト   |

## IV5ConferenceEventSink::onEnqueteDetailIsFailed
`virtual void onEnqueteDetailIsFailed(V5ErrorInfo const& errInfo) = 0;`

ポータルサーバへのアンケート詳細データ取得要求の送受信処理に失敗した際に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const V5ErrorInfo \&  | errInfo      | エラー情報         |

## IV5ConferenceEventSink::onEnqueteDetailIsSuccess
`virtual void onEnqueteDetailIsSuccess( V5Enquete const& enquete, std::vector<V5EnqueteAttribute> const& attrList) = 0;`

ポータルサーバへのアンケート詳細データ取得要求の送受信処理に成功した際に通知します。

###引数

| Member Type                               | Definition   | Description      |
|-------------------------------------------|--------------|------------------|
| const V5Enquete \&                        | enquete      | アンケート情報　　　　　　|
| const std::vector<V5EnqueteAttribute> \&  | attrList     | 属性　　　　　　　　　　　　|

## IV5ConferenceEventSink::onCreateEnqueteIsFailed
`virtual void onCreateEnqueteIsFailed(V5ErrorInfo const& errInfo) = 0;`

ポータルサーバへの会議内で作成した新規アンケートデータ通知要求の送受信に失敗した際に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const V5ErrorInfo \&  | errInfo      | エラー情報         |

## IV5ConferenceEventSink::onCreateEnqueteIsSuccess
`virtual void onCreateEnqueteIsSuccess( std::string const& enqueteID) = 0;`

ポータルサーバへの会議内で作成した新規アンケートデータ通知要求の送受信に成功した際に通知します。

###引数

| Member Type           | Definition   | Description      |
|-----------------------|--------------|------------------|
| const std::string \&  | enqueteID    | アンケートID        |

## IV5ConferenceEventSink::onEnqueteQuestionStart
`virtual void onEnqueteQuestionStart( std::string const& enqueteID, V5EnqueteInfo const & enqueteInfo, unsigned int attrCount, std::vector<V5EnqueteAttributeLabel> const& attrLabelList, bool pauseAnswerEnquete) = 0;`

会議内でアンケートをが開始された場合にアンケートの質問および回答情報を通知します。

###引数

| Member Type                                     | Definition         | Description        |
|-------------------------------------------------|--------------------|--------------------|
| const std::string \&                            | enqueteID          | アンケートID          |
| const V5EnqueteInfo \&                          | enqueteInfo        | アンケート情報        |
| unsigned int                                    | attrCount          | 属性カウンタ          |
| const std::vector<V5EnqueteAttributeLabel> \&   | attrLabelList      | アンケート内容リスト配列 |
| bool                                            | pauseAnswerEnquete | アンケート中断フラグ     |

## IV5ConferenceEventSink::onEnqueteAnswerPause
`virtual void onEnqueteAnswerPause( std::string const& enqueteID ) = 0;`

実施中のアンケートが中断された事を通知します。

###引数

| Member Type           | Definition         | Description        |
|-----------------------|--------------------|--------------------|
| const std::string \&  | enqueteID          | アンケートID          |

## IV5ConferenceEventSink::onEnqueteAnswerResume
`virtual void onEnqueteAnswerResume( std::string const& enqueteID, V5EnqueteInfo const & enqueteInfo,　unsigned int attrCount, std::vector<V5EnqueteAttributeLabel> const& attrLabelList ) = 0;`

中断中のアンケートが再開された際に通知します。

###引数

| Member Type                                     | Definition         | Description        |
|-------------------------------------------------|--------------------|--------------------|
| const std::string \&                            | enqueteID          | アンケートID          |
| const V5EnqueteInfo \&                          | enqueteInfo        | アンケート情報        |
| unsigned int                                    | attrCount          | 属性カウンタ          |
| const std::vector<V5EnqueteAttributeLabel> \&   | attrLabelList      | アンケート内容リスト配列 |

## IV5ConferenceEventSink::onEnqueteAnswerFinish
`virtual void onEnqueteAnswerFinish( std::string const& enqueteID ) = 0;`

実施されているアンケートが終了された場合に通知します。

###引数

| Member Type           | Definition         | Description        |
|-----------------------|--------------------|--------------------|
| const std::string \&  | enqueteID          | アンケートID          |

## IV5ConferenceEventSink::onEnqueteAnswerAggregate
`virtual void onEnqueteAnswerAggregate( std::string const& enqueteID, V5EnqueteInfo const& enqueteInfo, int numAnswers, std::vector<int> const& aggregate, unsigned int attrCount, std::vector<V5EnqueteAnswerAggregate> const& answerList, std::vector<V5EnqueteAnswerUnanswered> const& unAnswerList, bool mismatchVer) = 0;`

アンケート実施直後もしくは回答者がアンケートに回答した際にアンケート集計結果を質問者に送信した際に通知します。<br>
※サーバー側のバージョンが古く、未回答者リスト配列が存在しない場合には、**mismatchVer**フラグにて**true**が返されます。

###引数

| Member Type                                     | Definition         | Description             |
|-------------------------------------------------|--------------------|-------------------------|
| const std::string \&                            | enqueteID          | アンケートID               |
| const V5EnqueteInfo \&                          | enqueteInfo        | アンケート情報             |
| int                                             | numAnswers         | 回答者数                |
| const std::vector<V5EnqueteAnswerAggregate> \&  | answerList         | 回答者リスト配列    　　　　　|
| const std::vector<V5EnqueteAnswerUnanswered> \& | unAnswerList       | 未回答者リスト配列    　　　 |
| bool                                            | mismatchVer        | サーバーバージョン確認フラグ   |

## IV5ConferenceEventSink::onEnqueteAnswerAggregateEnd
`virtual void onEnqueteAnswerAggregateEnd() = 0;`

アンケート結果を閲覧する権限があるユーザに対してアンケート結果の公開が終了したことを通知します。

## IV5ConferenceEventSink::onParticipantLimitInfo
`virtual void onParticipantLimitInfo( V5AppParticipantLimit const& participantlimit ) = 0;`

会議への参加者上限人数が変更されそうな状況になった場合(CPU高負荷、NWの状態による帯域制限等)、現状の各種状態(ユーザ指定上限、CPU性能上限、帯域制限による上限)の最大参加可能人数を通知します。

###引数

| Member Type                     | Definition         | Description        |
|---------------------------------|--------------------|--------------------|
| const V5AppParticipantLimit \&  | participantlimit   | 参加者情報（最大値）|

## IV5ConferenceEventSink::onCPUUsageWarningInfo
`virtual void onCPUUsageWarningInfo( V5AppCPUWarning const& cpuWarning ) = 0;`

現在のCPU使用率(通常or高負荷の2択)の状況下で発生している警告情報を通知します。

###引数

| Member Type               | Definition   | Description        |
|---------------------------|--------------|--------------------|
| const V5AppCPUWarning \&  | cpuWarning   | CPU使用率情報       |

## IV5ConferenceEventSink::onStreamAlarmInfo
`virtual void onStreamAlarmInfo( V5StreamAlarmInfo const& streamAlarmInfo ) = 0;`

ライブラリ内で測定している各種情報に関連する警告を通知します。

###引数

| Member Type                 | Definition        | Description        |
|-----------------------------|-------------------|--------------------|
| const V5StreamAlarmInfo \&  | streamAlarmInfo   | 警告内容            |
