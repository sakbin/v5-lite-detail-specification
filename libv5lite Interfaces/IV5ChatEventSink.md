# class IV5ChatEventSink

ルームチャットのリスナー Interface です。  

##Public member types

| Member Type                       | Definition                             |
|-----------------------------------|----------------------------------------|
| ChatMessage                       | V5ChatMessage                          |

## IV5ChatEventSink::~IV5ChatEventSink
`virtual ~IV5ChatEventSink();`

デストラクタです。  
リスナーが消滅した際に呼ばれます。  

## IV5ChatEventSink::onChatMessageAdded
`virtual void onChatMessageAdded( const V5ChatMessage& message ) = 0;`

ルームチャットへの発言通知を受信します。
自身の発言も通知されます。

###引数
####messages
新規に追加された発言内容。

## IV5ChatEventSink::onChatLogUpdated
`virtual void onClearChatLogUpdated( const V5ChatMessage * const messages[], unsigned long length ) = 0;`

ルームチャットの内容が更新された旨の通知を受信します。  
この通知は、受信済みのメッセージがクリアされ、引数 messages によって上書きされたことを意味します。

###引数
####messages
すべての発言情報へのポインタの配列。

####length
引数 messages の配列長（個数）。

## IV5ChatEventSink::onErrorNotification
`virtual void onErrorNotification() = 0;`

エラー通知を受信します。