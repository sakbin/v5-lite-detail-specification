#Conference Server API

##Conference Server 内部の処理

###・会議経過時間
会議経過時間とは、あるカンファレンスにユーザが２名以上いる状態の時間の積算を示します。  
例えば下記のケースの場合

イベント  | 会議参加数| 時間 |概要
----------|--- | ------|-------
UserAが入室 |1名 | 0:00 |経過時間のカウントは行わない
UserBが入室 |2名 | 0:10 |経過時間のカウント開始
UserCが入室 |3名 | 0:20 |経過時間のカウント対象時間内
UserAが退出 |2名 | 0:30 |経過時間のカウント対象時間内
UserBが退出 |1名 | 0:40 |経過時間のカウント対象外へ
UserDが入室 |2名 | 0:50 |経過時間のカウント対象時間内
UserDが退出 |1名 | 0:55 |経過時間のカウント対象時間内
UserBが退出 |0名 | 1:15 |経過時間のカウント対象外へ

カウントの対象時間となるのは 0:10 ~ 0:40 の間と 0:50 ~ 0:55 となり
トータルの会議経過時間は 35分間 という事になります。

###・予約会議
ユーザ入室直前にPotalから通知される会議情報には、  
1 ) 予約会議であるかの有無  
2 ) 予約の開催時間  
3 ) 予約の終了時間  
4 ) その会議が、予約時間を超えて継続してよいかのフラグ

が含まれており、カンファレンスサーバは各予約がされているカンファレンスについて
下記の処理をする必要があります

1 ) 予約は５分単位での開催なので、次のきりの良い時間までタイマーをセットする  
2 ) 以降５分事に予約終了時間を確認する  
3 ) 予約終了時間 10分前になったらユーザへ残り時間を通知する  
4 ) 予約終了時間 5分前になったらユーザへ残り時間を通知する  
5 ) 時間延長が禁止されていた場合、予約時間を過ぎたら直ちに会議を終了させる  
6 ）時間延長が禁止されていない場合、以降通知は行わない

###・会議の情報
会議は下記の状態・情報をもちます 

データ  | 会議中の変更| パーティシパントへの通知タイミング
----------|--- | ------
会議名|YES|入室時 / 再入室時 / 変更時
会議室名|NO|入室時 / 再入室時  
予約会議かどうかのフラグ|NO|入室時 / 再入室時
会議終了時間を超えて<br>会議を続けてよいかのフラグ|NO|通知せず
会議開始時刻|NO|入室時 / 再入室時
会議終了時刻|NO|入室時 / 再入室時
招待ログイン用のURL|YES|入室時 / 再入室時
招待ログイン用のPINコード|YES|入室時 / 再入室時
会議人数上限数|NO|通知せず
会議経過時間|YES|入室時 / 再入室時
現在画面共有中のユーザ|YES|入室時 / 再入室時 / 変更時
現在録画中かどうか|YES|入室時 / 再入室時 / 変更時
予約会議終了時刻までの残り時間|YES|入室時 / 再入室時 / 10分前 / 5分前
参加中のリスト|YES|入室時 / 再入室時 / 変更時
チャット履歴|YES|入室時 / 再入室時
新規参加者を拒否しているか|YES|更新時

入室時 / 再入室時に通知が必要なものは***Established***イベントで  
更新時 通知が必要なもののうち参加者リストは、***ParticipantList***イベントで  
それ以外は、***ConferenceStatusUpdate***で通知を行います

###・ゲストに対する処理
**※本機能は操作権限処理の追加（後述）に伴い、参照は不要となる。  
ただし、下位バージョンとの互換性保持のため、各端末に通知するものとする。**
ConferenceサーバはPrepareParticipantで、接続してくるクライアントが
ゲストかどうかの情報を予め知らされます。Conferenceサーバはゲストに対して下記の制限をかけてください。

制限される機能  | 概要
------------- | -------------
RejectParticipant  | RejectParticipantメッセージをうけとったら<br>メッセージは処理せずに、MethodNotAllowedメッセージを返します。
LockConference  | LockConferenceメッセージをうけとったら<br>メッセージは処理せずに、MethodNotAllowedメッセージを返します。
conferenceURLを隠蔽 | CONFERENCE\_STATUS\_OBJECTの中のconferenceURLを<br>空文字で返します
conferencePinCodeを隠蔽 | CONFERENCE\_STATUS\_OBJECTの中のconferencePinCodeを<br>空文字で返します

###・操作権限について
#### ゲストの扱い
ゲスト機能を表すフラグ isGuest は パーミッションによる機能制限と共通の権限制約をもたらすが、下位バージョンのアプリケーションとの互換性の為、isGuestフラグは各端末に通知するものとする。ただし、サーバの操作処理についてはパーミッションに従う

#### 操作が禁止されたメッセージに対する処理
カンファレンスサーバはisGuestフラグを利用せずに、パーミッションによる操作権限のチェックを行う事

#### パーミッション確定のタイミング
パーティシパント入室直後は全てのパーミッションが 無い状態とみなす

```markup
{
	“conferenceOperation” : "0xffff"//フラグ値を16進数文字列で表現したもの,
	“quickEnquete” : "0xff"//フラグ値を16進数文字列で表現したもの
	“sharing” : "0xff",//フラグ値を16進数文字列で表現したもの
	“deviceOperation” : "0xff",//フラグ値を16進数文字列で表現したもの
	"cameraDevice" : "0xff",
	"microphoneDevice" : "0xff",
	"speakerDevice" : "0xff",
	"roomSetting" : "0xff"
}
```

適切なパーミッションは、action_UpdatParticipantsの戻り値にて通知するものとする

新たに追加されるパーミッションについては下記の通り
####conferenceOperation : 会議内の操作に関するパーミッション
値 | 機能 |　概要 |
-------- | -----|-----
1000000000000000 |--|（予約）
0100000000000000 |--|（予約）
0010000000000000 |--|（予約）
0001000000000000 |--|（予約）
0000100000000000 |--|（予約）
0000010000000000 |電話会議コールイン(フリーダイヤル)|電話連携のコールイン(フリーダイヤル)が利用可能かを示すパーミッション
0000001000000000 |電話会議コールイン(ローコールイン)|電話連携のコールイン(ローコール)が利用可能かを示すパーミッション
0000000100000000 |電話会議コールイン(ローカル)|電話連携のコールイン(ローカル)が利用可能かを示すパーミッション
0000000010000000 |電話会議コールアウト|電話連携のコールアウトが利用可能かを示すパーミッション
0000000001000000 |テレビ会議連携|テレビ会議連携を利用するかどうかのパーミッション
0000000000100000 |テレビ会議連携コールアウト|Legacyデバイスのコールアウトが可能かを示すパーミッション
0000000000010000 |録画|録画の開始、停止が可能かを示すパーミッション
0000000000001000 |会議名変更|会議名の変更を行えるか示すパーミッション
0000000000000100 |会議ロック|会議中にロックを行えるかを示すパーミッション。<br>ゲストユーザはこのビットが0になっている
0000000000000010 |キック|会議中に他ユーザのキックを行えるかを示すパーミッション。<br>ゲストユーザはこのビットが0になっている
0000000000000001 |招待機能|会議内招待を行えるかを示すパーミッション。<br>ゲストユーザはこのビットが0になっている

####quickEnquete : アンケートに関するパーミッション
値| 機能 |　概要 |
-------- | -----|-----
10000000 |--|（予約 )
01000000 |--|（予約 )
00100000 |--|（予約 )
00010000 |--|（予約 )
00001000 |--|（予約 )
00000100 |操作|アンケートの作成、出題、リスト閲覧が可能となるパーミッション
00000010 |回答権|アンケートの回答権をもっているパーミッション。<br>アンケートの出題先は、出題者以外の該当パーミッションを持ったパーティシパントに対してのみ実行される
00000001 |閲覧権|アンケート結果公開対象となるパーミッションで、アンケート結果の内容は本パーミッションを付与された<br>パーティシパントに対して実行される

####sharing : 画面共有操作に関するパーミッション
値 | 機能 |　概要 |
-------- | -----|-----
10000000 |--|（予約 )
01000000 |--|（予約 )
00100000 |--|（予約 )
00010000 |--|（予約 )
00001000 |共有開始（アプリケーション）|アプリケーション共有を開始するとが可能となるパーミッション
00000100 |共有開始（デスクトップ）|デスクトップ画面の共有を開始することができるパーミッション
00000010 |開始|画面共有側で、遠隔からのポインティングを受け付ける機能が許可されてるパーミッション
00000001 |操作要求|画面共有を行っているパーティシパントに対して、遠隔ポインティングの受け入れを要求することができるパーミッション


####cameraDevice : カメラデバイス操作に関するパーミッション
値 | 機能 |　概要 |
-------- | -----|-----
10000000 |--|（予約)
01000000 |--|（予約)
00100000 |--|（予約)
00010000 |--|（予約)
00001000 |--|（予約)
00000100 |--|（予約)
00000010 |--|（予約)
00000001 |カメラミュート|カメラミュート機能を有効にするためのパーミッション。<br>このパーミッションがないパーティシパントはカメラをミュート状態にすることが出来ない

####microphoneDevice : マイクデバイス操作に関するパーミッション
値 | 機能 |　概要 |
-------- | -----|-----
10000000 |--|（予約)
01000000 |--|（予約)
00100000 |--|（予約)
00010000 |--|（予約)
00001000 |--|（予約)
00000100 |--|（予約)
00000010 |--|（予約)
00000001 |マイクミュート|マイクミュート機能を有効にするためのパーミッション。<br>このパーミッションがないパーティシパントはマイクをミュート状態にすることが出来ない

####speakerDevice : スピーカーデバイス操作に関するパーミッション
値 | 機能 |　概要 |
-------- | -----|-----
10000000 |--|（予約)
01000000 |--|（予約)
00100000 |--|（予約)
00010000 |--|（予約)
00001000 |--|（予約)
00000100 |--|（予約)
00000010 |--|（予約)
00000001 |スピーカーミュート|スピーカーミュート機能を有効にするためのパーミッション。<br>このパーミッションがないパーティシパントはスピーカーをミュート状態にすることが出来ない

###roomSetting : 会議室設定に関するパーミッション
値 | 機能 |　概要 |
-------- | -----|-----
10000000 |--|（予約)
01000000 |--|（予約)
00100000 |--|（予約)
00010000 |--|（予約)
00001000 |--|（予約)
00000100 |--|（予約)
00000010 |--|（予約)
00000001 | チャット機能 | チャットを許可する/チャットを禁止する<br>このパーミッションがない場合チャット機能を利用することができない

###・録画の状態遷移について
サーバは、現在の録画の状態を **IDLE** ,**PREPARE**, **REC** の３つで管理し、その現在の状態とクライアント、カンファレンスからのメッセージを組み合わせて状態を変化させていく

状態名  | 概要 | 遷移可能な状態 |
------------- | --------|-----
IDLE  | 録画を行っていない状態 | PREPARE
PREPARE | 録画要求をPortalに送信し、その応答を待っている状態 | IDLE or REC
REC  | 現在サーバは録画状態にある | IDLE

###・リモートポインティング機能について
####用語説明
#####DesktopSharer ( デスクトップシェアラ / Sharer )
・会議参加者でデスクトップ画面を共有している拠点
#####PointSender ( ポイントセンダー / Sender )
・会議参加者でSharerに対して座標情報をおくる資格を有している拠点
#####バインディング
・会議内でSharerに対してSenderの組み合わせを行う処理。またはその組み合わせ。  
　Senderからの座標情報はバインディングが成立しているSharerに対してのみ送信され、ペアリングが成立していない場合はその要求は無効なものとして処理される。  
　Sharerに座標情報を送信可能なSenderは複数許容され、1:Nの形でバインドが可能。その逆で、Senderが複数のSharerとバインドすることは出来ない

###・クイックアンケートの状態遷移について
サーバは、現在のアンケートの状態を **WAIT** ,**WATCH**, **RESULT** の３つで管理し、その現在の状態とクライアント、カンファレンスからのメッセージを組み合わせて状態を変化させていく

状態名  | 概要 | 遷移可能な状態 |
------------- | --------|-----
WAIT | クイックアンケートを行っていない状態 | WATCH
WATCH | クイックアンケート集計中 | WAIT or RESULT
RESULT | クイックアンケート集計結果表示中 | WAIT or WATCH


##1. HTTP  GET で呼ばれるAPI
###1-1. URL : /api/NumTotalParticipants

###戻り値
Conferenceサーバに接続中のParticipantの総数を出力します。

###副作用
なし

##2. HTTP  POST で呼ばれるAPI
###2-1. URL : /api/PrepareConference

key名  | 概要
------------- | -------------
conferenceID  | 会議を特定する一意のID
conferenceInfo  | 会議情報を示すJSONオブジェクト

###引数
**conferenceInfo** : 会議情報を示すJSONオブジェクトです。
構造体は下記のとおりです

~~~
{
	“conferenceName” : “会議名”
	“roomName” : "会議室名",
	“isReserved” : "true" or "false", //予約会議かどうかのフラグ
	"canExtendPeriod" : "true" or "false",//予約時間を超えて開催してよいか
	"canUseTeleconference" : "true" or "false",//電話会議の利用ができるか
	"conferenceStartTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"conferenceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"nextConferenceStartTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"nextConferenceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)" 
	"maintenanceStartTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"maintenanceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"conferenceURL" : "招待URL",
	"legacyAddress" : {
		sipAddress : "00000000@00.2192.jp" or "", //コールインアドレス（連携オプションが無い場合は空）
		h323Address : "00000000@01.2192.jp" or "", //コールインアドレス（連携オプションが無い場合は空）
	},
	"conferencePinCode" : "会議入室用のPINコード",
	"maxParticipants" : "会議の上限人数",
	"wbConferenceURL" : "資料共有会議入室URL",
	"isRecord" : true or false, // 録画可否フラグ
	"autoRecord" : "" or "auto" or "force", // 自動録画フラグ
	"enqueteSaveMode" : MODE_OF_ENQUETE,
	"bandwidthHasBeenSpecifiedByAdmin" : true or false, // 帯域制限フラグ
	"specifiedVideoQuality" : 0 or 1 ~ 326767, // 帯域制限画質
	"specifiedUpStreamBandwidth" : 0 or 1 ~ 32767, // 帯域制限上り速度	"specifiedDownStreamBandwidth" : 0 or 1 ~ 32767, // 帯域制限下り速度}
~~~
####MODE\_OF\_ENQUETE 表

値  | 概要
------------- | -------------
0 (00b)  | ユーザは保存をする・しないを任意で選択でき、デフォルトはOFF(保存しない)
1 (01b)  | ユーザは保存をする・しないを任意で選択でき、デフォルトはON(保存する)
2 (10b)  | ユーザは保存する・しないの選択はできずに、会議ではアンケートの保存は無効
3 (11b)  | ユーザは保存する・しないの選択はできずに、会議ではアンケート結果を強制保存

###戻り値
値の保持が成功した場合は http status 200 と 1 という文字列。  
保持が失敗した場合は、http status 500 を返します。

###副作用
Conferenceサーバは、クライアントが接続してくるまでの有効時間を設けます。  
一定時間以内（60 sec）に クライアント側から接続が無かった場合、  
このConferenceは無効なものとした上で、該当するConferenceへの接続は
以降無効としてください。

###2-2. URL : /api/PrepareParticipant

key名  | 概要
------------- | -------------
token  | 接続を許可するための文字列
conferenceID | 会議を特定する一意のID
isGuest  | ゲスト接続かどうかを判定する文字列
isExcludePortCount  | ポート数カウントするかどうかを判定する
isLegacy | Legacy端末かどうか判定する
###引数
**token** : 会議入室に対して、有効な接続かどうかを判断させるための  
tokenを予め通知します。Conferenceサーバはこのtokenを接続URLに含まない  
WebSocket クライアント、またはLegacy端末からの接続を許可しません  
後述となる"isLegacy"フラグにて、WebSocketクライアント、またはLegacy端末を判別します  
**conferenceID** : 入室する会議IDとして扱います  
**isGuest** : 1 がセットしてある場合パーティシパントはゲストとして扱われます<br>それ以外の文字列もしくは値がセットされていない場合は通常ユーザとして扱います  
**isExcludePortCount** : 1 がセットしてある場合パーティシパントは参加者の人数からは除外  
**isLegacy** : 1 がセットしてある場合はLegacy端末として扱う  

###戻り値

値の保持が成功した場合は http status 200 と 1 という文字列。  
保持が失敗した場合は、http status 500 を返します。

=======

###副作用
Conferenceサーバは、クライアントが接続してくるまでの有効時間を設けます。  
一定時間以内（ ※暫定で10 sec ）に クライアント側から接続が無かった場合、  
このTokenは無効なものとした上で、ポータルサイトへログアウトの通知（※後述）を行って下さい。

クライアントがサーバに接続するときのURLは下記の方式となります
###url :: wss://CONF\_SERVER\_FQDN:PORT/CONFERENCE_ID/

key名  | 概要
------------- | -------------
method  | 接続 or 再接続を表す文字列で connect or reconnectを渡します
pid  | クライアント固有の PIDで、Vidyoライブラリから取得した値です
token  | クライアント固有の tokenで V5 Lite Portalから取得した値です
displayName | 会議中に表示される文字列です
apiVersion | 接続するクライアントが対応するAPIのバージョンです。"x.x" 形式で表現されます。


接続時にクライアントはリクエストクエリ付きのアドレスで接続を試みますので、文字列をパースし、接続の許可/不許可を判断することになります。  
接続の際に渡されるpidとtokenは、その後destを指定するWebSocketAPIやPortalへの通知で利用する  
ユーザを特定するキーとなるのでサーバ側でClientとの結びつきを保持してください。    
apiVersion は、接続するクライアントが対応しているプロトコルのバージョンを宣言します。 このパラメータを省略した場合、"1.0" 未満であると認識されます。

---
###2-3. URL : /api/ConnectConference
Legacy端末の入室を行う場合は当APIをコールします。

key名  | 概要
------------- | -------------
token  | PrepareParticipantにて通知したtoken
conferenceID | 会議を特定する一意のID
displayName  | 会議中に表示される文字列

###戻り値
入室に成功した場合は http status 200 と 1 という文字列を返します。  
会議満室や入室ロックがかかっている場合等、入室に失敗した場合は、http status 500 を返します。

---

###2-4. URL : /api/RemoveConference
Legacy端末の退室を行う場合は当APIをコールします。

key名  | 概要
------------- | -------------
token  | 退室するtoken

###戻り値
退室はカンファレンス側で保証する為、必ずhttp status 200 と 1 という文字列を返却します。

---
###2-5. URL : /api/CloseConference

key名  | 概要
------------- | -------------
conferenceID  | 会議を特定する一意のID


###引数
** conferenceID** : 会議を特定する一意のIDです    

###戻り値
該当するconferenceIDが見つからない場合は 404 http statusを返します
conferenceID が有効な場合 http status 200 と 1 という文字列を返してください


###副作用
Conferenceサーバは、該当するカンファレンスに存在するクライアント全てに対し、切断処理を開始します。
ただし、このAPIによる退出処理ではPortalへのClientの退出通知を行いません。

###2-6. URL : /api/UpdateConference

key名  | 概要
------------- | -------------
conferenceID  | 会議を特定する一意のID
conferenceInfo  | 会議情報を示すJSONオブジェクト

###引数
**conferenceInfo** : 会議情報を示すJSONオブジェクトです。
構造体は下記のとおりです

~~~
{
	"conferenceEndTime" : "" or "0" or "グリニッジ標準時のUNIX Time(秒)",
	"nextConferenceStartTime" : "" or "0" or "グリニッジ標準時のUNIX Time(秒)",
	"nextConferenceEndTime" : "" or "0" or "グリニッジ標準時のUNIX Time(秒)",
	"maintenanceStartTime" : "" or "0" or "グリニッジ標準時のUNIX Time(秒)"
	"maintenanceEndTime" : "" or "0" or "グリニッジ標準時のUNIX Time(秒)"
}
~~~
※ 0の場合は、時間削除。空文字の場合は更新なし。

###戻り値
値の保持が成功した場合は http status 200 と 1 という文字列。  
保持が失敗した場合は、http status 500 を返します。

###2-7. URL : /api/NoticeRecordStatus

key名  | 概要
------------- | -------------
conferenceID  | 会議を特定する一意のID
recordID | 録画開始要求時に送信したカンファレンスが生成した一意のID
result  | 録画要求に対する結果を示すJSONオブジェクト

###引数
**result** : 録画要求に対する結果を示すJSONオブジェクトです。
構造体は下記のとおり

~~~
{
	“result” : “OK" or "FAILD" or "CANCELED"
	“reason” : resultに対する理由コードです
}
~~~

Value of Reason  | 概要
------------- | -------------
200 | 録画成功 かならず result = OK と一緒に渡される
204 | 録画要求がキャンセルされた 必ず result = CANCELEDと一緒に渡される
408 | タイムアウト
500 | V5Liteサーバが処理を正常に処理できなかった
502 | Vidyoサーバが処理を正常に処理できなかった
503 | 録画サーバがBusy状態で録画を開始できなかった
507 | 容量不足により録画を開始することが出来なかった

サーバはこのメッセージを受け取ったあと、RequestStartRecord のACKを投げたParticipant全てに対してResultRecordStatus（後述）イベントを通知します

###2-8. URL : /api/TelephoneConference

key名  | 概要
------------- | -------------
conferenceID  | 会議を特定する一意のID
teleConferenceInfo  | 電話会議情報を示すJSONオブジェクト

###引数
**teleConferenceInfo** : 会議情報を示すJSONオブジェクトです。
構造体は下記のとおりです

~~~
{
	"teleConferenceID" : "電話会議ID",
	"clientID" : "電話会議AdminクライアントID",
	"clientPW" : "電話会議Adminクライアントパスワード",
	"webID" : "電話会議WebID",
	"webPW" : "電話会議Webパスワード"
}
~~~

###戻り値
値の保持が成功した場合は http status 200 と 1 という文字列。  
保持が失敗した場合は、http status 500 を返します。


---
##2. WebSocket API
###WebSocketAPIの構造体

WebSocket APIは基本的に下記の構造体のJSONオブジェクトを  
ConferenceServerに送ることで実現します。

```markup
{
	“event” : EVENT_ID,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : DATA
}
```
###ACK / NACKメッセージ
Conferenceに対して操作要求が送られてきた際に、Confrenceサーバはその操作要求が正しく処理できるかどうかを ACK ( ACKnowledgement : 確認応答 ) もしくは NACK ( Negative ACKnowledgement：否定応答 ) の形で返答します。  
#####※ “ACK” or "NACK" の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “ACK” or "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : パーティシパント送ってきた操作要求の名称
		"foo": recievedEventに対応された拡張フィールドで
		"bar": サーバは対応したデータを付与して送ることができます
	}
}
~~~
###Method : JoinConference

会議へ参加する際、クライアントから通知されます。  
このメソッドをコールしない場合、
Ping の送信は開始されますが Established は通知されません。  
クライアントが接続したのち、一定時間以内に JoinConference が送信されなかった場合、
Conference Server はクライアントとの通信を切断します。  

ただし、接続時の API バージョンが "2.0" 未満の場合は、JoinConference を待機しません。  
接続と同時に会議への参加が実行され、Established が通知されます。  

~~~
{
	“event” : “JoinConference”,
	“from” : SENDER_PARTICIPANT_ID,
	“conference” : "SENDER_PARTICIPANT_ID",
	“data” : {
		"participantStatus": {
			cameraMuted : "true" or "false",
			microphoneMuted : "true" or "false",
		}
	}
}
~~~

---
###EVENT : Established

conferenceへの接続を受け入れた後、
Confrenceサーバはクライアントに所属するconferenceの最新の
パーティシパントリスト、チャット履歴と、カンファレンスの最新の状態を通知します。
このイベントは、connect / reconnect 処理どちらとも
会議への参加を受け付けた後で送信してください。
#####※ Established の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “Established”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"participantList" : LIST_OF_PARTICIPANTS
		"participantStatusList" : LIST_OF_PARTICIPANT_STATUS_LIST (※後述)
		"chatData" : HISTORY_OF_CHAT_DATA_OBJECT（※後述）
		"conferenceStatus" : CONFERENCE_STATUS_OBJECT (※後述)
		"sharingList" : LIST_OF_SHARING (※後述)
	}
}
~~~
#####LIST\_OF\_PARTICIPANTSの構造
```
{
	“numParticipants” : NUMBER_PARTICIPANTS,
	"conferenceID" : ID_OF_COMFERENCE,
	“participants” :[
		{
			“display_name” : NAME_OF_PARTICIPANT,
			“participant_id” : PID_FROM_CLIENT
		},
		{
			“display_name” : NAME_OF_PARTICIPANT,
			“participant_id” : PID_FROM_CLIENT
		},...
	]
}
```
#####LIST\_OF\_PARTICIPANT\_STATUS\_LISTの構造
```
{
	“status” : {
		PARTICIPANT_ID1 : {
			cameraMuted: "true" or "false" or "unknown",
			microphoneMuted: "true" or "false" or "unknown",
		},
		PARTICIPANT_ID2 : {
			cameraMuted: "true" or "false" or "unknown",
			microphoneMuted: "true" or "false" or "unknown",
		},...
	}
}
```
#####CONFERENCE\_STATUS\_OBJECTの構造
~~~
{
	“pastTime” : “会議の経過時間（秒）”,
	“conferenceName” : “会議名”
	“roomName” : "会議室名",
	“isReserved” : "true" or "fale",
	"conferenceStartTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"conferenceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"remainTime" : "予約会議終了までの残り時間（秒）" or "0", //予約がないときは0固定
	"nextConferenceStartTime" : "0" or "グリニッジ標準時のUNIX Time(秒)"	"nextConferenceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)"
	"maintenanceStartTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"maintenanceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
	"maintenanceRemainTime" : "メンテナンス開始までの時間（秒）" or "0", //メンテナンスがないときは0固定
	"conferenceURL" : "招待URL" or "",
	"conferencePinCode" : "会議入室用のPINコード" or "",
	"legacyAddress" : {
		sipAddress : "00000000@00.2192.jp" or "", //コールインアドレス（連携オプションが無い場合は空）
		h323Address : "00000000@01.2192.jp" or "", //コールインアドレス（連携オプションが無い場合は空）
	},
	"documentSender" : "none" or "パーティシパントのPID", // 現在資料共有中のパーティシパントPID
	"isRecoading" : "true" or "false", // 現在録画中かどうかのフラグ
	"autoRecord" : "" or "auto" or "force", // 自動録画フラグ
	"recordStartStatus" : {
		"operate" : "" or "manual" or "auto", // 録画開始契機
		“display_name” : 録画開始実行ユーザ名 or "",
		“participant_id” : 録画開始実行PID or ""
	},
	"enqueteSaveMode" : MODE_OF_ENQUETE, // アンケート保存モード(0 ~ 3)
	"bandwidth" : {
		"isLimited" : true or false, // 帯域制限フラグ		"videoQuality" : 0 or 1 ~ 32767, // 帯域制限画質(0:対象外 1~32767:制限速度)		"upStreamBandwidth" : 0 or 1 ~ 32767,	 // 帯域制限上り速度(0:対象外 1~32767:制限速度)		"downStreamBandwidth" : 0 or 1 ~ 32767, // 帯域制限下り速度(0:対象外 1~32767:制限速度)	}
}
~~~
※ ゲストに対してはconferenceURLとconferencePinCodeが常に空文字で渡されます

#####LIST\_OF\_SHARINGLISTの構造
```
[
	{
		"sharingParticipantID" : SHAREE_PARTICIPANT_ID,
		"sharingURI" : 共有画面のURI,
		"width" : 共有画面の画面解像度を示すInt型の値,
		"height" : 共有画面の画面解像度を示すInt型の値,
		"pointingFlg" : "true" or "false" // ポインティング許可フラグ(true:許可、false:不許可)
	},
	{
		"sharingParticipantID" : SHAREE_PARTICIPANT_ID,
		"sharingURI" : 共有画面のURI,
		"width" : 共有画面の画面解像度を示すInt型の値,
		"height" : 共有画面の画面解像度を示すInt型の値,
		"pointingFlg" : "true" or "false" // ポインティング許可フラグ(true:許可、false:不許可)
	},...
]
```

---
###EVENT : ParticipantList
######このAPIは削除されました
---
###EVENT : ParticipantJoin
参加者が追加された際に、そのパーティシパントの情報を通知します。

#####ParticipantJoin の メッセージ構造体（ Server > Client  )
~~~
{
	“event” : “ParticipantJoin”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"display_name” : NAME_OF_PARTICIPANT,
		“participant_id” : PID_FROM_CLIENT
	}
}
~~~

---
###EVENT : ParticipantLeave
参加者が退出した際に、そのパーティシパントの情報を通知します。

#####ParticipantLeave の メッセージ構造体（ Server > Client  )
~~~
{
	“event” : “ParticipantLeave”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"display_name” : NAME_OF_PARTICIPANT,
		“participant_id” : PID_FROM_CLIENT
	}
}
~~~

---

###EVENT : Ping / Pong

WebSocket コネクションが確立された時点から  
Conferenceサーバは、WebSocket クライアントに対して  
12秒毎に Pingメッセージを送ります。  
Pingメッセージを受け取ったクライアントはサーバにPongメッセージを返してください。  
3回Pongを受け取れなかったConferenceサーバは、action_PrepareRemove
を呼び出し、5回受け取れない場合はaction_RemoveParticipantを実行してください。
#####Ping の メッセージ構造体（ Server > Client  )
```
{
	“event” : “Ping”,
	“from” : "system"
	“dest” : PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : “”
}
```

#####Pong の メッセージ構造体（ Client > Server )
```
{
	“event” : “Pong”,
	“from” : PARTICIPANT_ID
	“dest” : "",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : ""
}
```

###EVENT : ConferenceStatusUpdate
同一conference内にいるメンバー全員に、サーバ側のステータス変更を通知します


#####ConferenceStatusUpdate の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “ConferenceStatusUpdate”,
	“from” : "system"
	“dest” : "all",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		“conferenceName” : "" or “新しい会議名”,
		"conferenceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)"		“remainTime” : "0" or "予約会議終了までの残り時間（秒）",
		"nextConferenceStartTime" : "0" or "グリニッジ標準時のUNIX Time(秒)"		"nextConferenceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)"
		“documentSender” : "" or "none" or "パーティシパントのPID",
		“isRecoading” : "" or "true" or "false",
		"recordStartStatus" : {
			"operate" : "" or "manual" or "auto", // 録画開始契機
			“display_name” : 録画開始実行ユーザ名 or "",
			“participant_id” : 録画開始実行PID or ""
		},
		"recordStopStatus" : {
			"operate" : "" or "manual" or "auto", // 録画停止契機
			“display_name” : 録画停止実行ユーザ名 or "",
			“participant_id” : 録画停止実行PID or ""
		},
		"isLocked" : "" or "true" or "false",
		"maintenanceStartTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",,
		"maintenanceEndTime" : "0" or "グリニッジ標準時のUNIX Time(秒)",
		"maintenanceRemainTime" : "メンテナンス開始までの時間（秒）" or "0", //メンテナンスがないときは0固定
	}
}
~~~
###Method : UpdateParticipantStatus

同一conference内にいるメンバー全員に参加者情報の変更通知を送信します。  
また、この参加者情報は上述した、Established メッセージで
クライアントに最新の情報として送信するため、サーバ側で最新状態を保持する必要があります。

#####※ UpdateParticipantStatus の メッセージ構造体（ Server > Client )
~~~
{
	“event” : UpdateParticipantStatus,
	“from” : SENDER_PARTICIPANT_ID,
	“conference” : "SENDER_PARTICIPANT_ID",
	“data” : {
		"update": {
			KEY_1: VALUE_1,
			KEY_2: VALUE_2,
			...
		}
	}
}
~~~
#####※ UpdateParticipantStatus の メッセージ構造体（ Client > Server )
~~~
{
	“event” : UpdateParticipantStatus,
	“from” : SENDER_PARTICIPANT_ID,
	“conference” : "SENDER_PARTICIPANT_ID",
	“data” : {
		"update": {
			KEY_1: VALUE_1,
			KEY_2: VALUE_2,
			...
		}
	}
}
~~~

---
###EVENT : SendMessage
同一conference内にいるメンバー全員に文字列を送信します。  
サーバは送られてきたメッセージに対して、GMT+0 のUNIXタイムスタンプ（秒）を付与し、  
クライアントに送信してください。  
また、このチャットデータは上述した、Established メッセージで  
クライアントに最新の情報として送信するため、サーバ側で履歴を保持する必要があります。  
デバイス操作に関するパーミッションのチャット機能にビットが立っている場合、通知を受けても無視する。

#####SendMessage の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “SendMessage”,
	“from” : SENDER_PARTICIPANT_ID
	“conference” : TARGET_CONFERENCE_ID,
	“data” : "MESSAGE"
}
~~~
#####SendMessage の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “SendMessage”,
	“from” : SENDER_PARTICIPANT_ID
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		“message“ : "MESSAGE",
		“displayname“ : "DISPLAY_NAME",
		“timestamp” : "グリニッジ標準時のUNIX Time(秒)"
	}
}
~~~
HISTORY\_OF\_CHAT\_DATA_OBJECT の構造は送られてきたCHATデータを時系列順に並べたもの（だたし、event名は不要）となります。  
このチャットデータは、同一conference内のParticipantが0人になったタイミングで破棄してしまって大丈夫です。

~~~
{
	length : COUNTS_OF_CHAT_MESSAGE,
	data : [
		{
			“from” : SENDER_PARTICIPANT_ID,
			“conference” : TARGET_CONFERENCE_ID,
			“data” : {
				“message“ : "MESSAGE",
				“displayname“ : "DISPLAY_NAME",
				“timestamp” : "グリニッジ標準時のUNIX Time(秒)"
			}
		},
		{
			“from” : SENDER_PARTICIPANT_ID,
			“conference” : TARGET_CONFERENCE_ID,
			“data” : {
				“message“ : "MESSAGE",
				“displayname“ : "DISPLAY_NAME",
				“timestamp” : "グリニッジ標準時のUNIX Time(秒)"
			}
		},...
	]
}
~~~
---
###EVENT : RejectParticipant
会議室内の指定ユーザを強制退出させます
<br>**このイベントはキック権限を持たないユーザは実行することができません**  

#####RejectParticipant の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RejectParticipant”,
	“from” : "PARTICIPANT_ID"
	“dest” : “PARTICIPANT_ID”,
	“conference” : TARGET_CONFERENCE_ID
}
~~~
#####RejectParticipant の メッセージ構造体（ Server > 切断先のクライアント )
~~~
{
	“event” : “RejectParticipant”,
	“from” : "PARTICIPANT_ID"
	“dest” : “PARTICIPANT_ID”,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"display_name" : "DISPLAY_NAME"
	}
}
~~~
カンファレンスサーバはメッセージを受け取とったあと、  
1 ) 切断先のクライアントに、RejectParticipantを送信  
2 ) 切断先のクライアントに、CloseFromServerを送信  
3 ) 切断対象のクライアント側とのWebSocketコネクションを切断し、TOKENを無効化  
4 ) Portalサーバに対しログアウト処理を通知します。

※data部に設定される DISPLAY_NAME はRejectParticipantを送信したユーザの
displayNameが設定される。

---

###EVENT : LockConference
カンファレンスに対して一時的に入室制限をかけます。<br>
Lock状態のカンファレンスに対する新規接続および、再接続は全てCloseFromServerで403を通知し入室を拒否してください
<br>**このイベントは会議ロック権限を持たないユーザは実行することができません**  

#####LockConference の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “LockConference”,
	“from” : PARTICIPANT_ID
	“dest” : “”,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : "true" or "false"
}
~~~
ロック状態、ロック状態の開放は、ConferenceStatusUpdate イベントを通じて参加者全員に通知されます。

---
###EVENT : UpdateConferenceName
現在参加中の会議名を更新します。
<br>**このイベントは会議名変更権限を持たないユーザは実行することができません**  
権限を持たないユーザからのリクエストに関してはMethodNotAllowedを通知するのと同時に、Portal側へ会議室名の変更を通知します（※後述

#####UpdateConferenceName の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “UpdateConferenceName”,
	“from” : PARTICIPANT_ID
	“dest” : “”,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : "NAME_OF_CONFERENCE"
}
~~~
変更の通知はConferenceStatusUpdateを通じて参加者全員へ通知をしてください。

---
###EVENT : MethodNotAllowed
利用制限がかけられたイベントが通知された場合に、サーバから利用制限がかかっている旨を通知します。
#####MethodNotAllowed の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “MethodNotAllowed”,
	“from” : "system"
	“dest” : PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : EVENT_NAME
}
~~~
---

###EVENT : RemoveFromConference
クライアント側から送られてくるメッセージで、  
会議室から抜ける旨の通知を受け取ります

#####RemoveFromConference の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RemoveFromConference”,
	“from” : PARTICIPANT_ID
	“dest” : “”,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : TOKEN
}
~~~
カンファレンスサーバはクライアント側とのWebSocketコネクションを切断し、  
TOKENを無効化、Portalサーバに対しログアウト処理を通知します。

---
###Method : CloseFromServer

サーバ側からWSのコネクションを切る前に  
クライアント側に切断理由を通知します。

~~~
{
	“event” : “CloseFromServer”,
	“from” : "system"
	“dest” : “”,
	“conference” : "",
	“data” : "REASON_OF_CLOSE_CONNECTION"
}
~~~
REASON\_OF\_CLOSE\_CONNECTION は  
文字列型で表現される値で、一覧は下記の通りです。

ステータス | code  | 説明
-------|------ | -------------
200 ~ 299 <br> 成功応答| 200  | 正常に切断。RemoveFromConferenceに対する正常な応答
300 ~ 399 <br> 転送応答| 302  | ( Reserved ) <br> カンファレンスサーバの移動が必要になった為切断
400 ~ 499 <br> リクエストエラー| 401  | connect / reconnect 接続に対しtokenが無効
　| 403  | 該当するカンファレンスが入室拒否状態
　| 404  | connect / reconnect 接続に対し該当するカンファレンスが存在しない
　| 408  | Connectionが切断されタイムアウトした<br>※ Portal側への通知にのみ利用
　| 410  | Pingに対するクライアントからの応答が一定回数確認出来なかった
　| 412  | PrepareParticipant後、接続が確立されなかった<br>※ Portal側への通知にのみ利用
　| 486  | 該当するカンファレンスが満員
500 ~ 599 <br> サーバエラー| 500  | サーバ内部で何らかのエラーが発生し会議の継続ができない
　| 501  | 会議入室時の接続バージョンが不足している
　| 504  | Portalのaction_NoticeJoinedConferenceがエラーコードを返答した
600 ~ 699 <br> グローバルクローズ| 600  | 予約会議時間を超えた為、会議が終了した<br>※ RemoveParticipantでは利用せず、ConferenceClosedでのみ利用
　| 603  | 強制的に会議全体が終了させられた<br>※ RemoveParticipantでは利用せず、ConferenceClosedでのみ利用
　| 604  | システム側から退出させられた
---

###EVENT : NotificationFailed
クライアント側から受信したイベントメッセージの対処に失敗した場合、サーバから失敗したイベント名を送信する。

#####NotificationFailed の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “NotificationFailed”,
	“from” : "system",
	“dest” : PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"event" : "TARGET_EVENT_NAME",
		"detail" : "ERROR_MESSAGE"
	}
}
~~~
detailには失敗理由を載せる。

---
###EVENT : RequestStartRecord
会議に対する録画開始の要求メッセージです
<br>**このイベントは録画権限を持たないユーザは実行することができません**
#####RequestStartRecord の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RequestStartRecord”,
	“from” : TARGET_PARTICIPANT_ID
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが生成する一意なシーケンス番号
	}
}
~~~
カンファレンスサーバはメッセージを受け取とった後、状態をPREPAREへ遷移させ、  
1. 現在録画の状態がIDLEであれば新しい録画の状態を示すユニークなID( recordID )を生成し、送信元に対してACKを返します  
2. 現在の録画状態がPREPAREであれば、現在のrecordIDを送信元にACKメッセージとして返します

#####RequestStartRecord に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "RequestStartRecord",
		"recordID": 現在最新の録画状態のID
	}
}
~~~

３．会議の録画が許可されていない場合、または現在の録画状態がRECの場合、またはログインユーザが２人未満の場合は無効な処理としてNACKを返します
#####RequestStartRecord に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "RequestStartRecord",
		"errorCode" : エラーコードを示す数字
	}
}
~~~

状態  | エラーコード |
------------- | ---------
ログインユーザが２人未満の場合 | 403
会議の録画が許可されていない場合 | 405
本人にパーミッションが付与されていない | 405
現在の録画状態がRECの場合 | 409

この後、Portalに対して録画開始要求のHTTPリクエストを送信します（後述）

---
###EVENT : StopRecord
会議中の録画の停止を行います
<br>**このイベントは録画権限を持たないユーザは実行することができません**
#####StopRecord の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “StopRecord”,
	“from” : TARGET_PARTICIPANT_ID
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号
	}
}
~~~

1. カンファレンスサーバは現在のステータスがRECの場合、ステータスをIDLEに変更し
ACKを返します。  
その後、Portalに録画停止要求を投げ（後述） ConferenceStatusのisRecordをfalseに変更し
参加者全員に録画状態の変更を通知します  
#####StopRecord に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "StopRecord"
	}
}
~~~

2. 会議の録画が許可されていない場合、または現在の状態がRECではない場合、NACKを返します
#####StopRecord に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "StopRecord",
		"errorCode" : エラーコードを示す数字
	}
}
~~~

状態|エラーコード
-------------|-----
現在の録画状態がREC中でない場合 | 409
会議の録画が許可されていない場合 | 405
強制録画会議の場合| 405
本人にパーミッションが付与されていない | 405

---
###EVENT : CancelRecord
会議中の録画要求の取り消しを行います
<br>**このイベントは録画権限を持たないユーザは実行することができません**
#####CancelRecord の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “CancelRecord”,
	“from” : "system"
	“dest” : "TARGET_PARTICIPANT_ID",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recordID": 現在最新の録画状態のID
	}
}
~~~
カンファレンスサーバはメッセージを受け取とった後、  

1.現在の recordID の値が正しく、PREPARE状態の場合、RequestStartRecord のACKを投げたParticipant全てに対してResultRecordStatus イベントを通知します  

#####CancelRecord に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "CancelRecord"
	}
}
~~~

2.会議の録画が許可されていない場合、またはrecordIDが不正、または現在の状態がPREPAREではない場合、NACKを返します

#####CancelRecord に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "CancelRecord",
		"errorCode" : エラーコードを示す数字
	}
}
~~~

状態  | エラーコード |
------------- | ---------
現在の録画状態が録画準備中でない場合 | 409
会議の録画が許可されていない場合 | 405
強制録画会議の場合| 405
本人にパーミッションが付与されていない | 405

---
###EVENT : ResultRecordStatus
録画開始要求のACKを受け取ったParticipantに対して、最終的な録画状況の結果を通知します
#####ResultRecordStatus の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “ResultRecordStatus”,
	“from” : "system"
	“dest” : "TARGET_PARTICIPANT_ID",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : { 
		"recordID": 現在最新の録画状態のID,
		"autoRecording": "true" or "false" // true: 自動録画, false: 通常録画 
		"result" : “OK" or "FAILD" or "CANCELED"
		"reason” : resultに対する理由コードです(Number型)
	}
}
~~~
理由コード一覧

Value of Reason  | 概要
------------- | -------------
200 | 録画成功 かならず result = OK と一緒に渡される
204 | 録画要求がキャンセルされた 必ず result = CANCELEDと一緒に渡される
408 | タイムアウト
500 | V5Liteサーバが処理を正常に処理できなかった
502 | Vidyoサーバが処理を正常に処理できなかった
503 | 録画サーバがBusy状態で録画を開始できなかった
504 | 自動録画に失敗した
507 | 容量不足により録画を開始することが出来なかった

---
###EVENT : StartDesktopSharing
あるパーティシパントがデスクトップ共有を開始した際、それをConferenceに通知します。
<br>**このイベントはシェアリング権限を持たないユーザは実行することができません**
#####StartDesktopSharing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “StartDesktopSharing”,
	“from” : SHAREE_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sharingParticipantID" : SHAREE_PARTICIPANT_ID,
		"sharingURI" : 共有画面のURI,
		"width" : Senderの画面解像度を示すInt型の値,
		"height" : Senderの画面解像度を示すInt型の値,
		"pointingFlg" : "true" or "false" // ポインティング許可フラグ(true:許可、false:不許可)
	}
}
~~~
#####StartDesktopSharing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “StartDesktopSharing”,
	“from” : "system",
	“dest” : SENDER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sharingParticipantID" : SHAREE_PARTICIPANT_ID,
		"sharingURI" : 共有画面のURI,
		"width" : Senderの画面解像度を示すInt型の値,
		"height" : Senderの画面解像度を示すInt型の値,
		"pointingFlg" : "true" or "false" // ポインティング許可フラグ(true:許可、false:不許可)
	}
}
~~~
カンファレンスサーバはメッセージを受け取とった後、リモートポインティングの送信可能な先として
リストに追加をします。

---
###EVENT : StopDesktopSharing
あるパーティシパントがデスクトップ共有を停止した際、それをConferenceに通知します。
<br>**このイベントはシェアリング権限を持たないユーザは実行することができません**
#####StopDesktopSharing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “StopDesktopSharing”,
	“from” : SHAREE_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sharingParticipantID" : SHAREE_PARTICIPANT_ID,
		"sharingURI" : 共有画面のURI
	}
}
~~~
#####StopDesktopSharing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “StopDesktopSharing”,
	“from” : "system"
	“dest” : "SENDER_PARTICIPANT_ID",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sharingParticipantID" : SHAREE_PARTICIPANT_ID,
		"sharingURI" : 共有画面のURI
	}
}
~~~
カンファレンスサーバはメッセージを受け取とった後、リモートポインティングの送信可能な先としてリストから排除します。  
サーバからクライアントへのイベントの通知は  
１．Sharerとバインディング状態のSenderがいた場合はそのSenderに対して送信  
２．SharerがStopDesktopSharingを呼ばずにConnectionLostもしくは退出した場合バインディング状態のSenderに送信のいずれかのタイミングで送信されます

---
###EVENT : RequestRemotePointing
SenderからSharerに対してリモートポインティングのバインディング要求を出します
<br>**このイベントはポインティング権限を持たないユーザは実行することができません**
#####RequestRemotePointing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RequestRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHAREE_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {　
		"sequence" : パーティシパントから送られる Int 型のシーケンス番号　
	}
}
~~~
サーバは SHAREE\_PARTICIPANT\_ID が Sharerとして有効な場合（StartDesktopSharingの開始を申告していた場合）はSenderに対してACKを送信し
#####RequestRemotePointing に対する ACK応答（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”,
	“dest” : SENDER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントから送られる Int 型のシーケンス番号,
		"recievedEvent" : "RequestRemotePointing"
	}
}
~~~

SHAREE\_PARTICIPANT\_ID が Sharerとして無効な場合はNACKを返します
#####RequestRemotePointing に対する NACK応答（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”,
	“dest” : SENDER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントから送られる Int 型のシーケンス番号,
		"recievedEvent" : "RequestRemotePointing"
	}
}
~~~

Senderの要求通りのバインディングが成立した場合、SharerにもRequestRemotePointingメッセージを送信します

#####RequestRemotePointing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “RequestRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHAREE_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {　
		"sequence" : パーティシパントから送られる Int 型のシーケンス番号　
	}
}
~~~

---
###EVENT : RemotePointing
SenderからSharerに対してリモートポインティングの座標情報を送ります
<br>**このイベントはポインティング権限を持たないユーザは実行することができません**
#####RemotePointing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RemotePointing”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : SHARER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {　
		"sequence" : パーティシパントから送られる Int 型のシーケンス番号,
		"width" : Senderの画面解像度を示すInt型の値,
		"height" : Senderの画面解像度を示すInt型の値,
		"x" : 座標X
		"y" : 座標Y 
	}
}
~~~
#####RemotePointing に対する ACK応答（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”,
	“dest” : SENDER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントから送られる Int 型のシーケンス番号,
		"recievedEvent" : "RemotePointing"
	}
}
~~~

サーバは SHARER\_PARTICIPANT\_ID が Senderのバインディング対象では無い場合、無効な要求とみなしNACK応答を返します
#####RemotePointing に対する NACK応答（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”
	“dest” : SENDER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントから送られる Int 型のシーケンス番号,
		"recievedEvent" : "RemotePointing"
	}
}
~~~

Senderに対する有効なバインド先であった場合、Sharerに対してRemotePointingメッセージを送信します

#####RemotePointing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “RemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHARER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {　
		"sequence" : パーティシパントから送られる Int 型のシーケンス番号,
		"width" : Senderの画面解像度を示すInt型の値,
		"height" : Senderの画面解像度を示すInt型の値,
		"x" : 座標X,
		"y" : 座標Y　
	}
}
~~~

---
###EVENT : ReleaseRemotePointing
Sharerに対してリモートポインティングの解除を申請します
<br>**このイベントはポインティング権限を持たないユーザは実行することができません**
#####ReleaseRemotePointing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “ReleaseRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHARER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {}
}
~~~

サーバは Senderに対する有効なバインド先であった場合のみSharerに対してReleaseRemotePointingメッセージを送り、Sharer Sender 間のバインドを解除します。

#####ReleaseRemotePointing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “ReleaseRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHARER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {}
}
~~~

このイベントでは無効なバインド対象へのReleaseRemotePointingが通知されてもNACKは特に返答をしません


###EVENT : RequestStartEnquete
クイックアンケート開始の要求メッセージです
#####RequestStartEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RequestStartEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが生成する一意なシーケンス番号
		"quickEnqueteID" : quickEnqueteID,
		"title" : アンケートタイトル,
		"quickEnqueteQuestion" : 質問内容,
		"quickEnqueteQuestionType" : 回答種別
		"quickEnqueteAttributeCount" : 選択肢数
		"quickEnqueteAttributeList" :[
			{
				"quickEnqueteAttributeID" : "Quick Enquete Attribute ID",
				"quickEnqueteAttributeLabel": "Label",
				"quickEnqueteAttributeSort": Sort Number,
			},
			{
				"quickEnqueteAttributeID" : "Quick Enquete Attribute ID",
				"quickEnqueteAttributeLabel": "Label",
				"quickEnqueteAttributeSort": Sort Number,
			},...
		]
	}
}
~~~
※ quickEnqueteQuestionType: 1:yes/no、2:番号選択（radio）、3:番号＋テキスト（radio）、4:番号選択（checkbox）、5:番号＋テキスト（checkbox）、6:テキスト（入力式）  
※重説では1〜3のみ   
※ quickEnqueteQuestionTypeが 1, 2の場合は、"quickEnqueteAttributeList"の設定値は[]とする。

カンファレンスサーバはメッセージを受け取とった後、 以下の処理を実施する。

1. 現在クイックアンケートの状態が**WAIT**であれば、**WATCH**に遷移させ、参加者全員にクイックアンケート質問内容（後述）を通知し、送信元に対してACKを返す。 

#####RequestStartEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "RequestStartEnquete"
	}
}
~~~

2. 現在のアンケート状態が**WATCH**、または**RESULT**であれば、送信元にNACKメッセージ返します

#####RequestStartEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "RequestStartEnquete"
	}
}
~~~

###EVENT : QuestionEnquete
クイックアンケート質問内容メッセージです
#####QuestionQuickEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “QuestionEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
		"title" : アンケートトタイトル,
		"quickEnqueteQuestion" : 質問内容,
		"quickEnqueteAttributeCount" : 選択肢数,
		"quickEnqueteQuestionType" : 回答種別,
		"quickEnqueteAttributeList" :[
			{
				"sortNumber" : Sort Number,
				"label": "Label-1",
			},
			{
				"sortNumber" : Sort Number,
				"label": "Label-2",
			},...
		],
		"isPauseAnswerEnquete" : "true" or "false" // true:中断中, false:"進行中"
	}
}
~~~
質問者を除く参加者全員に通知します。<br>
※ quickEnqueteQuestionTypeが 1, 2の場合は、"quickEnqueteAttributeList"の設定値は[]とする。

###EVENT : AnswerEnquete
クイックアンケート回答内容メッセージです
#####AnswerEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “AnswerEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID,
		"quickEnqueteAnswer" : "" or "回答番号",
		"input_data" : "" or "回答データ"
	}
}
~~~
QuestionEnqueteメッセージの回答種別によってquickEnqueteAttributeID、input_dataのどちらかに値を設定する。  
メッセージ受信に成功した場合、回答者にはACKを返却し、質問者にはAnswerEnqueteを通知する。  

#####AnswerEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "AnswerEnquete"
	}
}
~~~

#####AnswerEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “AnswerEnquete”,
	“from” : "system"
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
		"title" : アンケートタイトル,
		"quickEnqueteQuestion" : 質問内容,
		"quickEnqueteQuestionType" : 回答種別
		"quickEnqueteAttributeCount" : 選択肢数,
		"numAnswers" : 回答者母数,
		"aggregate" : [ 10,20,0,9,...... ] , // 0 ~ n番の解答結果集計情報
		"detail" : [
			{
				"participantID" : 回答者のPARTICIPANT_ID,
				"quickEnqueteAnswer" : "" or "回答番号",
				"input_data" : "" or "回答データ"
			},
			{
				"participantID" : 回答者のPARTICIPANT_ID,
				"quickEnqueteAnswer" : "" or "回答番号",
				"input_data" : "" or "回答データ"
			}, ...
		],
		"unansweredList" : [
			{
				"participant_id" : 未回答者のPARTICIPANT_ID,
				"display_name" : 未回答者のDISPLAY_NAME
			},
			{
				"participant_id" : 未回答者のPARTICIPANT_ID,
				"display_name" : 未回答者のDISPLAY_NAME
			}, ...
		]
	}
}
~~~
※ "aggregate"には回答結果集計数が設定される。  
※ 集計できないinput_dataの場合は空の配列を設定する。
※ quickEnqueteQuestionTypeが 1, 2の場合、"quickEnqueteAnswer"には以下の値を設定する  
  quickEnqueteQuestionType:1(yesno) yes:"1" no:"2"  
  quickEnqueteQuestionType:2(radio) 該当番号  

また、閲覧者に通知する場合、detail, unansweredListには[]（リスト型空配列）を設定する。
#####AnswerEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “AnswerEnquete”,
	“from” : "system"
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
		"quickEnqueteAttributeCount" : 選択肢数,
		"numAnswers" : 回答者母数,
		"aggregate" : [ 10,20,0,9,...... ] , // 0 ~ n番の解答結果集計情報
		"detail" : [], // リスト型空配列
		"unansweredList" : [] // リスト型空配列
	}
}
~~~

quickEnqueteIDの不一致、または回答種別に対する回答番号、または回答データが誤っていた場合、または質問者からアンケート終了通知受信後はNACKを返却する。

#####AnswerEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "AnswerEnquete"
	}
}
~~~

###EVENT : PauseEnquete
クイックアンケート回答受付停止の要求メッセージです
#####PauseEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “PauseEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、回答者にはStopEnqueteを通知する。  

#####PauseEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "PauseEnquete"
	}
}
~~~

#####PauseEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “PauseEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~
当メッセージを受信後は、AnswerEnqueteメッセージを受け付けない。  
クイックアンケートの状態を**RESULT**に遷移させる。

quickEnqueteIDの不一致だった場合、NACKを返却する。

#####PauseEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "PauseEnquete"
	}
}
~~~

###EVENT : ResumeEnquete
会議に対するクイックアンケート再開要求メッセージです
#####ResumeEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “ResumeEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、回答者にはRestartEnqueteを通知する。  

#####ResumeEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "ResumeEnquete"
	}
}
~~~

#####ResumeEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “ResumeEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~
当メッセージを受信後は、AnswerEnqueteメッセージを受け付ける。  
クイックアンケートの状態を**WATCH**に遷移させる。

quickEnqueteIDの不一致だった場合、NACKを返却する。

#####ResumeEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "ResumeEnquete"
	}
}
~~~

###EVENT : NoticeAnswerEnquete
クイックアンケートの集計結果を全員に通知するメッセージです
#####NoticeAnswerEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “NoticeAnswerEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID,
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、AnswerEnqueteを閲覧権限のある参加者に送信する。  

#####NoticeAnswerEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "NoticeAnswerEnquete"
	}
}
~~~
quickEnqueteIDの不一致だった場合、NACKを返却する。

#####NoticeAnswerEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "NoticeAnswerEnquete"
	}
}
~~~

###EVENT : NoticeAnswerClose
クイックアンケートの集計結果の全員通知取りやめを通知するメッセージです
#####NoticeAnswerClose の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “NoticeAnswerClose”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID,
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、NoticeAnswerCloseを閲覧権限のある参加者に送信する。  

#####NoticeAnswerClose に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "NoticeAnswerClose"
	}
}
~~~

#####NoticeAnswerClose の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “NoticeAnswerClose”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~

quickEnqueteIDの不一致だった場合、NACKを返却する。

#####NoticeAnswerClose に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "NoticeAnswerClose"
	}
}
~~~

###EVENT : FinishEnquete
会議に対するクイックアンケートの終了メッセージです
#####FinishEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “FinishEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
		"needSave" : "true" or "false"
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、FinishEnqueteを回答権限、または閲覧権限のある参加者に送信する。  

#####FinishEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "FinishEnquete"
	}
}
~~~

#####FinishEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “FinishEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
	}
}
~~~
現在のステータスを**WAIT**に遷移させ、prepareConferenceの"enqueteSaveMode"と"needSave"の組み合わせにより、Portalにクイックアンケート集計結果通知を投げるか判断する（後述）。  
また、アンケート質問者がアンケート終了前に退室してしまった場合、  
"enqueteSaveMode"が3(11b)以外はアンケートを保存しない。

#### アンケート保存パターン
enqueteSaveMode | needSave | 保存判定
---- | ---- | ----
0 (00b) | "true" | アンケートを保存する
　 | "false" | アンケートを保存しない
1 (01b) | "true" | アンケートを保存する
　 | "false" | アンケートを保存しない
2 (10b) | (無視) | アンケートを保存しない
3 (11b) | (無視) | アンケートを保存する

quickEnqueteIDの不一致だった場合、NACKを返却する。

#####FinishEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "FinishEnquete"
	}
}
~~~

###EVENT : Dialout
電話会議ユーザのコールアウトメッセージです
#####Dialout の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “Dialout”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"phoneNumber" : コールアウトするユーザの電話番号
	}
}
~~~
メッセージ受信後、PGiサーバへの通知に成功した場合はACKを返却し、失敗した場合はNACKを返却する。
電話会議ユーザが入室したら会議入室者にParticipantJoin(※前述)を通知する。  

#####Dialout に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "Dialout",
		"phoneNumber" : コールアウトするユーザの電話番号,
		"participant_id" : コールアウトする電話会議ユーザのParticipantID
	}
}
~~~

#####Dialout に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "Dialout"
	}
}
~~~

###EVENT : DialoutCancel
電話会議ユーザ呼び出しキャンセルのメッセージです
#####DialoutCancel の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “DialoutCancel”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"participant_Id" : ダイアルアウトをキャンセルする電話会議ユーザのParticipantID
	}
}
~~~
メッセージ受信後、PGiサーバへの通知に成功した場合はACKを返却し、失敗した場合はNACKを返却する。  
対象のParticipantIDを持つ電話会議ユーザが会議入室前であれば、DialoutFailed(※後述)をメッセージ通知者に通知する。  
通知タイミングによっては、すでに対象ユーザーが会議に入室済みとなる場合があるが、  
その場合はDialoutFailedではなく、会議入室者にParticipantLeave(※前述)を通知する。  

#####Hangup に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "DialoutCancel",
		"participant_id" : コールアウトした電話会議ユーザのParticipantID
	}
}
~~~

#####Hangup に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "DialoutCancel"
	}
}
~~~
###EVENT : DialoutFailed
電話会議ユーザのダイアルアウトに失敗、またはキャンセルした場合のメッセージです
#####DialoutFailed の メッセージ構造体（ Server > Client )
~~~
{
	“event” : "DialoutFailed",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"participant_id" : ハングアップした電話会議ユーザのParticipantID
	}
}
~~~
本メッセージの通知契機は、Dialoutメッセージ通知後にDialoutCancelメッセージを通知した場合、またはダDialoutメッセージ通知後に電話会議ユーザがダイアルアウトを拒否した場合に通知となる。


---
##3. 入退出の管理とPortalとの連携
Conferenceサーバは、パーティシパントの増減を参加者へ通知するのと同時に、ユーザアカウントを管理する Portal サーバと強調動作する必要がある為、幾つかのタイミングでPortalサーバ側に  HTTPのリクエストをPostする必要があります。

###3-1.接続時パラメータ
クライアントがサーバに接続するときのURLは下記の方式となります
####url :: wss://CONF\_SERVER\_FQDN:PORT/CONFERENCE_ID/

key名  | 概要
------------- | -------------
method  | 接続 or 再接続を表す文字列で connect or reconnectを渡します
pid  | クライアント固有の PIDで、Vidyoライブラリから取得した値です
token  | クライアント固有の tokenで V5 Lite Portalから取得した値です
displayName | 会議中に表示される文字列です

###3-2.退出通知のタイミング
Participantが下記の状態になった場合、必要に応じて通知を行います

状態  | 概要 | Listの更新 | 詳細 
------------- | -----|-----|---
add  | 接続 | ○ | 新規接続元にはEstablish通知。既存参加者にはParticipantJoinを通知
remove  | 会議からの退出が確定 |△| lostからremoveとなる場合は、すでにListが更新されてるので既存参加者への通知は発生しない
lost  | 会議の参加状態が不定 | ○ | lost状態の検知がされると、直ちにListを更新し既存参加者にはParticipantLeaveを通知
reconnect | lost状態から復帰した  | △ |  新規接続元にはEstablish通知。lostの検出が出来ずにreconnectしてきた場合は、Listの更新は行わない

#####上記いずれのケースもPotal側へHTTPによるList変更の通知を行います
####URL : https://SERVER_FQDN/api/v5lite/conference/?action\_UpdateParticipants
#####パラメータ、戻り値など詳細はWEBAPIを参照

----

###3-3. Master プロセスの起動時毎に 起動したことを通知する
####URL : https://SERVER_FQDN/api/v5lite/conference/?action\_Register
#####パラメータ、戻り値など詳細はWEBAPIを参照

---
###3-4. 会議中に行われた会議名の変更
####URL : https://SERVER_FQDN/api/v5lite/conference/?action\_ChangeConferenceName
#####パラメータ、戻り値など詳細はWEBAPIを参照

---
###3-5. 会議の開放を通知
Confereseサーバは該当の会議室の接続数が0になったタイミングで、チャットデータの履歴を破棄、会議の内部状態を破棄、会議IDを無効化した上で、会議が終了したことを通知します

####URL : https://SERVER_FQDN/api/v5lite/conference/?action\ _ConferenceClosed

#####パラメータ、戻り値など詳細はWEBAPIを参照

###3-6. Legacy端末の強制退室

####URL : https://SERVER_FQDN/api/v5lite/conference/?action\ _RemoveParticipant

#####パラメータ、戻り値など詳細はWEBAPIを参照

###3-7. 録画の開始
####URL : https://SERVER_FQDN/api/v5lite/conference/?action\_StartRecord
#####パラメータ、戻り値など詳細はWEBAPIを参照

###3-8. 録画の停止
####URL : https://SERVER_FQDN/api/v5lite/conference/?action\_StopRecord
#####パラメータ、戻り値など詳細はWEBAPIを参照

###3-9. 録画のキャンセル
####URL : https://SERVER_FQDN/api/v5lite/conference/?action\_CancelRecord
#####パラメータ、戻り値など詳細はWEBAPIを参照

###3-10. クイックアンケート集計結果通知
####URL : https://SERVER_FQDN/api/v5lite/conference/?action\_sendQuickEnqueteAnswer
#####パラメータ、戻り値など詳細はWEBAPIを参照
