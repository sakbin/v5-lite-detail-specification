#V5ホワイトボード データ構造

##1.データ形式 (.v5dフォーマット)
####ホワイトボードで利用される一つのドキュメント（.v5d）は、下記の形式のディレクトリ構成をzipアーカイブ化したもので定義されます
~~~
フォルダ構成
documentID
│
│  document.json
│  
│  PAGE_ID_1_bg.pdf
│  PAGE_ID_1.json
│
│  PAGE_ID_2_bg.pdf
│  PAGE_ID_2_bg.json

~~~
#####document.json で表現されるドキュメントオブジェクトは一つの資料は1つのタイトルと、一つのリビジョン、1つ以上のPageとデータ互換性のために定義される protocol プロパティを持ちます
~~~
Class Document {
	String title
	String revision
	Number protocol
	Array<Page> pages
}

document.json {
 
  title:"TITLE_OF_DOCUMENT",
  numPages:2,
  protocol:0.5
  pageIndex:[ 
  				"PAGE_ID_1",
  				"PAGE_ID_2"
  			   ]
}
~~~
#####page.jsonで表現される一つのページは1つのメモ、背景と複数のiAnnotationから構成されます
~~~
Class Page {
	String id
	String background
	String memo
	Array<iAnnotation> anotations
}

page.json
{
	id:"PAGE_ID" 
	background:"NAME_OF_BACKGROUND",
	memo:"STRING_OF_MEMO",
	annotations:[  ]
}
~~~

#####書き込み情報を表す iAnnotation は、一つのIDと、一つのタイプ、位置、サイズの情報、書き込み情報を表現するdataプロパティから構成されます
~~~
Class iAnotation {
	 AnotationType type
	 String id
	 Point position
	 Size size
	 Int color
	 Int alpha
	 Int weight
	 Int zOrder
	 Object data
}
~~~
##### ライブ中データのやり取りは所有権を示す iSyncAnnotation として通信が行われます
Class iSyncAnnotation : iAnotation {
	String ownner
}
#####書き込み情報のタイプは下記の通りです
~~~
Enum AnnotationType {
	Draw = 0
	Shape = 1
	Text = 2
}
~~~
#####一本の手書きの線情報は、一つの色情報、透明度、線の太さの情報、一つ以上の座標情報から構成されます
~~~
Class DrawData: iAnnotation {
	Array<Point> points
}

page.json 内部での表現
{
	type:TYPE_OF_ANNOTATION,
	data:{ 
		points:[
			{x:X1,y:Y1},
			{x:X2,y:Y2},...
			{x:Xn,y:Yn}
		]
	}
}
~~~
#####一つのシェイプデータの情報は、一つの種類情報、座標、大きさ、色、透明度の情報を持ちます
~~~
Class ShapeData: iAnnotation {
	ShapeType type
}

page.json 内部での表現
{
	type:TYPE_OF_ANNOTATION,
	data:{ 
		type: TYPE_OF_SHAPE
	}
}
~~~
#####シェイプの種類については下記のとおりです
~~~
Enum ShapeType {
	Line
	Rect
	Triangle
	Circle
	Arrow
}
~~~
#####一つのテキストデータの情報は、一つのテキスト情報、座標、テキストエリアの大きさ、テキストの色情報、テキストの大きさの情報を含みます
~~~
Class TextData: iAnnotation {
	String text
}

page.json 内部での表現
{
	type:TYPE_OF_ANNOTATION,
	data:{ 
		text: TEXT_OF_ANOTATION
	}
}
~~~
<br>

##2.更新情報
#####ホワイトボードに対する更新情報を示すメタデータフォーマットです。ある段階の資料のバージョンから最新の状態にするための最新の情報を含んだデータです
~~~
delta { 
  documentID:ID_OF_DOCUMENT,
  fromDelta:HEAD_REVISION,
  removed:[
  	REMOVED_PAGE_ID_1,
  	REMOVED_PAGE_ID_2
  ]
  added:[
  	ADDED_PAGE_ID_1,
  	ADDED_PAGE_ID_2
  ]
  pageIndex:[ 
  				HASH_OF_PAGE_1,
  				HASH_OF_PAGE_2
  ]
  deltas:{
  	HASH_OF_PAGE_1:DATA_OF_PAGE_1,
  	HASH_OF_PAGE_2:DATA_OF_PAGE_2
  }
}
~~~
<br>

##3.操作情報
####ホワイトボードに対する更新情報を示すメタデータフォーマットを説明します。操作情報には大きく２種類のタイプがあり、ページ情報に関する操作と 書き込みに関する操作があります。このうち前半で説明するページに関する操作については永続化されません


###3-1.ページに類する操作一覧

--
####3-1-1 : ページの切り替え
概要：現在閲覧中のページを指定されたIDのページに切り替えます

プロパティ名|型|概要
---|---|---
type|文字列|FORCUS を指定する
documentID|文字列|対象となるドキュメントのID  
pageID|文字列| 対象となるページのID  
data|Object|予約  
~~~
pageOperation {
	documentID:ID_OF_DOCUMENT,
	pageID:ID_OF_PAGE
	type:"FORCUS",
	data:{}
}
~~~
--
####3-1-2 :ページ拡大/縮小/移動
概要：現在閲覧中のページを指定された大さ、位置に変形させます  

プロパティ名|型|概要
---|---|---
type|文字列|TRANSFORM を指定する
documentID|文字列|対象となるドキュメントのID 
pageID|文字列| 対象となるページのID   
data|Object| 位置、大きさを示すオブジェクト  
~~~
pageOperation {
	documentID:ID_OF_DOCUMENT,
	pageID:ID_OF_PAGE
	type:"TRANSFORM",
	data:{ size:{ witdh:00,height:00 },
			positon:{ x:00,y:00 } }
}
~~~

<br>
###3-2.書き込み操作
--
#### 3-2-0a. アノテーションの所有
概要：現在閲覧中のページに存在する、アノテーションの所有権をリクエストします。一度のメッセージで
一つのアノテーションのリクエストに対応します。

プロパティ名|型|概要
---|---|---
type|文字列| OWN を指定する
documentID|文字列|対象となるドキュメントのID  
pageID |文字列|対象となるページのID  
data|String | 所有権を保持するiAnnotation オブジェクトのID

~~~
anotarionOperation {
	documentID:"ID_OF_DOCUMENT",
	pageID:"ID_OF_PAGE",
	type:"OWN",
	data:"ID_OF_ANNOTARION"
~~~

#### 3-2-0b. アノテーション所有権の放棄
概要：現在閲覧中のページに存在する、アノテーションの所有権をリクエストします。一度のメッセージで
複数のアノテーション所有権の放棄リクエストに対応します。

プロパティ名|型|概要
---|---|---
type|文字列| RELEASE を指定する
documentID|文字列|対象となるドキュメントのID  
pageID |文字列|対象となるページのID  
data| Array<String> | 所有権を保持するiAnnotation オブジェクトのID


--
#### 3-2-1. アノテーション追加 
概要：現在閲覧中のページに、アノテーションを追加します。一度のメッセージで
複数アノテーションの追加を指示できます

プロパティ名|型|概要
---|---|---
type|文字列| ADD を指定する
documentID|文字列|対象となるドキュメントのID  
pageID |文字列|対象となるページのID  
data|Array\< iAnnotation > | ページに追加される iAnnotation オブジェクトの配列  
~~~
anotarionOperation {
	documentID:ID_OF_DOCUMENT,
	pageID:ID_OF_PAGE,
	type:"ADD",
	data:[ DATA_OF_ANOTAION_01,
	DATA_OF_ANOTAION_02 ]
}
~~~
--
#### 3-2-2. アノテーション 拡大/縮小/移動 
概要：対象アノテーションの位置、大さを変化させます。複数のアノテーションに対して処理をすることが可能ですが、所有権を持っていないアノテーションに対する操作は無視してください

プロパティ名|型|概要
---|---|---
type|文字列|TRANSFORM を指定する
documentID|文字列|対象となるドキュメントのID  
pageID |文字列|対象となるページのID  
data|Array\<Object>| 位置、大きさを示すオブジェクト の配列
~~~
anotarionOperation {
	documentID:ID_OF_DOCUMENT,
	pageID:ID_OF_PAGE,
	type:"TRANSFORM",
	data:[ 
			{ id:ID_OF_ANOTATION,
				size:{ witdh:00,height:00 },
				positon:{ x:00,y:00 } 
			},
			{ id:ID_OF_ANOTATION,
				size:{ witdh:00,height:00 },
				positon:{ x:00,y:00 } 
			}
	]
}
~~~
--
#### 3-2-3. アノテーション 削除
概要：対象アノテーションを削除します。複数のアノテーションに対して処理をすることが可能ですが、所有権を持っていないアノテーションに対する操作は無視してください
 
プロパティ名|型|概要
---|---|---
type|文字列| REMOVE を指定する
documentID|文字列|対象となるドキュメントのID  
pageID |文字列|対象となるページのID  
data|Array<文字列>| 対象となるアノテーションのID  
~~~
anotarionOperation {
	documentID:ID_OF_DOCUMENT,
	pageID:ID_OF_PAGE,
	type:"REMOVE",
	data:ID_OF_ANOTATION
}
~~~
--
#### 3-2-3. アノテーション深度変更
概要：対象アノテーションを最前面に移動します。所有権を持っていないアノテーションに対する操作は無視してください。

プロパティ名|型|概要
---|---|---
type|文字列| FORCUS を指定する
documentID|文字列|対象となるドキュメントのID  
pageID |文字列|対象となるページのID  
data|文字列| 対象となるアノテーションのID  
~~~
anotarionOperation {
	documentID:ID_OF_DOCUMENT,
	pageID:ID_OF_PAGE,
	type:"FORCUS",
	data:ID_OF_ANOTATION
}
~~~

--
#### 3-2-3. アノテーション編集
#### 2016年4月の段階で下記の実装は未来に予定される機能となります
概要：対象アノテーションの情報を更新します。複数のアノテーションに対して処理をすることが可能ですが、所有権を持っていないアノテーションに対する操作は無視してください

プロパティ名|型|概要
---|---|---
type|文字列| EDIT を指定する
documentID|文字列|対象となるドキュメントのID  
pageID |文字列|対象となるページのID  
data|Object| 更新情報を表すオブジェクト  
~~~
anotarionOperation {
	documentID:ID_OF_DOCUMENT,
	pageID:ID_OF_PAGE,
	type:"EDIT",
	data:[ { 
				annoationID:ID_OF_ANOTATION,
				property:NAME_OF_PROPERTY,
				value:VALUE_OF_PROPERTY
		   },
		   { 
				annoationID:ID_OF_ANOTATION,
				property:NAME_OF_PROPERTY,
				value:VALUE_OF_PROPERTY
		   }
		  ]
}
~~~



####ライブラリで必要な機能（パブリックなもの）
~~~
データのローカルへの保存 ( UI )
　セーブが必要なタイミングでライブラリが呼び出すのでUIはクロージャを設定する
　引数として、case-1 生Json文字列とファイルパス
　			  case-2 画像のバイトアレイとファイルパス

データの展開 ( zip展開 )
　ダウンロードが終わったら、保存先を指定するようにする

データの編集
　メモリ上のJsonを編集 

各種PortalAPIへの対応
　1. ログイン
　2. ログイン延長
　3. 資料の編集権限の確認
　4. 資料のダウンロード
　5. 資料の保存
　6. 資料の上書き保存
　
ポータルで実装される強制上書きAPI の時のみ必要
  
資料を公開してる人の処理 ( Conference / Lib )  
現在公開されている資料の状態の共有 （資料名やページなど）( Conference / Lib )  
~~~





