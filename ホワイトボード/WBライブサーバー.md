#ホワイトボード ライブサーバー
##1. WebSocket API
###WebSocketAPIの構造体

WebSocket APIは基本的に下記の構造体のJSONオブジェクトを  
LiveServerに送ることで実現します。

client > server

```
{
	“event” : "id_of_event",
	"sequence" : NUM_OF_SEQUENCE,
	“from” : "id_of_sender"
	“dest” : "id_of_target",
	“conference” : "id_of_conferene",
	“data” : {  }
}
```

server > client

```
{
	“event” : "id_of_event",
	“from” : "id_of_sender",
	“conference” : "id_of_conferene",
	“data” : {  }
}
```

####data 構造体

key名  | 型 | null値 | 概要
-------|------ | ------|-------
event  | string | 必須 | イベントのタイプを表す文字列 
sequence | numeric | 必須 | メッセージの送信番号<br>このフィールドは client から server の通信のみ含まれます
from  | 文字列 | 必須 | メッセージの送信元。サーバの判断による送信の場合、SERVERという文字列が渡されます。
dest  | 文字列 | オプション | メッセージの宛先。メッセージを特定の参加者にのみ送る時に利用します<br>このフィールドは client から server の通信のみ含まれます
conference | 文字列 | 必須 | 会議のID
data | Object | オプション | 各種メッセージに対応した構造体

###RESPONSEメッセージ
サーバに対して操作要求が送られてきた際に、Confrenceサーバはその操作要求が正しく処理できるかどうかを ACK ( ACKnowledgement : 確認応答 ) もしくは NACK ( Negative ACKnowledgement：否定応答 ) の形で返答します。  
##### “ACK” or "NACK" の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “RESPONSE"
	"sequence" : NUM_OF_SEQUENCE,
	"from" : "SERVER",
	“conference” : "id_of_conferene",
	"code" : NUM_OF_STATUS_CODE
	"discription" : "message_of_error"
	“data” : null 
}
~~~

key名  | 型 | null値 | 概要
-------|------ | ------|-------
event  | string | 必須 | “RESPONSE"
sequence | numeric | 必須 | クライアントが送ってきたメッセージの送信番号
from  | string列 | 必須 | "SERVER" 固定
conference | string | 必須 | 会議のID
code | numeric | 必須 | 処理コード 200 は成功 で それ以外はエラーとなる。エラーについては各種メッセージに記載
discription | string | オプション | ステータスの詳細メッセージ
data | Object | オプション | 拡張用のフィールド


##2.接続処理
###2-1.接続時パラメータ
クライアントがサーバに接続するときのURLは下記の方式となります
####url :: wss://CONF\_SERVER\_FQDN:PORT/CONFERENCE_ID/

key名  | 概要
------------- | -------------
mode  | 予約語 WB を指定します
token  | クライアント固有の tokenで V5 Lite Portalから取得した値です

CONFERENCE_ID に関係した資料情報がサーバ上にない場合、  
1。ポータルからこの会議で利用することが決まってる資料の情報を取得した上で  
2。サーバ上で情報を展開します

##3.同期処理
####EVENT Init
####この機能は仕様変更により無くなりました


##4.管理権の変更
####EVENT changeOwner
####この項目は仕様変更により無くなりました


##5.資料の公開 / 非公開 / 同期処理
###5-1.資料の公開処理
####EVENT publishDocument
あるユーザから資料の公開の希望があった時に通知されるメッセージです。  
1.資料を公開したユーザは publisherとなります  
2.すでに他のユーザがpublisherの場合はまず全員にunpublishDocument (後述)を  
　送信してください。  
既に公開済みの資料に対するメッセージに対しては失敗のレスポンスを返します
#####client > server

```
{
	“event” : "publishDocument",
	"sequence" : num_of_squence,
	“from” : "id_of_participant",
	"dest" : null
	“conference” : "id_of_conference",
	“data” : 
	{ id:"id_of_document",
	  revision:revision_of_document	
	}
}
```
RESPONSE Code

値  | 概要
-------|------ | 
200  | 成功
403 | 実行権限がない
409 | 古いリビジョンの資料を公開状態にしようとした 
412 | 既に資料は公開済み

サーバはポータルに対して  
1.この資料が公開状態になったこと通知します  
2.リビジョンが異なる場合、  
　古いリビジョンであればエラーを  
　新しいリビジョンであればポータルから最新の資料情報を受け取った後に  
  参加者に資料のIDを通知します。

##### server > client

```
{
	“event” : "publishDocument",
	“from” : "id_of_participant",
	“conference” : "id_of_conference",
	“data” : "id_of_document"
}
```

###5-2.最新状態の取得
####EVENT getDelta
あるユーザから、会議中の資料の最新の状態を取得するAPIに対して、  
サーバは、そのユーザに対して、資料の最新情報を送ります
#####client > server

```
{
	“event” : "getDelta",
	"sequence" : num_of_squence,
	“from” : "id_of_participant",
	"dest" : null
	“conference” : "id_of_conference",
	“data” : "id_of_document"
}
```

RESPONSE Code

値  | 概要
-------|------ | 
200  | 成功
404 | 該当する資料が存在しない


##### server > client

```
{
	“event” : "noticeDelta",
	“from” : "id_of_participant",
	“conference” : "id_of_conference",
	“data” : DELTA_OF_DOCUMENT
}
```



<br><br>

###5-3.資料の非公開処理
####EVENT unpublishDocument
現在のpubliserから資料の公開取りやめの希望があった時に通知されるメッセージです。  
公開状態の資料でなかったり、publiserでないユーザからの
メッセージに対しては失敗のレスポンスを返します。  
またpublisherとのコネクションが
切断された場合も、残り参加者全員にunpublishDocumentを送信してください

#####client > server

```
{
	“event” : "unpublishDocument",
	"sequence" : num_of_squence,
	“from” : "id_of_participant",
	"dest" : null
	“conference” : "id_of_conference",
	“data” : "id_of_document"
}
```

RESPONSE Code

値  | 概要
-------|------ | 
200  | 成功
403 | 実行権限がない
404 | 公開状態の資料が存在しない

サーバはポータルに対してこの資料が公開状態になったこと通知し  
その後参加者に資料のIDを通知します。

##### server > client

```
{
	“event” : "unpublishDocument",
	“from” : "id_of_participant",
	“conference” : "id_of_conference",
	“data” : "id_of_document"
}
```



##6.資料の切り替え
####EVENT changeDocument
####仕様変更により無くなりました。資料切り替えは　publish / unpublishDocument で実行します。

##7.資料の追加
準備中
##8.ページの追加
準備中
##9.ページ操作
####EVENT pageOperation
あるオーナーから表示中の資料に対する操作があった際に通知されるメッセージです。
#####client > server

```
{
	“event” : "pageOperation",
	"sequence" : num_of_squence,
	“from” : "id_of_participant",
	"dest" : null
	“conference” : "id_of_conference",
	“data” : pageOperationのデータ 別紙参照
}
```
RESPONSE Code

値  | 概要
-------|------ | 
200  | 成功
400 | 必要なパラーメータがない
403 | 実行権限がない
404 | 該当の資料は公開されていない または 該当のページは見つかない


サーバは参加者に操作情報を通知します。

##### server > client

```
{
	“event” : "changePage",
	“from” : "id_of_participant",
	“conference” : "id_of_conference",
	“data” : pageOperationのデータ 別紙参照
}
```

##10.アノテーション操作
####EVENT editOperation
あるユーザから表示中の資料に対する操作があった際に通知されるメッセージです。
#####client > server

```
{
	“event” : "editOperation",
	"sequence" : num_of_squence,
	“from” : "id_of_participant",
	"dest" : null
	“conference” : "id_of_conference",
	“data” : editOperationのデータ 別紙参照
}
```
RESPONSE Code

値  | 概要
-------|------ | 
200  | 成功
400 | 必要なパラーメータがない
403 | 実行権限がない
404 | 該当の資料は公開されていない または 該当のページは見つかない


サーバは参加者に資料のIDを通知します。

##### server > client

```
{
	“event” : "editOperation",
	“from” : "id_of_participant",
	“conference” : "id_of_conference",
	“data” : editOperationのデータ 別紙参照
}
```
