#V5 Conference環境構築手順書
本資料は、V5Lite版V-CUBEミーティングのカンファレンスサーバを構築する為の手順を示した資料です。

## 目次

+ [V-CUBEミーティングを構成するサーバについて](#sec001)
+ [インストールに関する前提条件](#sec002)
+ [サーバ構成図](#sec003)
+ [アプリケーションインストール手順](#sec100)
	+ [node.js インストール手順](#sec101)
	+ [Redisサーバ インストール手順](#sec102)
	+ [npm インストール手順](#sec103)
	+ [pm2 インストール手順](#sec104)
	+ [Conferenceサーバ インストール手順](#sec105)
+ [サーバ環境設定手順](#sec200)
	+ [Redisサーバ コンフィグ設定手順](#sec201)
	+ [Conferenceサーバ コンフィグ設定手順](#sec202)
+ [サーバ起動手順](#sec300)
	+ [Redisサーバ起動手順](#sec301)
	+ [Conferenceサーバ起動手順](#sec302)
+ [サーバ停止手順](#sec400)
	+ [Redisサーバ停止手順](#sec401)
	+ [Conferenceサーバ停止手順](#sec402)
+ [付録](#sec900)
	+ [redisのデーモン化](#sec901)
	+ [Conferenceサーバのデーモン化](#sec902)
	+ [conferenceサーバのテスト用config説明](#sec903)

<a id="sec001"></a>
## V-CUBEミーティングを構成するサーバについて
###サーバの種類と役割
+ Conferenceサーバ
	+ ParticipantList管理
	+ 会議情報管理
	+ チャット機能
	+ チャットログ管理
+ Redisサーバ
	+ DB機能

<a id="sec002"></a>
## インストールに関する前提条件
本資料では、以下の条件にてインストールを行うものとする。

+ サーバ台数	1台~n台
+ サーバOS		ubuntu 14.04.01
+ ミドルウェア
	+ apt-get 1.0.1
	+ wget 1.15
+ アカウント
	+ root (OSのビルドアカウント)
	+ node-user (conferenceサーバ起動アカウント)
+ 証明書のアクセス権限の変更
	+ ConferenceサーバのアクセスにはSSL認証が必要となる。その為、node-user(conferenceサーバ起動アカウント)が秘密鍵 (Private Key)へアクセスできるよう権限を設定する必要がある。
	+ 本資料で、下記ディレクトリにそれぞれ証明書が格納されているものとする。

###
	"秘密鍵"  : "/etc/ssl/private/star.vcube.com_2015.key",
	"証明局"  : "/etc/ssl/certs/ca-certificates.crt",
	"証明書"  : "/etc/ssl/certs/star.vcube.com_2015.pem"


	
	
<a id="sec003"></a>
## サーバ構成図
![サーバ構成図](./image.pdf)
---
<a id="sec100"></a>
# アプリケーションインストール手順
※ 以下の作業はrootユーザにて作業を行う

<a id="sec101"></a>
## node.js インストール手順
node.js インストール手順について、以下に記す。なお、バージョンについては、任意である。その時点での安定最新版を推奨する。
###1. /usr/local/bin配下でnode.jsをダウンロード
※ダウンロードするバージョンについては、任意。

	# cd /usr/local/bin
	# wget http://nodejs.org/dist/v0.12.0/node-v0.12.0.tar.gz
###2. tar.gz ファイルを解凍
	# tar -zxvf node-v0.12.0.tar.gz
###3. make を実行し、インストールする
	# cd node-v0.12.0
	# ./configure
	# make
	# make install
※ライブラリ不足でコンパイルが通らない場合は、適宜インストールすること。

### 4. ケーパビリティ設定
Conferenceサーバポートが443を使用しているので、1024番未満のポートを使用できるように設定する。

~~~
# sudo setcap cap_net_bind_service=+ep /usr/local/bin/node

[例] sudo setcap cap_net_bind_service=+ep /usr/local/bin/node
~~~

#### ケーパビリティ確認
設定した情報が反映されているか確認する。

~~~
# sudo getcap /usr/local/bin/node

[例] sudo getcap /usr/local/bin/node
> /usr/local/bin/node = cap_net_bind_service+ep
~~~


<a id="sec102"></a>
## Redisサーバ インストール手順
Redisサーバのインストール手順について、以下に記す。なお、バージョンについては、任意である。その時点での安定最新版を推奨する。
###1. redisをダウンロード
	# apt-get install redis-server

<a id="sec103"></a>
## npm インストール手順
npmのインストール手順について、以下に記す。
###1. npmをダウンロード
	# apt-get install npm
	
<a id="sec104"></a>
## pm2 インストール手順
pm2のインストールについて、以下に記す。
###1. npmからpm2をグローバルインストール
	# npm install pm2 -g
	
<a id="sec105"></a>
## Conferenceサーバ インストール手順
Conferenceサーバのインストール手順について、以下に記す
###1. conferenceServerをSourceTreeからチェックアウトする
###2. conferenceServerの所有者をnode-userに変更
	# cd "conferenceサーバ格納先"
	# chown -R node-user:node-user "conferenceサーバディレクトリ"

---
<a id="sec200"></a>
# サーバ環境設定手順
※ 以下の作業はrootユーザにて作業を行う。

<a id="sec201"></a>
## Redisサーバ コンフィグ設定手順
RedisサーバはMasterのみの単独構成、またはMaster/Slaveによる複数構成で立ち上げる事が可能となる。
以下に単独構成、複数構成のコンフィグ設定について記す。

###1. Redisサーバ単独構成
###1-1. ログ格納先ディレクトリを作成
	# mkdir -p /var/log/redis
###1-2. Master用config編集
以下の値を設定

	# cd /etc/redis/
	# vi redis.conf	
####1-2-1. "daemonize"の値を設定
	# daemonize no
	daemonize yes
####1-2-2. "bind"の値を設定
	# bind 127.0.0.1
	bind XXX.XXX.XXX.XXX
	(XXX -> redis Masterサーバーアドレス)
	
	[例] bind 10.132.139.117
####1-2-3. "dumpファイル名"を変更
	# dbfilename dump.rdb
	dbfilename conferenceRedis.rdb
####1-2-4. "logfile"を設定
	# logfile ""
	logfile /var/log/redis/redis-server.log

###2. Redisサーバ 複数構成
Master/Slave構成で立ち上げる場合は、Master:1に対してSlave:nの構成とする。
また、各Redisサーバはそれぞれ別のサーバで起動する。
###2-1. ログ格納先ディレクトリを作成
	# mkdir -p /var/log/redis
###2-2. Master用config編集
以下の値を設定

	# cd /etc/redis/
	# vi redis.conf	
####2-2-1. "daemonize"の値を設定
	# daemonize no
	daemonize yes
####2-2-2. "bind"の値を設定
	# bind 127.0.0.1
	bind XXX.XXX.XXX.XXX
	(XXX -> redis Masterサーバーアドレス)
	
	[例] bind 10.132.139.117
####2-2-3. "dumpファイル名"を変更
	# dbfilename dump.rdb
	dbfilename conferenceRedis.rdb
####2-2-4. "logfile"を設定
	# logfile ""
	logfile /var/log/redis/redis-server.log
####2-2-5. "slave-read-only"を設定
	# slave-read-only yes
	slave-read-only no
	
###2-3. Slave用config編集
以下の値を設定

	# cd /etc/redis/
	# vi redis.conf	
####2-3-1. "daemonize"の値を設定
	# daemonize no
	daemonize yes
####2-3-2. "bind"の値を設定
	# bind 127.0.0.1
	bind XXX.XXX.XXX.XXX
	(XXX -> redis Slaveサーバーアドレス)
	
	[例] bind 10.132.139.121
####2-3-3. "dumpファイル名"を変更
	# dbfilename dump.rdb
	dbfilename conferenceRedisS.rdb
####2-3-4. "logfile"を設定
	# logfile ""
	logfile /var/log/redis/redis-serverS.log
####2-3-5. "slaveof"を設定
	# slaveof <masterip> <masterport>
	slaveof XXX.XXX.XXX.XXX YYYY
	(XXX -> redis Masterサーバーアドレス
	 YYY -> redis Masterサーバーポート)
	 
	[例] slaveof 10.132.139.121 6379
####2-3-6. "slave-read-only"を設定
	# slave-read-only yes
	slave-read-only no
	
###2-4. sentinel用config編集
以下の値を設定

	# cd /etc/redis/
	# vi sentinel.conf	
####2-4-1. "daemonize"の値を設定
	daemonize yes
####2-4-2. "sentinel monitor"の値を設定
	# sentinel monitor mymaster 127.0.0.1 6379 2
	sentinel monitor mymaster XXX YYY Z
	(XXX -> redis Masterサーバーアドレス
	 YYY -> redis Masterサーバーポート
	 Z   -> redis Sentinel起動数)
	 
	[例] sentinel monitor mymaster 10.132.139.117 6379 2
####2-4-3. "sentinel down-after-milliseconds"の値を設定
	# sentinel down-after-milliseconds mymaster 30000
	sentinel down-after-milliseconds mymaster 5000
####2-4-4. "sentinel client-reconfig-script"の値を設定
	# sentinel client-reconfig-script mymaster /var/redis/reconfig.sh
	sentinel client-reconfig-script mymaster "Conferenceサーバ格納先"/sh/reconfig.sh
	
	[例] sentinel client-reconfig-script mymaster /home/project/v5LiteConference/sh/reconfig.sh

	
<a id="sec202"></a>
## Conferenceサーバ コンフィグ設定手順
conferenceサーバのコンフィグ設定について、以下に記す。

###1. Conferenceサーバ格納先に移動
	# cd "conferencサーバ格納先"
###2. conferenceサーバ config編集
以下の値を設定
####2-1. conferenceサーバー アドレス,ポートを設定
	"conference": {
		"port": XXXX,
		"addr": "YYYY"
	}
	(XXXX -> conferenceServer ポート番号
	 YYYY -> conferenceServer アドレス)
	 
	[例]
	"conference":{
		"port": 443,
		"addr": "mtg5l-cf-01.vcube.com"
	}
####2-2. SSL証明書設定
	"secret" : {
		"key"  : "XXX.key",
		"ca"   : "YYY.crt",
		"cert" : "ZZZ.pem"
	}
	(XXX -> 秘密鍵 (Private Key)
	 YYY -> CA用証明書(CRT)
	 ZZZ -> サーバ証明書
	 ※各設定はフルパスで指定する
	 
	[例]
	"secret" : {
		"key"  : "/etc/ssl/private/star.vcube.com_2015.key",
		"ca"   : "/etc/ssl/certs/ca-certificates.crt",
		"cert" : "/etc/ssl/certs/star.vcube.com_2015.pem"
	}
####2-3. portalサーバ アドレスを設定
	"portal": { 
		"addr": "XXXX" 
	}
	(XXXX -> portal サーバアドレス)
	
	[例]
	"portal": {
		"addr": "mtg5l.vcube.com"
	}
####2-3. Redisサーバ アドレス、ポート設定
	"redis": {
		"master": {
			"port": XXXX,
			"addr": "YYYY"
		},
		"slave": {
			"port": VVVV,
			"addr": "ZZZZ”
		}
	}
	(XXXX -> redis Master ポート番号
	 YYYY -> redis Master アドレス番号
	 VVVV -> redis Slave ポート番号
	 ZZZZ -> redis Slave アドレス番号)
	 ※ Redis単独で立ちあげる場合は、slaveにredis Masterのアドレス,ポートを設定する。

---
<a id="sec300"></a>
# サーバ起動手順

<a id="sec301"></a>
## Redisサーバ起動手順
※ 以下の作業はrootユーザにて作業を行う。

###1. Redisサーバ単独構成の場合
以下のコマンドを実行する。
####1-1. Redisサーバが起動していない事を確認
	# ps -ef | grep redis-server | grep -v grep 	# 
+ 起動してた場合はサーバ停止手順を参考にRedisサーバを停止する。

####1-2. Redisサーバ起動
	# redis-server /etc/redis/redis.conf
またはserviveコマンドでredis-serverを起動する

	# service redis-server start
	> redis-server を起動中: [  OK  ]

####1-3. Redisサーバが起動したことを確認
	# ps -ef | grep redis-server | grep -v grep
	> root 444 1 0 12:36 ? 00:00:00 redis-server 10.132.65.161:6379
	
###2. Redisサーバ複数構成の場合
以下のコマンドを実行する。
####2-1. Redisサーバが起動していない事を確認
	# ps -ef | grep redis-server | grep -v grep
	#
+ 起動してた場合はサーバ停止手順を参考にRedisサーバを停止する。

####2-2. Redisサーバ起動
####2-2-1. Redis-Masterサーバ起動
	# redis-server /etc/redis/redis.conf
またはserviveコマンドでredis-serverを起動する

	# service redis-server start
	> redis-server を起動中: [  OK  ]
####2-2-2. Redis-Masterサーバが起動したことを確認
	# ps -ef | grep redis-server | grep -v grep
	> root 444 1 0 12:36 ? 00:00:00 redis-server 10.132.65.161:6379
####2-2-3 Redis-Slaveサーバ起動
	# redis-server /etc/redis/redis.conf
またはserviveコマンドでredis-serverを起動する

	# service redis-server start
	> redis-server を起動中: [  OK  ]

####2-2-4. Redis-Slaveサーバが起動したことを確認
	# ps -ef | grep redis-server | grep -v grep
	> root 444 1 0 12:36 ? 00:00:00 redis-server 10.132.65.162:6379

####2-3. RedisサーバがMaster/Slave構成となっているか確認
####2-3-1. Reids-Masterの場合
	# redis-cli -h 10.132.65.161 -p 6379 info Replication
	> # Replication	> role:master	> connected_slaves:1	> slave0:ip=10.132.65.162,port=6379,state=online,offset=560448,lag=0	> master_repl_offset:560448	> repl_backlog_active:1	> repl_backlog_size:1048576	> repl_backlog_first_byte_offset:2	> repl_backlog_histlen:560447

+ 以下の内容を確認
	+ "role"が"master"となっていること
	+ "connected_slaves"がRedis-Slaveとして立ち上げたサーバー数となっていること
	+ "slave0"がRedis-Slaveとして立ち上げたアドレス、ポートになっていること(Redis-Slaveサーバ数分確認)

####2-3-2. Reids-Slaveの場合
	# redis-cli -h 10.132.65.162 -p 6379 info Replication	> # Replication	> role:slave	> master_host:10.132.65.161	> master_port:6379	> master_link_status:up	> master_last_io_seconds_ago:1	> master_sync_in_progress:0	> slave_repl_offset:561116	> slave_priority:100	> slave_read_only:1	> connected_slaves:0	> master_repl_offset:0	> repl_backlog_active:0	> repl_backlog_size:1048576	> repl_backlog_first_byte_offset:0	> repl_backlog_histlen:0
+ 以下の内容を確認
	+ "role"が"slave"となっていること
	+ "master-host"がRedis-Masterのアドレスになっていること
	+ "master-port"がRedis-Masterのポートになっていること
	+ "master-link-status"が"up"になっていること(Redis-Masterが立ち上がっていない場合は"down"となる)

####2-4. Redis-Sentinelサーバ起動
※ Redis-SentinelサーバはRedis-Masterサーバ、Redis-Slaveサーバを起動した各サーバでそれぞれ起動する。

####2-4-1. Redis-Sentinelサーバが起動していない事を確認
	# ps -ef | grep redis-server | grep 26379 | grep -v grep
	#
+ 起動してた場合はサーバ停止手順を参考にRedis-Sentinelサーバを停止する。

####2-4-2. Redis-Sentinelサーバ起動
	# redis-server /etc/redis/sentinel.conf --sentinel
####2-4-3. Redis-Sentinelサーバ状態確認

	# redis-cli -p 26379 info sentinel
	# Sentinel
	sentinel_masters:1
	sentinel_tilt:0
	sentinel_running_scripts:0
	sentinel_scripts_queue_length:0
	master0:name=mymaster,status=ok,address=10.132.65.161:6379,slaves=1,sentinels=2
+ 以下の内容を確認
	+ "master0"の"address"がRedis-Masterのアドレス、ポートになっていること

<a id="sec302"></a>
## Conferenceサーバ起動手順※ 以下の作業はnode-userユーザにて作業を行う。

###1. Conferenceサーバが起動していない事を確認する
	# ps -ef | grep node | grep -v grep
	#

またはpm2から確認する

	# pm2 list
	> ┌──────────┬────┬──────┬─────┬────────┬───────────┬────────┬────────┬──────────┐	> │ App name │ id │ mode │ PID │ status │ restarted │ uptime │ memory │ watching │	> └──────────┴────┴──────┴─────┴────────┴───────────┴────────┴────────┴──────────┘
+ 起動してた場合はサーバ停止手順を参考にConferenceサーバを停止する。

###2. Conferenceサーバを起動する
	# cd "conferenceServer格納先"
	# pm2 start bin/main.js	> [PM2] Process bin/main.js launched	> ┌──────────┬────┬──────┬──────┬────────┬───────────┬────────┬─────────────┬─────────────┐	> │ App name │ id │ mode │ PID  │ status │ restarted │ uptime │      memory │    watching │	> ├──────────┼────┼──────┼──────┼────────┼───────────┼────────┼─────────────┼─────────────┤	> │ main     │ 1  │ fork │ 3385 │ online │         0 │ 0s     │ 19.656 MB   │ unactivated │	> └──────────┴────┴──────┴──────┴────────┴───────────┴────────┴─────────────┴─────────────┘	>  Use `pm2 desc[ribe] <id>` to get more details

###3. Conferenceサーバが起動した事を確認

ps コマンドの確認

	# ps -ef | grep node | grep -v grep	> root　737 22546　0 12:39 ?　00:00:04 node /home/project/v5LiteConference/bin/main.js	> root　755　737　0 12:39 ?　00:00:13 /usr/local/bin/node --debug-port=5859 ./bin/sub.js	> root　758　737　0 12:39 ?　00:00:15 /usr/local/bin/node --debug-port=5860 ./bin/sub.js	> root　3509　3496　0 13:18 pts/0　00:00:04 node /usr/local/bin/pm2 logs+ 以下の内容を確認
	+ main.jsが起動していること
	+ sub.jsがCPUのコア数分起動している事

pm2 listの確認

	# pm2 list	> ┌──────────┬────┬──────┬──────┬────────┬───────────┬────────┬─────────────┬─────────────┐	> │ App name │ id │ mode │ PID  │ status │ restarted │ uptime │      memory │    watching │	> ├──────────┼────┼──────┼──────┼────────┼───────────┼────────┼─────────────┼─────────────┤	> │ main     │ 1  │ fork │ 3385 │ online │         0 │ 0s     │ 19.656 MB   │ unactivated │	> └──────────┴────┴──────┴──────┴────────┴───────────┴────────┴─────────────┴─────────────┘	>  Use `pm2 desc[ribe] <id>` to get more details
+ 以下の内容を確認
	+ "status"が"online"となっていること
	+ "restarted"の数が増えていない事

---
<a id="sec400"></a>
# サーバ停止手順

<a id="sec401"></a>
## Redisサーバ停止手順
※ 以下の作業はrootユーザにて作業を行う。

※ サーバの停止順序は

+ 1. Redis-Sentinel
+ 2. Redis-Slave
+ 3. Redis-Master

の順序で停止すること

以下のコマンドを実行する。
###1. Redis-Sentinelサーバ停止
Redisクライアントから停止させる場合は、以下のコマンドを実行する。

	# redis-cli -p 26379 shutdown

killコマンドから停止させる場合は、以下のコマンドを実行する

	# ps -af | grep redis-server | grep 26379 | grep -v grep
	> root 5088 1 0 12:36 ? 00:00:00 redis-server *:26379
	# kill 5088

###2. Redisサーバ停止(Master/Slave)
Redisクライアントから停止させる場合は、以下のコマンドを実行する。

	# redis-cli -h 10.132.65.161 -p 6379 shutdown

killコマンドから停止させる場合は、以下のコマンドを実行する

	# ps -af | grep redis-server | grep -v grep
	> root 444 1 0 12:36 ? 00:00:00 redis-server 10.132.65.161:6379
	# kill 444
	
+ Master/Slave構成となっている場合は、Redis-Slaveから停止させること。

###3. Redisサーバが停止した事を確認
	# ps -af | grep redis-server | grep -v grep
	#

<a id="sec402"></a>
## Conferenceサーバ停止手順
※ 以下の作業はnode-userユーザにて作業を行う。

以下のコマンドを実行する。
###1. Conferenceサーバ停止
pm2から停止させる場合は、以下のコマンドを実行する。

プロセス停止

	# pm2 stop main 	> [PM2] Stopping main	> [PM2] stopProcessId process id 1	> ┌──────────┬────┬──────┬──────┬─────────┬───────────┬────────┬────────┬─────────────┐	> │ App name │ id │ mode │ PID  │ status  │ restarted │ uptime │ memory │    watching │	> ├──────────┼────┼──────┼──────┼─────────┼───────────┼────────┼────────┼─────────────┤	> │ main     │ 1  │ fork │ 5550 │ stopped │         0 │ 0      │ 0 B    │ unactivated │	> └──────────┴────┴──────┴──────┴─────────┴───────────┴────────┴────────┴─────────────┘	>  Use `pm2 desc[ribe] <id>` to get more details

プロセス削除

	# pm2 delete main 	> [PM2] Deleting main process	> [PM2] deleteProcessId process id 1	> ┌──────────┬────┬──────┬─────┬────────┬───────────┬────────┬────────┬──────────┐	> │ App name │ id │ mode │ PID │ status │ restarted │ uptime │ memory │ watching │	> └──────────┴────┴──────┴─────┴────────┴───────────┴────────┴────────┴──────────┘	>  Use `pm2 desc[ribe] <id>` to get more detailskillコマンドから停止させる場合は、以下のコマンドを実行する

	# ps -ef | grep node | grep -v grep	> root　737 22546　0 12:39 ?　00:00:04 node /home/project/v5LiteConference/bin/main.js	> root　755　737　0 12:39 ?　00:00:13 /usr/local/bin/node --debug-port=5859 ./bin/sub.js	> root　758　737　0 12:39 ?　00:00:15 /usr/local/bin/node --debug-port=5860 ./bin/sub.js	> root　3509　3496　0 13:18 pts/0　00:00:04 node /usr/local/bin/pm2 logs
	
	# kill 737
	# kill 755 758

+ killコマンドで停止させる場合は"main.js"から停止させること。<br>
"sub.js"から停止させようとすると、mainがsub停止を検知し、自動で復旧させてしまうため。

main.jsの親プロセスがpm2の場合は、以下のコマンドを実行する。

	# pm2 kill
	
###2. Conferenceサーバの停止を確認
	# ps -af | grep node | grep -v grep
	#

またはpm2から確認する

	# pm2 list
	> ┌──────────┬────┬──────┬─────┬────────┬───────────┬────────┬────────┬──────────┐	> │ App name │ id │ mode │ PID │ status │ restarted │ uptime │ memory │ watching │	> └──────────┴────┴──────┴─────┴────────┴───────────┴────────┴────────┴──────────┘
	
<a id="sec900"></a>
# 付録
<a id="sec901"></a>
## redisのデーモン化
※ 以下の作業はrootユーザにて作業を行う。

### 1. sysv-rc-confをインストールする
	# apt-get install sysv-rc-conf

### 2. redis-serverを登録する
	# sysv-rc-conf redis-server on
### 3. デーモン化の確認
	# sysv-rc-conf redis-server --list | grep redis-server
	> redis-server 0:off	1:off	2:on	3:on	4:on	5:on	6:off

<a id="sec902"></a>
## Conferenceサーバのデーモン化
※ 以下の作業はrootユーザにて作業を行う。

### 1. sysv-rc-confをインストールする
※すでにインストール済みの場合は不要

	# apt-get install sysv-rc-conf

### 2. PM2起動ファイルを作成する
	# pm2 startup ubuntu
	
※このコマンドを実行すると、/etc/init.d/pm2-init.sh が作成され 各runlevelディレクトリにリンクが貼られる。

### 3. PM2起動ファイルの記載を変更する

	# vi /etc/init.d/pm2-init.sh
   

以下の情報を追加する。


~~~
変更前
 42 start() {
 43     echo "Starting $NAME"
 44     super $PM2 resurrect
 45 }

変更後
 42 start() {
 43     echo "Starting $NAME"
 44     super $PM2 resurrect
 45     cd /home/project/v5LiteConference/
 46     sudo -u node-user pm2 start bin/main.js
 47 }
~~~

### 4. PM2を登録する
	# sysv-rc-conf pm2-init.sh on

### 5. PM2起動順序の変更（必要時のみ）
Conferenceサーバは Redisサーバの起動後に起動する必要がある。

そのため、各runlevelディレクトリ配下の起動順番が Redis -> pm2  の順番であることを確認する。

	// S が起動を表し、数字が小さいほど優先度が高い
	// 下記の場合20番目に redis-server の起動が実行され、80番目に pm2 が起動される
	S20redis-server
	S80pm2-init.sh

変更が必要な場合は、シンボリックリンクをコピーして数値を変更する。元のリンクは削除。

	// 例：rc2.d 配下の場合
	# cd /etc/rc2.d
	# sudo cp -R S20pm2-init.sh S80pm2-init.sh
	# sudo rm S20pm2-init.sh

### 6. デーモン化の確認
	# sysv-rc-conf pm2-init.sh --list | grep pm2
	> pm2          2:on	3:on	4:on	5:on

<a id="sec903"></a>
## conferenceサーバのテスト用config説明
以下にconferenceサーバのテスト用configについて説明する。

### local.json
※ 本機能を反映する場合はconferenceサーバの再起動が必要

	// DEBUG("ON"の場合はDEBUGモード)
	"DEBUG": {
		"PARTICIPANTLIST_FLUSH" : "OFF",
		"USERLISTINFO_FLUSH"    : "OFF",
		"CONFERENCEINFO_FLUSH"  : "OFF",
		"CHATLOG_FLUSH"         : "OFF",
		"PINGPONG_DISPLAY"      : "OFF",
		"LOCAL_MODE"            : "OFF",
		"local" : {
			"redis_addr" : "127.0.0.1",
			"redis_port" : "6379",
			"portaladdr" : "127.0.0.1",
			"portalport" : "8000"
		}
	}
+ PARTICIPANTLIST_FLUSH (Default:"OFF")
	+ "ON" : Conferenceサーバ起動時にParticipantListをクリアしない
	+ "OFF" : Conferenceサーバ起動時にParticipantListをクリアする
+ USERLISTINFO_FLUSH (Default:"OFF")
	+ "ON" : Conferenceサーバ起動時にユーザ情報をクリアしない
	+ "OFF" : Conferenceサーバ起動時にユーザ情報をクリアする
+ CONFERENCEINFO_FLUSH (Default:"OFF")
	+ "ON" : Conferenceサーバ起動時に会議情報をクリアしない
	+ "OFF" : Conferenceサーバ起動時に会議情報をクリアする
+ CHATLOG_FLUSH (Default:"OFF")
	+ "ON" : Conferenceサーバ起動時にチャットログをクリアしない
	+ "OFF" : Conferenceサーバ起動時にチャットログをクリアする
+ PINGPONG_DISPLAY (Default:"OFF")
	+ "ON" : Conferenceサーバログにping/pongログを出力する
	+ "OFF" : Conferenceサーバログにping/pongログを出力しない
+ LOCAL_MODE (Default:"OFF" ローカルPC作業用の為、詳細説明は割愛)
	+ "ON" : "local"に設定したRedisサーバ、ポータルサーバのアドレス、ポートを有効にする	
	+ "OFF" :  "local"に設定したRedisサーバ、ポータルサーバのアドレス、ポートを有効にしない
+ local (LOCAL_MODE:"ON"の場合に機能する)
	+ redis_addr : ローカルPCのRedisサーバアドレス
	+ redis_port : ローカルPCのRedisサーバポート
	+ portaladdr : ローカルPCのポータルサーバアドレス
	+ portalport : ローカルPCのポータルサーバポート

### logConfig.json
※ 本機能の反映はconferenceサーバの再起動不要(60秒周期で反映される)

	"levels" : {
		"node" : "DEBUG"
	},
+ node : Conferenceサーバログ出力レベル。以下の値を設定可能
	+ TRACE
	+ DEBUG
	+ INFO
	+ WARN
	+ ERROR
	+ FATAL 

	