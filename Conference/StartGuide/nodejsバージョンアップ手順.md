# node.jsバージョンアップ手順
本資料は、node.jsのバージョンアップ手順についてまとめた資料です。  
本手順は、Conference起動ユーザ"node-user"にて実行します。

## 1. Conferenceサーバ停止
### 1.1. Conferenceサーバ停止 
起動中Conferenceサーバを全て停止させる。  

※ Conferenceサーバ停止手順については、"Conferenceサーバ 運用マニュアル"のConferenceサーバ停止手順を参照

### 1.2. Conferenceサーバの停止状態確認
Conferenceサーバが起動していない事を確認する。

~~~
# ps -ef | grep node | grep -v grep
~~~

## 2. node.jsバージョンアップ
### 2.1. node.js最新バージョンインストール
nvmアプリケーションを使用し、node.jsの最新バージョンをインストールする。  

~~~
# nvm install "node.jsバージョン"
ex) nvm install v0.12.6
> ######################################################################## 100.0%
> Now using node v0.12.6 (npm v2.11.2)
~~~

### 2.2. node.jsインストール確認
nvmアプリケーションを使用し、node.jsのバージョンが最新バージョンを指している事を確認する。

~~~
# nvm ls
>         v0.12.0
> ->      v0.12.6
>          system
> default -> v0.12 (-> v0.12.6)
> node -> stable (-> v0.12.6) (default)
> stable -> 0.12 (-> v0.12.6) (default)
> iojs -> iojs- (-> N/A) (default)
~~~

### 2.3 最新バージョンデフォルト設定
ログインしなおすとnode.jsのバージョンが前バージョンに戻ってしまう為、最新バージョンをデフォルト設定する

~~~
# nvm alias default "node.jsバージョン"  
ex) nvm alias default v0.12.6  
> default -> v0.12 (-> v0.12.6)
~~~

## 3. ケーパビリティ設定
### 3.1. ケーパビリティ設定
Conferenceサーバポートが443を使用しているので、1024番未満のポートを使用できるように設定する。

~~~
# sudo setcap cap_net_bind_service=+ep /home/node-user/.nvm/versions/node/"node.jsバージョン"/bin/node
ex) sudo setcap cap_net_bind_service=+ep /home/node-user/.nvm/versions/node/v0.12.6/bin/node
~~~

### 3.2 ケーパビリティ確認
設定した情報が反映されているか確認する。

~~~
# sudo getcap /home/node-user/.nvm/versions/node/"node.jsバージョン"/bin/node
ex) sudo getcap /home/node-user/.nvm/versions/node/v0.12.6/bin/node
> /home/node-user/.nvm/versions/node/v0.12.6/bin/node = cap_net_bind_service+ep
~~~

## 4. Conferenceサーバ起動
### 4.1. Conferenceサーバ起動 
Conferenceサーバを起動させる。  

※ Conferenceサーバ起動手順については、"Conferenceサーバ 運用マニュアル"のConferenceサーバ起動手順を参照

### 4.2. Conferenceサーバの起動状態確認
Conferenceサーバが起動している事を確認する。  

~~~
# ps -ef | grep node | grep -v grep
> node-us+  1299  1291  0 Jul09 ?        00:00:18 node /home/project/v5LiteConference/bin/main.js> node-us+  1316  1299  0 Jul09 ?        00:00:05 /home/node-user/.nvm/versions/node/v0.12.6/bin/node --debug-port=5859 ./bin/sub.js> node-us+  1317  1299  0 Jul09 ?        00:00:04 /home/node-user/.nvm/versions/node/v0.12.6/bin/node --debug-port=5860 ./bin/sub.js
~~~
※ node.jsのバージョンが最新版で起動している事を確認する。  
※ sub.jsの起動はサーバ環境によって異なる。  

