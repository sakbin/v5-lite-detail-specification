# Conferenceサーバ 運用マニュアル
本資料は、Conferenceサーバの運用についてまとめた資料です。

## 目次
+ サーバの操作
	+ [Conferenceサーバ起動手順](#sec101)
	+ [Conferenceサーバ停止手順](#sec201)
	+ [Conferenceサーバアップデート手順](#sec301)


+ AdminToolによるサーバ操作
	+ [AdminTool操作手順](#sec401)
	+ [conferenceサーバ起動 / 停止 / 再起動](#sec411)	+ [会議情報出力](#sec421)	+ [会議情報 / ユーザ情報検索](#sec431)	+ [会議情報削除](#sec441)	+ [ユーザ情報削除](#sec451)


<a id="sec101"></a>
## Conferenceサーバ起動手順※ Conferenceサーバを起動させる場合は、ポータルサーバが起動済みであること。

###1. 本番環境ログイン
Conferenceサーバにログインする。

	# ssh "V5LiteConferenceサーバアドレス" -l "ユーザ名"

###2. ユーザ変更
Conferenceサーバ起動ユーザに変更する。

	# sudo su - node-user

###3. Conferenceサーバの停止状態確認
Conferenceサーバが起動していない事を確認する。

psコマンドで確認する場合

	# ps -ef | grep node | grep -v grep

pm2で確認する場合

	# pm2 list
	> ┌──────────┬────┬──────┬─────┬────────┬───────────┬────────┬────────┬──────────┐	> │ App name │ id │ mode │ PID │ status │ restarted │ uptime │ memory │ watching │	> └──────────┴────┴──────┴─────┴────────┴───────────┴────────┴────────┴──────────┘
+ 起動してた場合は[Conferenceサーバ停止手順](#sec201)を参考にConferenceサーバを停止する。

###4. Conferenceサーバ起動
pm2(デーモンツール)を使用し、Conferenceサーバを起動させる。

	# cd /home/project/v5LiteConference
	# pm2 start bin/main.js	> [PM2] Process bin/main.js launched	> ┌──────────┬────┬──────┬──────┬────────┬───────────┬────────┬─────────────┬─────────────┐	> │ App name │ id │ mode │ PID  │ status │ restarted │ uptime │      memory │    watching │	> ├──────────┼────┼──────┼──────┼────────┼───────────┼────────┼─────────────┼─────────────┤	> │ main     │ 1  │ fork │ 3385 │ online │         0 │ 0s     │ 19.656 MB   │ unactivated │	> └──────────┴────┴──────┴──────┴────────┴───────────┴────────┴─────────────┴─────────────┘	>  Use `pm2 desc[ribe] <id>` to get more details

###5. Conferenceサーバの起動状態確認
Conferenceサーバが起動している事を確認する。

psコマンドで確認する場合

	# ps -ef | grep node | grep -v grep	> node-us+ 19018     1  0 Apr30 ?        00:02:18 PM2 v0.12.7: God Daemon	> node-us+ 19027 19018  0 Apr30 ?        00:03:48 node /home/project/v5LiteConference/bin/main.js	> node-us+ 19045 19027  0 Apr30 ?        00:00:29 /usr/local/bin/node --debug-port=5859 ./bin/sub.js	> node-us+ 19050 19027  0 Apr30 ?        00:00:30 /usr/local/bin/node --debug-port=5860 ./bin/sub.js	> node-us+ 19055 19027  0 Apr30 ?        00:00:31 /usr/local/bin/node --debug-port=5861 ./bin/sub.js	> node-us+ 19061 19027  0 Apr30 ?        00:00:40 /usr/local/bin/node --debug-port=5862 ./bin/sub.js	> node-us+ 19066 19027  0 Apr30 ?        00:00:28 /usr/local/bin/node --debug-port=5863 ./bin/sub.js	> node-us+ 19071 19027  0 Apr30 ?        00:00:29 /usr/local/bin/node --debug-port=5864 ./bin/sub.js	> node-us+ 19076 19027  0 Apr30 ?        00:00:31 /usr/local/bin/node --debug-port=5865 ./bin/sub.js	> node-us+ 19081 19027  0 Apr30 ?        00:00:41 /usr/local/bin/node --debug-port=5866 ./bin/sub.js+ 以下の内容を確認
	+ main.jsが起動していること
	+ sub.jsがCPUのコア数分起動している事

pm2で確認する場合

	# pm2 list	┌──────────┬────┬──────┬───────┬────────┬─────────┬────────┬─────────────┬──────────┐	│ App name │ id │ mode │ pid   │ status │ restart │ uptime │ memory      │ watching │	├──────────┼────┼──────┼───────┼────────┼─────────┼────────┼─────────────┼──────────┤	│ main     │ 0  │ fork │ 19027 │ online │ 0       │ 0s     │ 62.199 MB   │ disabled │	└──────────┴────┴──────┴───────┴────────┴─────────┴────────┴─────────────┴──────────┘	 Use `pm2 show <id|name>` to get more details about an app

+ 以下の内容を確認
	+ "status"が"online"となっていること
	+ "restarted"の数が増えていない事

<a id="sec201"></a>
## Conferenceサーバ停止手順###1. 本番環境ログイン
Conferenceサーバを停止させる本番環境にログインする。

	# ssh "V5LiteConferenceサーバアドレス" -l "ユーザ名"

###2. ユーザ変更
Conferenceサーバ起動ユーザに変更する。

	# sudo su - node-user

###3. Conferenceサーバ停止
pm2(デーモンツール)を使用し、Conferenceサーバ、デーモンツールを停止させる。

	# pm2 delete main	> [PM2] Deleting main process	> [PM2] deleteProcessId process id 1	> ┌──────────┬────┬──────┬─────┬────────┬─────────┬────────┬────────┬──────────┐	> │ App name │ id │ mode │ pid │ status │ restart │ uptime │ memory │ watching │	> └──────────┴────┴──────┴─────┴────────┴─────────┴────────┴────────┴──────────┘	>  Use `pm2 show <id|name>` to get more details about an app
	# pm2 kill	> [PM2] Stopping PM2...	> [PM2][WARN] No process found	> [PM2] All processes has been stopped and deleted	> [PM2] PM2 stopped

強制的にkillコマンドで停止させる場合は以下のコマンドを実行する。

	# ps -ef | grep PM2 | grep -v grep	> node-us+ 19018     1  0 Apr30 ?        00:02:19 PM2 v0.12.7: God Daemon	# kill 19018
※デーモンツール強制終了後にConferenceサーバが起動(ゾンビ状態)していた場合は、Conferenceサーバも強制終了させる。

	# ps -ef | grep node | grep main | grep -v grep	> node-us+ 19027 19018  0 Apr30 ?        00:03:48 node /home/project/v5LiteConference/bin/main.js	# kill 19027

###4. Conferenceサーバの停止状態確認
Conferenceサーバが起動していない事を確認する。

psコマンドで確認する場合

	# ps -ef | grep node | grep -v grep

pm2で確認する場合

	# pm2 list
	> ┌──────────┬────┬──────┬─────┬────────┬───────────┬────────┬────────┬──────────┐	> │ App name │ id │ mode │ PID │ status │ restarted │ uptime │ memory │ watching │	> └──────────┴────┴──────┴─────┴────────┴───────────┴────────┴────────┴──────────┘

<a id="sec301"></a>
## Conferenceサーバアップデート手順###1. ConferenceサーバソースファイルをDL
リポジトリから最新のConferenceサーバソースファイルをDLし、ファイルを圧縮する。

	# tar czvfo conference.tgz ./conference

###2. Conferenceサーバソースファイル転送
WinSCP、FileZilla等ファイル転送ツールを使用し、本番環境サーバの任意の一時領域にソースファイルを転送する。

###3. 本番環境ログイン
Conferenceサーバを起動させる本番環境にログインする。

	# ssh "V5LiteConferenceサーバアドレス" -l "ユーザ名"

###4. ユーザ変更
Conferenceサーバ起動ユーザに変更する。

	# sudo su - node-user

###5. Conferenceサーバ停止
Conferenceサーバを停止する。<br>
※ [Conferenceサーバ停止手順](#sec201)を参照

###6. Conferenceサーバソースファイル差し替え
転送したソースファイルを、/home/projectディレクトリに移動する。

	# mv ./conference.tgz /home/project/

移動させたソースファイルを展開する。

	# cd /home/project
	# tar xzovf conference.tgz

※ この時点でディレクトリ構成は以下の形となっている

+ /home/project/v5LiteConference ... 既存の動作用ソースディレクトリ
+ /home/project/conference ... 展開された一時ソースディレクトリ

configファイルを既存のものからコピーする。<br>
※ 各サーバで設定値が違うため、それを引き継ぐ

	# cp -p ./v5LiteConference/config/* ./conference/config/

※ 必要であれば、ログファイルも引き継ぎを行う。

	# cp -p ./v5LiteConference/log/* ./conference/log/

既存のソースディレクトリをバックアップリネームする。

	# mv ./v5LiteConference ./v5LiteConference_oldYYYYMMDD

最新のソースディレクトリを置き換える。

	# mv ./conference ./v5LiteConference

###7. Conferenceサーバ起動
Conferenceサーバを起動する。<br>
※ [Conferenceサーバ起動手順](#sec101)を参照

<a id="sec401"></a>
## AdminTool操作手順本ツールは、Conferenceサーバの起動/停止、開催中の会議情報取得等の補助ツールとなります。

### AdminTool格納先
+ /home/project/v5LiteConference/sh/

### AdminToolファイル名
+ AdminTool.sh

### 起動方法
	# cd /home/project/v5LiteConference/sh/
	# ./AdminTool.sh

### 実行方法
対話形式ですので、実行したい番号、またはY/Nを選択してください。
### AdminToolメニュー構成
AdminTool.sh<br>
┣ conferenceサーバ起動/停止/再起動 メニュー<br>
┃ ┣ 起動<br>
┃ ┣ 停止<br>
┃ ┗ 再起動<br>
┣ 会議情報出力 メニュー<br>
┃ ┗ 開催中会議の詳細情報<br>
┣ 会議情報/ユーザ情報検索<br>
┃ ┗ 開催中会議にログインしているユーザ一覧<br>
┣ 会議情報削除 メニュー<br>
┃ ┗ 指定会議の強制終了<br>
┗ ユーザ情報削除 メニュー<br>
　 ┗ 指定ユーザの強制削除<br>

***

<a id="sec411"></a>###1. conferenceサーバ起動/停止/再起動
※ 本機能を実行する場合は、node-userで実行する必要があります。	#####################################
	# conferenceサーバ 起動/停止/再起動    #
	#####################################
	 1: 起動
	 2: 停止
	 3: 再起動
	 ------------------------------------
	 b: 戻る
	 q: 終了
	-------------------------------------
	cmd :1. 起動<br>
	Conferenceサーバを起動させます。
	Conferenceサーバがすでに起動中の場合は実行できませんので、先に停止を実行してください。
2. 停止<br>
	Conferenceサーバを停止させます。
	Conferenceサーバがすでに停止中の場合は実行できません。
3. 再起動<br>
	Conferenceサーバを再起動させます。
	Conferenceサーバがすでに起動中の場合は実行できません。

<a id="sec421"></a>###2. 会議情報出力
※ 本機能はnode-userでなくとも実行可能です。

	###############################################
	# 会議情報出力                                  #
	# RedisServer Addr:10.132.139.117  Port:6379  #
	# Date 15/05/13_17:57:28                      #
	###############################################
	  0:Room 10 (13570da54e441bee9d79b521bc47e25) (1 / 30)
	  1:ROOM2 (61d208a27eca26c5873f6f84fcdc6d1) (1 / 30)
	  2:ROOM3 (cea267f7e54dc7b349a398fa282f747) (1 / 50)
	  3:Room 2 (2096768ef389c77c140c7703c3fed28) (2 / 30)
	 --------------------------------------  
	  r:再読み込み
	  b:戻る
	  q:終了
	-----------------------------------------
	cmd :

* 開催中会議の詳細情報<br>
	現在開催中の会議リストを出力します。
	詳細情報を参照したい会議の番号を選択すると、会議情報/ParticipantList/ユーザ情報/チャットログ/サーバ情報が参照できます。

		cmd :0
		[会議情報]
		 ConferenceID        : 13570da54e441bee9d79b521bc47e25
		 ConferenceName      :
		 RoomName            : Room 10
		 PastTime            : 0
		 TimerStart          : 0
		 MaxParticipants     : 30
		 NumParticipants     : 1
		 ConnParticipants    : 0
		 LostParticipants    : 0
		 IsReserved          : false
		 CanExtendPeriod     : true
		 ReserveFlg          : false
		 ConferenceStartTime : 1431507443
		 ConferenceEndTime   : 0
		 RemainTime          : 0
		 IsLocked            : false
		 ConferenceURL       : https://mtg5l.vcube.com/g/ja/13570da54e441bee9d79b521bc47e25/1854818c4281e455b327f79e9e8f514
		 ConferencePinCode   : 45297740
		 DocumentSender      : none
		 IsRecoading         : false

		[ParticipantList]
		 Revision            : 1
		 CsAPI1431482918-82-e9816078ffd99d22-c37fb1db05fea516: {"display_name":"手塚 匠哉","participant_id":"CsAPI1431482918-82-e9816078ffd99d22-c37fb1db05fea516"}

		[ユーザ情報]
		 Token               : e6fcdf26ead91ca14b5aae78f295ca7
		 Status              : LOGIN
		 ConferenceID        : 13570da54e441bee9d79b521bc47e25
		 ParticipantID       : CsAPI1431482918-82-e9816078ffd99d22-c37fb1db05fea516
		 DisplayName         : 手塚 匠哉
		 IsGuest             : 0
		 ServerName          : mtg5l-cf-01.vcube.com
		 WorkerID            : 2
		 Detail              :

		[チャットログ]
		    0 {"from":"CsAPI1431482918-82-e9816078ffd99d22-c37fb1db05fea516","conference":"13570da54e441bee9d79b521bc47e25","data":{"message":"メッセージ１","displayname":"手塚 匠哉","timestamp":"1431507600"}}
		    1 {"from":"CsAPI1431482918-82-e9816078ffd99d22-c37fb1db05fea516","conference":"13570da54e441bee9d79b521bc47e25","data":{"message":"メッセージ２","displayname":"手塚 匠哉","timestamp":"1431507602"}}

		[サーバ情報]
		 ServerName          : mtg5l-cf-01.vcube.com
		 LoginUserNum        : 2

		 ServerName          : mtg5l-cf-02.vcube.com
		 LoginUserNum        : 3

<a id="sec431"></a>
###3. 会議情報/ユーザ情報検索
※ 本機能はnode-userでなくとも実行可能です。

	###############################################
	# 会議情報/ユーザ情報検索                         #
	# RedisServer Addr:10.132.139.117  Port:6379  #
	###############################################
	 ■ ConferenceID:13570da54e441bee9d79b521bc47e25 (1 / 30)
	 ┗  e6fcdf26ead91ca14b5aae78f295ca7 [LOGIN]

	 ■ ConferenceID:61d208a27eca26c5873f6f84fcdc6d1 (1 / 30)
	 ┗  555cb9c209798bc5e154fe1f5f314e5 [LOGIN]

	 ■ ConferenceID:cea267f7e54dc7b349a398fa282f747 (2 / 50)
	 ┗  9a0559f8a61fb9ca18af0e7e6eb7fc5 [LOGIN]
	 ┗  a231287e89f7411ebe8e035b78ec00a [LOGIN]

	 ■ ConferenceID:2096768ef389c77c140c7703c3fed28 (2 / 30)
	 ┗  f4c34ea8b2f27587de036b63fba0190 [LOGIN]
	 ┗  f20e48e48ca80681638fef8e336ff99 [LOGIN]


	 -- please enter key --


* 開催中会議にログインしているユーザ一覧<br>
	現在開催中の会議に何人ログインしているか、また、ログインユーザのステータス状態を出力します。<br>

	もし、どの会議にも属しないユーザが居た場合は、ユーザが浮いている旨の表示をしますので、ユーザの確認、ログ情報収集等の情報取得後に、別メニューの"ユーザ情報削除"を選択し、ユーザ情報をクリアしてください。<br>
	また、会議情報が浮いている場合は、ログインユーザ無しの状態で出力されます。会議の確認、ログ情報収集等の情報取得後に、別メニューの"会議情報削除"を選択し、会議情報をクリアしてください。

		###############################################		# 会議情報/ユーザ情報検索                         #		# RedisServer Addr:10.132.139.117  Port:6379  #		###############################################		 ■ ConferenceID:13570da54e441bee9d79b521bc47e25 (1 / 30)		 ┗  e6fcdf26ead91ca14b5aae78f295ca7 [LOGIN]		  :
		 ■ ConferenceID:aabbcc1234567890123456789012345 (0 / 30)		 ユーザ浮き状態		 ★  a123456789012345678901234567890 [CLOSE] ConferenceID:[1234567890123456789012345678901]		 -- please enter key --<a id="sec441"></a>###4. 会議情報削除
※ 本機能を実行する場合は、node-userで実行する必要があります。	###############################################
	# 会議情報削除                                  #
	# RedisServer Addr:10.132.139.117  Port:6379  #
	###############################################
	  0:Room 10 (13570da54e441bee9d79b521bc47e25)
	  1:ROOM2 (61d208a27eca26c5873f6f84fcdc6d1)
	  2:ROOM3 (cea267f7e54dc7b349a398fa282f747)
	  3:Room 2 (2096768ef389c77c140c7703c3fed28)
	 --------------------------------------  
	  i:conferenceID 手動入力
	  r:再読み込み
	  b:戻る
	  q:終了
	-----------------------------------------
	cmd :* 指定会議(番号指定)の強制終了<br>	現在開催中の会議リストから、指定した会議を強制終了させます。
	強制終了実行時はログイン中のユーザを強制退室させます。

* 指定会議(手動入力)の強制終了<br>
	リストに存在しない場合、手動入力で会議IDを入力する事で、対象会議IDの会議を強制終了させます。
	強制終了実行時はログイン中のユーザを強制退室させます。

<a id="sec451"></a>
###5. ユーザ情報削除
※ 本機能を実行する場合は、node-userで実行する必要があります。	###############################################
	# ユーザ情報削除                                #
	# RedisServer Addr:10.132.139.117  Port:6379  #
	###############################################
	  0:9a0559f8a61fb9ca18af0e7e6eb7fc5 [LOGIN] ConferenceID:[cea267f7e54dc7b349a398fa282f747]
	  1:a231287e89f7411ebe8e035b78ec00a [LOGIN] ConferenceID:[cea267f7e54dc7b349a398fa282f747]
	  2:f4c34ea8b2f27587de036b63fba0190 [LOGIN] ConferenceID:[2096768ef389c77c140c7703c3fed28]
	  3:f20e48e48ca80681638fef8e336ff99 [LOGIN] ConferenceID:[2096768ef389c77c140c7703c3fed28]
	  4:e6fcdf26ead91ca14b5aae78f295ca7 [LOGIN] ConferenceID:[13570da54e441bee9d79b521bc47e25]
	  5:555cb9c209798bc5e154fe1f5f314e5 [LOGIN] ConferenceID:[61d208a27eca26c5873f6f84fcdc6d1]
	 --------------------------------------  
	  i:Token 手動入力
	  r:再読み込み
	  b:戻る
	  q:終了
	-----------------------------------------
	cmd :* 指定ユーザ(番号指定)の強制削除<br>	現在ログイン中のユーザリストから、指定したユーザの情報を削除します。
	ユーザがログイン中であった場合、ユーザの会議継続不可とし、強制退室させます。
* 指定ユーザ(手動入力)の強制削除<br>	リストに存在しない場合、手動入力でTokenを指定する事で、対象Tokenのユーザ情報を削除します。
	ユーザがログイン中であった場合、ユーザの会議継続不可とし、強制退室させます。
