# Conference内部仕様説明
本資料はConferenceサーバの処理内容について記載した資料です。

## 目次

+ [Conferenceサーバ全体構成図](#sec001)
+ [機能説明](#sec100)
	+ [プロセス群](#sec110)
		+ [起動プロセス(main.js)](#sec111)
		+ [ワーカプロセス(sub.js)](#sec112)
		+ [メイン機能(app.js)](#sec113)
		+ [コネクトタイマプロセス(connectChild.js)](#sec114)
		+ [リコネクトタイマプロセス(reconnectChild.js)](#sec115)
		+ [予約会議タイマプロセス(reserveChild.js)](#sec116)
	+ [ライブラリ群](#sec120)
		+ [イベント管理(socketInfo.js)](#sec121)
		+ [接続処理(connectInfo.js)](#sec122)
		+ [切断処理(closeInfo.js)](#sec123)
		+ [メッセージ作成(messageInfo.js)](#sec124)
		+ [ポータル管理(portalInfo.js)](#sec125)
		+ [ポータルイベント受信(portalReceive.js)](#sec1213)
		+ [ユーザ情報管理(userListInfo.js)](#sec126)
		+ [パーミッション管理(permissionInfo.js)](#sec1214)
		+ [Participant管理(participantInfo.js)](#sec127)
		+ [会議情報管理(ConferenceInfo.js)](#sec128)
		+ [サーバ情報管理(serverInfo.js)](#sec129)
		+ [チャットログ管理(chatData.js)](#sec1210)
		+ [Redis管理(redisInfo.js)](#sec1211)
		+ [ログ管理(nodeLogs.js)](#sec1212)
		+ [録画機能管理(recordInfo.js)](#sec1215)
		+ [指差し機能管理(remotePointingInfo.js)](#sec1216)
		+ [アンケート機能管理(enqueteInfo.js)](#sec1217)
	+ [コンフィグ群](#sec130)
		+ [サーバコンフィグ(local.json)](#sec131)
		+ [ログコンフィグ(logConfig.json)](#sec132)
		+ [定義ファイル(default.json)](#sec133)

<a id="sec001"></a>
## Conferenceサーバ全体構成図
![サーバ構成図](./Conferenceサーバ構成図.pdf)

<a id="sec100"></a>
## 機能説明
<a id="sec110"></a>
### プロセス群
--
<a id="sec111"></a>
#### 起動プロセス(main.js)
Conferenceサーバ起動プロセス。
PM2(デーモンツール)から起動する。

主な機能は以下の通り

+ RedisサーバのDB情報クリア
+ クラスタ機能を使用してワーカプロセスの作成
+ ワーカプロセスのイベントメッセージ送受信
+ [クライアント接続タイマプロセス](#sec114)の生成
+ [クライアント再接続タイマプロセス](#sec115)の生成
+ [予約会議タイマプロセス](#sec116)の生成
+ RedisサーバのKeepAlive処理

--
<a id="sec112"></a>
#### ワーカプロセス(sub.js)
[起動プロセス](#sec111)から生成されるワーカプロセス。
後述となる[メイン機能](#sec113)をコールする。

ワーカプロセスはCPUのコア数分生成される。

--
<a id="sec113"></a>
#### メイン機能(app.js)
ワーカプロセスのメイン機能モジュール。[ワーカプロセス](#sec112)からコールされる。
Express4.0フレームワークのテンプレートを改編。

主な機能は以下の通り

+ クライアントのWebsocketイベントリスナー生成

--
<a id="sec114"></a>
#### コネクトタイマプロセス(connectChild.js)
[起動プロセス](#sec111)から生成されるタイマプロセス。
ポータルサーバからPrepareParticipantの受信後に起動する。

[コンフィグファイル](#sec131)に記載された秒間クライアントの接続を監視する。  
接続の確認、またはタイムアウトでプロセスを正常終了させる。

主な機能は以下の通り

+ クライアントの接続監視

--
<a id="sec115"></a>
#### リコネクトタイマプロセス(reconnectChild.js)
[起動プロセス](#sec111)から生成されるタイマプロセス。
クライアントの異常切断、ワーカプロセスの異常終了時に起動する。

[コンフィグファイル](#sec131)に記載された秒間クライアントの再接続を監視する。接続の確認、またはタイムアウトでプロセスを正常終了させる。

主な機能は以下の通り

+ クライアントの再接続監視

--
<a id="sec116"></a>
#### 予約会議タイマプロセス(reconnectChild.js)
[起動プロセス](#sec111)から生成されるタイマプロセス。
会議情報に予約フラグが立っていた場合に起動する。

会議参加者が全て退室、または予約会議終了時間になったらプロセスを正常終了させる。

主な機能は以下の通り

+ 予約会議終了10分前通知
+ 予約会議終了5分前通知
+ 予約会議延長フラグが立っていない場合のクライアント強制退室

--
<a id="sec120"></a>
### ライブラリ群
--
<a id="sec121"></a>####イベント管理(socketInfo.js)Websocketのイベント管理モジュール。[メイン機能](#sec113)からコールされる。

主な機能は以下の通り

+ クライアントの接続イベント受信
+ クライアントの切断イベント受信
+ クライアントのイベントメッセージの送受信
+ pingpongタイマ管理
--<a id="sec122"></a>####接続処理(connectInfo.js)クライアントの接続処理モジュール。クライアントのWebsocket接続処理、再接続処理を扱う。

--<a id="sec123"></a>####切断処理(closeInfo.js)クライアントの切断処理モジュール。クライアントのWebsocket切断処理を扱う。

--<a id="sec124"></a>####メッセージ作成(messageInfo.js)Websocketイベントメッセージ作成モジュール。クライアントに通知するイベントメッセージの成形を扱う。

--<a id="sec125"></a>####ポータル管理(portalInfo.js)ポータルサーバ通知モジュール。HTTP通信の送信処理を扱う。--<a id="sec1213"></a>####ポータルイベント受信(portalReceive.js)ポータルサーバからのイベント受信モジュール。HTTP通信の受信処理を扱う。--<a id="sec126"></a>####ユーザ情報管理(userListInfo.js)クライアント情報の管理モジュール。クライアントの情報を扱う。

クライアント情報は[Redis管理(redisInfo.js)](#sec1211)を経由してRedisサーバに設定、削除を行う。

--<a id="sec1214"></a>####パーミッション管理(permissionInfo.js)クライアントのパーミッション管理モジュール。クライアントのパーミッション情報を扱う。  

パーミッション情報は[Redis管理(redisInfo.js)](#sec1211)を経由してRedisサーバに設定、削除を行う。
--<a id="sec127"></a>####Participant管理(participantInfo.js)ParticipantListの管理モジュール。クライアントのParticipant情報を扱う

Participant情報は[Redis管理(redisInfo.js)](#sec1211)を経由してRedisサーバに設定、削除を行う。
--<a id="sec128"></a>####会議情報管理(ConferenceInfo.js)会議情報の管理モジュール。会議情報を扱う

会議情報は[Redis管理(redisInfo.js)](#sec1211)を経由してRedisサーバに設定、削除を行う。
--<a id="sec129"></a>####サーバ情報管理(serverInfo.js)Conferenceサーバ情報の管理モジュール。Conferenceサーバの情報を扱う。

Conferenceサーバ情報は[Redis管理(redisInfo.js)](#sec1211)を経由してRedisサーバに設定、削除を行う。
--<a id="sec1210"></a>####チャットログ管理(chatData.js)チャットメッセージの管理モジュール。チャットメッセージログを扱う。

チャットメッセージの内容は[Redis管理(redisInfo.js)](#sec1211)を経由してRedisサーバに設定、削除を行う。

--<a id="sec1211"></a>####Redis管理(redisInfo.js)Redisサーバと通信をするためのモジュール。
RedisサーバとやりとりをするためRedisクライアントを作成し、各情報の設定、削除を行う。

--<a id="sec1212"></a>####ログ管理(nodeLogs.js)
Conferenceサーバのログ登録用モジュール。[コンフィグファイル](#sec132)に設定された内容でログをファイルに出力する。

--<a id="sec1215"></a>####録画管理管理(recordInfo.js)
録画機能管理モジュール。録画開始処理、中断処理、終了処理を扱う。

--<a id="sec1216"></a>####指差し機能管理(remotePointingInfo.js)
指差し機能管理モジュール。シェアリングの開始、停止、バインディングの開始、停止を扱う。

--<a id="sec1217"></a>####アンケート機能管理(enqueteInfo.js)
アンケート機能管理モジュール。アンケートの開始、停止、中断、再開、回答集計を扱う。

アンケートの内容は[Redis管理(redisInfo.js)](#sec1211)を経由してRedisサーバに設定、削除を行う。


<a id="sec130"></a>
### コンフィグ群
--
<a id="sec131"></a>
#### サーバコンフィグ(local.json)
コンフィグ設定内容は以下の通り

+ Conferenceサーバ情報
	+ ポート番号
	+ サーバ名
	+ 証明書ファイル名
	+ Websocket認証切り替えフラグ
+ worker起動数
+ Redisサーバ情報
	+ Redisサーバポート番号
	+ Redisサーバアドレス
+ ポータルサーバ情報
	+ ポータルサーバアドレス
+ DEBUG情報
	+ ParticipantLIstDBクリアフラグ
	+ ユーザ情報DBクリアフラグ
	+ 会議情報DBクリアフラグ
	+ チャットログDBクリアフラグ
	+ アンケート情報DBクリアフラグ
	+ PINGPONG表示フラグ
	+ 指差し座標表示フラグ
	+ ローカルテストフラグ

--
<a id="sec132"></a>
#### ログコンフィグ(default.json)
コンフィグ設定内容は以下の通り

+ ファイル名 (node.log)
+ ログローテート日付 (-yyyy-mm-dd)
+ ログ出力レベル

--
<a id="sec133"></a>
#### 定義ファイル(default.json)
コンフィグ設定内容は以下の通り

+ Conferenceサーバ情報
	+ ログインタイムアウト時間 (Default:60秒)
	+ クライアントとのKeepAlive時間 (Default:60秒)
	+ ログアウトタイムアウト時間 (Default:30秒)
	+ 録画タイムアウト時間 (Default:30秒)
+ Redisサーバ情報
	+ RedisとのKeepAlive時間 (Default:1秒)
	+ RedisFailover時間 (Default:9秒)
	+ RedisFailover完了時間 (Default:1秒)
+ パーミッション情報
	+ パーミッション名
	+ パーミッションコード
+ UpdateParticipantコード
+ ユーザステータス
+ ユーザ切断コード
+ 会議終了コード
+ 録画情報
	+ 録画ステータス
	+ 録画コード
	+ 録画サーバコード
+ アンケート情報
	+ アンケートステータス
	+ アンケート種別
