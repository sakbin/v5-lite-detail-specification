#WEB API

詳細設計については下記URLを参照の事
https://docs.google.com/a/vcube.co.jp/spreadsheets/d/1RyoP3qF7Poo5ny7buqIP2WDVD7LxkXokIuzcQJvsj4s/edit#gid=426570493

###更新履歴
日付  | 更新者  | 内容
------------- | ------------- | -------------
2016/5/10  | 登尾 | エラーコード追加
2016/2/13  | 濱名 | PINログイン追加
2015/2/13  | 濱名 | 会議内招待API追加
2015/1/29  | 真太郎 | Sprint2向けの機能を追加
2015/1/28  | 真太郎 | 戻り値としてのParticipantListと<br>Conferenceに送信するParticipantListの２種類を区別するように
2015/1/28  | 濱名  | 戻り値ParticipantListを再修正
2015/1/27  | 濱名  | 戻り値ParticipantListを修正
2015/1/22  | 真太郎  | action_GenerateAuthToken を追加
2015/1/22  | 真太郎  | 変数名にuserがあるものをparticipantで統一
2015/1/19  | 濱名  | CreateTokenをaction\_startに変更
2015/1/19  | 濱名  | GoogleDocs側action\_PrepareJoinConferenceの戻り値箇所修正
2015/1/19  | 濱名  |テキストのAPIドメイン箇所をdev-v5lite.vcube.netに変更
2015/1/19  | 真太郎 | GoogleDocsを最新の仕様とします

##action\_login
###URL : http://dev-v5lite.vcube.net/api/v5lite/user/
ログイン

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_login  | Method  |  - | - | -
◯  | id   | V-CUBE ID   |  string(64) | - | api_test
◯  | pw   | Password   |  string(64) | - | hogehoge1

##action\_pin\_login
###URL : http://dev-v5lite.vcube.net/api/v5lite/user/
ログイン

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_pin\_login  | Method  |  - | - | -
◯  | pin_cd   | PIN Cord   |  int(64) | - | 11111111
◯  | device   | Device   |  string(64) | - | osx
-  | login_mode   | Login Mode   |  string(64) | - | 画面共有モード : sharing

##action\_get\_room\_list
###URL : http://dev-v5lite.vcube.net/api/v5lite/user/
部屋一覧取得

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_get\_room_list  | Method  |  - | - | l6teg8lv9rh35iemee2cjoq7b0
◯  | n2my_session   | SESSION ID   |  string(255) | - | -

##action\_get\_router\_list
###URL : http://dev-v5lite.vcube.net/api/v5lite/user/
部屋一覧取得

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_get\_router_list  | Method  |  - | - |

#####レスポンス
~~~
{
	"tyo":"mtg5l-vr-jp-01.vcube.com",
	"tyo2":"mtg5l-vr-jp-02.vcube.com",
	"sin":"mtg5l-vr-sg-01.vcube.com",
	"hkg":"dev-v5-lite-vrhk01.vcube.net",
	"ams":"mtg5l-vr-nl-01.vcube.com",
	"sjc":"mtg5l-vr-us-01.vcube.com",
	"syd":"mtg5l-vr-au-01.vcube.com",
　}
~~~
上記レスポンスをbase64エンコードして、渡すようにする。
Portal側にはconfig/router_list.jsonで保存して、実行時に読み込むようにする。


##action\_start
###URL : http://dev-v5lite.vcube.net/api/v5lite/user/meeting/
揮発性のToken( volatileToken ) を発行します

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_start  | Method  |  - | - | -
◯  | n2my_session   | Session ID   |  string(255) | - | l6teg8lv9rh35iemee2cjoq7b0
◯  | room_id   | Room ID   |  string(255) | - | api_test-1-6b4e

#####レスポンス
揮発性のToken( volatileToken ) で、ハッシュ前の文字列を返します。
※サーバ側からは、ハッシュ前の値を返しますが、10回ハッシュ化した文字列をサーバで保持し
後に参照することになります。

#client

##API全般のERROR CODE
文字列型で表現される値で、一覧は下記の通りです。

ステータス | code |説明
--------|------|-------------
100<br> パラメーターエラー | 100 |VALID,INVALID,REQUIREDなど詳細はレスポンスに書く
200<br> 成功応答| 200 | 正常に実行完了
300 ~ 302 <br> トークンエラー| 300 |一般的なトークンエラー
| 301 |トークンが渡してない
| 302 |該当トークンが存在しない（有効時間切りの場合にも含め）
| 303 |Volatileトークンが渡してない
| 304 |該当Volatileトークンが存在しない
| 305 |該当Volatileトークンがもう利用した
| 306 |該当Volatileトークンが有効期間切った
| 308 |クライアントアプリケーションが必須最低バージョンを満たしていない
| 309 |メンテナンス中のため、入室できない
| 310 |BOXプラン1のため、入室できない
| 311 |すでに招待済みのため、招待できない
| 312 |利用ポート数上限に達しているため、入室できない
400 ~ 499 <br> サーバーエラー| 400 |一般的なサーバーエラー
| 401 |該当カンファレンスが存在しない（削除する場合も含める）
| 402 |該当カンファレンスが終了
| 403 |該当カンファレンスがまだ開催してない
| 404 |カンファレンス シーケンスが存在しない（DBに追加失敗の原因で）
| 405 |該当カンファレンスサーバーが存在しない
| 406 |該当招待記録が存在しない
| 407 |該当参加者が存在しない（PrepareJoinConference叩いた時参加者データ追加失敗）
| 408 |該当参加者もう退室した
| 409 |該当参加者もう会議室に入った(NoticeJoinedConferenceもう叩いた、もう一度叩けない)
500 ~ 599 <br> カンファレンスサーバエラー| 500 |一般的なカンファレンスサーバーエラー
| 501 |アクセスできるカンファレンスサーバーが存在しない
| 502 |カンファレンス情報をカンファレンスサーバーに送信失敗
| 503 |参加者情報をカンファレンスサーバーに送信失敗
600 ~ 699 <br> Vidyoポータルサーバーエラー| 600 |一般的なカンファレンスサーバーエラー
| 601 |ROOM POOLに使えるVidyo部屋がない
| 602 |USER POOLに使えるVidyoユーザーがない
| 603 |参加者情報を設定失敗
| 604 |入室失敗
| 605 |参加者がVidyo部屋にいない
| 606 |オーディオミュート失敗
| 607 |オーディオアンミュート失敗
| 608 |ビデオミュート失敗
| 609 |ビデオアンミュート失敗
| 609 |ビデオアンミュート失敗
| 901 |予約開始時間前
| 902 |画面共有モード無効

##action\_GenerateAuthToken
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/
####このAPI は非推奨ですが、互換性のために残しておいてください。


Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_GenerateAuthToken  | Method  |  - | - | -
◯  | volatileToken   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6

volatileToken : サーバ側から受け取った揮発性のTOKEN文字列をハッシュ化したもの
※サーバ側からはハッシュ前の値をうけとっていますが、10回ハッシュ化した文字列をサーバで保持しています
ので、10回ハッシュ化してサーバへ送ります。

##action\_StartConferenceSession
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_StartConferenceSession  | Method  |  - | - | -
◯  | volatileToken   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | type | type | Int | - | 101
◯  | version | version | string(255) | - | 5.0.0.0

volatileToken : サーバ側から受け取った揮発性のTOKEN文字列をハッシュ化したもの
※サーバ側からはハッシュ前の値をうけとっていますが、10回ハッシュ化した文字列をサーバで保持しています
ので、10回ハッシュ化してサーバへ送ります。

type | 対応プラットフォーム
------------- | ------------- |
101|Windows V5 Lite
201|Mac V5 Lite
301|iOS V5 Lite
401|Android V5 Lite
501|Linux V5 Lite(予約)

#####レスポンス
~~~
{
	"displayName":"" or "DISPLAY_NAME",
	"requireVersion":"REQUIRED_VERSION",
	"applicationName":"Application Name",
	"result" : "OK" or "ERROR",
	"token": "VALID_TOKEN",
	"permission": :[
                        {
                            "conferenceOperation" : "0xff",
                            "quickEnquete" : "0xff",
                            "sharing" : "0xff",
                            "deviceOperation" : "0xff",
                            "cameraDevice" : "0xff",
                            "microphoneDevice" : "0xff",
                            "speakerDevice" : "0xff"
                        }
                    ],
　	"logoUrl" : {
　			type."ID_OF_IMAGE1" : "https://dev-v5-lite-web01.vcube.net/custom/vuzer01/osx/Logo_login.png",
　			type."ID_OF_IMAGE2" : "https://dev-v5-lite-web01.vcube.net/custom/vuzer01/osx/ButtonMenu_up.png",...
　		}
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　		}  I
}
~~~

Logoの識別はtype番号(上記表参照).ユニークなID(UUID)で発行
ロゴの変更がない場合は、"logoUrl" : "" // 空で渡します。
permissionの渡される値の詳細はDraft/permission/permission.mdを参照


Windows ID | 画像タイプ | ファイル名
------------- | ------------- | ------------- |
101.1C56AD8F-EBA9-41CF-AA91-14239535CDD3 | Logo_login | Logo_login.png
101.8114E76C-BAD4-423F-A588-5FA60820DB8E | ButtonMenu_up | ButtonMenu_up.png
101.430A1E91-7D3C-4BD7-B2E5-C20256F3FF65 | ButtonMenu_over | ButtonMenu_over.png
101.0CDF9498-E6FF-4633-9B0F-2BA0D0C85192 | ButtonMenu_down | ButtonMenu_down.png
101.7D59BB48-053C-4D1E-AB50-8001C6C0411D | ButtonMenu_disable | ButtonMenu_disable.png

OSX ID | 画像タイプ | ファイル名
------------- | ------------- | ------------- |
201.1C56AD8F-EBA9-41CF-AA91-14239535CDD3 | Logo_login | Logo_login.png
201.8114E76C-BAD4-423F-A588-5FA60820DB8E | ButtonMenu_up | ButtonMenu_up.png
201.430A1E91-7D3C-4BD7-B2E5-C20256F3FF65 | ButtonMenu_over | ButtonMenu_over.png
201.0CDF9498-E6FF-4633-9B0F-2BA0D0C85192 | ButtonMenu_down | ButtonMenu_down.png
201.7D59BB48-053C-4D1E-AB50-8001C6C0411D | ButtonMenu_disable | ButtonMenu_disable.png

iOS ID | 画像タイプ | ファイル名
------------- | ------------- | ------------- |
301.1C56AD8F-EBA9-41CF-AA91-14239535CDD3 | Icon_loginLogo | Icon_loginLogo.pn
301.EB8ED16B-F068-4EE8-AA78-ED8CA36AF9A6 | Icon_loginLogo 2x | Icon_loginLogo_2x.png
301.E91767A9-4D73-4C18-9C0E-7A44A49EA77A | Icon_loginLogo 3x | Icon_loginLogo_3x.png
301.8114E76C-BAD4-423F-A588-5FA60820DB8E | Button_drawerWithLogoOnNavbar | Button_drawerWithLogoOnNavbar.png
301.67CE87E3-719A-44F3-8875-DB5EE4E4256D | Button_drawerWithLogoOnNavbar　2x | Button_drawerWithLogoOnNavbar_2x.png
301.C21AC6D6-62F9-4A3A-B919-DC9BCA787106 | Button_drawerWithLogoOnNavbar　3x | Button_drawerWithLogoOnNavbar_3x.png
301.33732871-AF16-4145-9D81-CE5647B22878 | SplashLogo | SplashLogo.png
301.E37CC1C2-17FC-47EB-A6DA-A1DA85289A12 | SplashLogo　2x | SplashLogo_2x.png
301.3B8771C2-C9A8-4A0E-B88D-1CCA1B766589 | SplashLogo　3x | SplashLogo_3x.png
301.F9765EE2-6558-4B27-935B-55EC9C4F0FA2 | Watermark | Watermark_.png
301.75AD0FDD-CBFD-4AF7-B4A5-FA513068ABE0 | Watermark　2x | Watermark_2x.png
301.E5F68238-E918-4C32-9636-616045402C89 | Watermark　3x | Watermark_3x.png

※旧バージョンのアプリでクラッシュしないか各プラットフォームに確認

SOME\_ERROR\_CODE :
1.ポータル内部エラー
2.無効なvolatileToken
3.バージョンが要求バージョンを満たしていない
のいずれかのエラーを返す



##action\_PrepareJoinConference
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/


Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_PrepareJoinConference  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | password   | password   |  string(255) | - | b89eaac7e61417341b710b727768294d0e6a277b
◯  | displayName   | Participant's name   |  string(255) | - | myName


token :        サーバ側から受け取ったセッション用のTOKEN文字列
password : SHA1 でハッシュ化したパスワード文字列
displayName : 表示用のユーザ名

1.  名前入力が必須であり、displayNameが空文字であった場合、名前入力が必須である旨を返す
2.  パスワードが必要かチェックし、パスワードが渡されてない場合はパスワードが必要である旨を返す
3.  パスワードが必要かチェックし、パスワードが間違っている場合はパスワードが違う事を返す
4.  パスワードが不必要、または、パスワードがあっている場合は部屋とユーザをPoolから選択し、ConferenceサーバのPrepareParticipant API に 該当するユーザのTokenをPOSTする
5.  ３が成功した場合、JSON形式で VidyoのUN,PW,Portal_Address,Confrence_adressを返す。

#####レスポンス
~~~
{
	"conferencePassword":"REQUIRED" or "INVALID" or "VALID",
	"requireDisplayName":"REQUIRED" or "INVALID" or "VALID",
   "displayName":"NAME_OF_PARTICIPANT",
　	"un"  :  "VIDYO_USER_UN",
　	"pw"  :  "VIDYO_USER_PW",
　	"portalAddress" : "https://fqdn:port/",
　	"conferenceAddress" : "wss://fqdn:port/",
　	"conferenceID" : "conferenceID",
　	"locationTag" : "tyo", //
　	"locationTagList" :[
	　　　　　　　　　　　　　　　　　　"tyo",
	　　　　　　　　　　　　　　　　　　"sig",
	　　　　　　　　　　　　　　　　　　"sao"...
	　　　　　　　　　　　　　　　　　　]
}
	"isGuest" : "true" or "false",
	"result" : "OK" or "ERROR",

　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　		}
　}
~~~
#####エラー
SOME\_ERROR\_CODE : ポータル内部エラー、Conferenceサーバがエラー、パラメーターエラー

##action\_UpdateLocationTag
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/


Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_UpdateLocationTag  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | locationTag   | locationTag   |  string(255) | - | tyo
◯  | displayName   | Participant's name   |  string(255) | - | myName


token :        サーバ側から受け取ったセッション用のTOKEN文字列
locationTag : action\_PrepareJoinConferenceで受け取ったlocationTagListより選択

#####レスポンス
~~~
{
    "result" : "OK" or "ERROR",
　   "detail" : {
　           "status" : "200" or SOME_ERROR_CODE,
　           "description" : SOME_MESSAGE_FROM_SERVER
　   }
}
~~~
#####エラー
SOME\_ERROR\_CODE : ポータル内部エラー、Conferenceサーバがエラー、パラメーターエラー

##action\_JoinConference
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_JoinConference  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6


入室を開始します。

#####レスポンス
~~~
{
	"result" : "OK" or "ERROR",
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~
#####エラー
SOME_ERROR_CODE : ポータル内部エラー、Vidyoサーバがエラー、パラメーターエラー

##action\_SendInvitationMail
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_SendInvitationMail  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | conferenceID   | ConferenceID   |  unfixed | - |
◯  | mailAddress   | Mail address   |  string(255) | - | hogehoge@vcube.co.jp


招待メールを送信します。

#####レスポンス
~~~
{
	"result" : "OK" or "ERROR",
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~
#####エラー
SOME_ERROR_CODE : パラメーターエラー

##action\_GetQuickEnqueteList
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_GetQuickEnqueteList  | Method  |  - | - | -
◯  | token   | Token   |  string | - | euiu03pja35lv9dsgrrsbkoul6
- | ownership   | 所有権フィルタ   |  int | 0 | 0: 共有+個人、1: 個人のみ、2: 共有のみ
- | title   | タイトル検索   |  string | - | 文字列検索
- | quickEnqueteQuestionType   | アンケート識別タイプ   |  int | - | 識別タイプ検索
- | limit   | 検索データ取得行数   |  int | 10 | 20（空の場合はデフォルト。0を指定した場合は全件取得）
- | page   | ページ番号   |  int | 1 | 2 (表示するページ番号)
- | sort_key   | データソート順   |  string | title | title: タイトルでソート / createtime: 作成順
- | sort_type   | データソート順   |  string | asc | asc: 昇順 / desc: 降順


tokenから紐づくユーザーに登録されたアンケート一覧を渡す

#####レスポンス
~~~
{
	"result" : "OK" or "ERROR",
   "quickEnqueteListCount" : 件数,
   "quickEnqueteList" :[
							{
								"quickEnqueteID" : "quickEnqueteID",
								"title" : "Quick Enquete Title",
								"quickEnqueteQuestion": "Quick Enquete question",*
								"quickEnqueteQuestionType" : quickEnqueteType,*
								"ownership" : 所有権,
							},
							{
								"quickEnqueteID" : "quickEnqueteID",
								"title" : "Quick Enquete Title",
								"quickEnqueteQuestion": "Quick Enquete question",*
								"quickEnqueteQuestionType" : quickEnqueteType,*
								"ownership" : 所有権,
							},...
						],
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~
#####エラー
SOME_ERROR_CODE : パラメーターエラー

*に関しては、削る可能性あり
quickEnqueteQuestionType:
1:yes/no、2:番号選択（radio）、3:番号＋テキスト（radio）、4:番号選択（checkbox）、5:番号＋テキスト（checkbox）、5:テキスト（入力式）
※重説では1〜3

##action\_CalloutLegacyDevice
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_CalloutLegacyDevice  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | conferenceID   | ConferenceID   |  unfixed | - |
◯  | type   | type   |  string(255) | - | sip or h323
-  | id   | type   |  string(255) | - | SIP接続用内線番号
◯  | address   | Legacy address   |  string(255) | - | IPアドレス

SIP / H.323端末のコールアウトを実施。

#####レスポンス
~~~
{
	"result" : "OK" or "ERROR",
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~
#####エラー
SOME_ERROR_CODE : パラメーターエラー


##action\_GetQuickEnqueteDetail
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_GetQuickEnquete  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | quickEnqueteID   | quickEnqueteID   |  unfixed | - |


IDで指定されたクイックアンケートの詳細を渡す

#####レスポンス
~~~
{
	"result" : "OK" or "ERROR",
   "quickEnqueteID" : "QuickEnqueteID",
	"title": "Quick Enquete Title",
	"quickEnqueteQuestion": "Question",
   "quickEnqueteQuestionType": QuestionType,
   "ownership" : 所有権,
   "quickEnqueteAttributeCount" : 選択肢数
　	"quickEnqueteAttributeList" :[
                            {
                                "quickEnqueteAttributeID" : "Quick Enquete Attribute ID",
                                "quickEnqueteAttributeLabel": "Label",
                                "quickEnqueteAttributeSort": Sort Number,
                            },
                            {
                                "quickEnqueteAttributeID" : "Quick Enquete Attribute ID",
                                "quickEnqueteAttributeSort": Sort Number,
                            },...
                        ],
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~
quickEnqueteAttributeListがない場合は、空配列

quickEnqueteQuestionType:
1:yes/no、2:番号選択（radio）、3:番号＋テキスト（radio）、4:番号選択（checkbox）、5:番号＋テキスト（checkbox）、6:テキスト（入力式）
※重説では1〜3


#####エラー
SOME_ERROR_CODE : パラメーターエラー

##action\_createQuickEnquete
###URL : http://dev-v5lite.vcube.net/api/v5lite/client/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_GetQuickEnquete  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | quickEnqueteQuestion   | Question   |  string | - | -
◯  | quickEnqueteQuestionType  | Question Type  |  int | - | -
◯  | quickEnqueteAttributeList   | 設問   | Json | - | -
◯  | save_flg   | アンケートの保存フラグ   | int | 0 | 0: 保存しない、1: 保存する
-  | title   | Title   |  text | - | -

クイックアンケートの情報を保存するためのAPI

~~~
{
	"token" : "token",
	"title": "title",
	"quickEnqueteQuestion": "Question",
   "quickEnqueteQuestionType": "QuickEnquete Type",
   "quickEnqueteAttributeCount": "Attribute count", // 設問数（番号選択用）
	"quickEnqueteAttributeList" :[
	　　　　　　　　　　　　　　　　　　"quickEnqueteAttributeLabel-1",
	　　　　　　　　　　　　　　　　　　"quickEnqueteAttributeLabel-2"
	　　　　　　　　　　　　　　　　　　]
}
~~~

quickEnqueteQuestionType:
1:yes/no、2:番号選択（radio）、3:番号＋テキスト（radio）、4:番号選択（checkbox）、5:番号＋テキスト（checkbox）、6:テキスト（入力式）
※重説では1〜3

#####レスポンス
~~~
{
   "result" : "OK" or "ERROR",
   "quickEnqueteID" : "QuickEnqueteID",
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~
#####エラー
SOME_ERROR_CODE : パラメーターエラー


#conference


##action\_Register
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_Register  | Method  |  - | - | -
◯  | serverAddress   | FQDN or IP   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6

カンファレンスサーバの初回起動時にコールされる
該当するカンファレンスが開催中ならば全て初期化する

##action\_NoticeJoinedConference
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_NoticeJoinedConference  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | clientPid   | Client PID   |  string(255) | - | hoifaheofhao


#####レスポンス

最新のParticipantList JSON OBJECT
Participant List Object の構造は下記の通りです

~~~
{
	"result" : "OK" or "ERROR",
	"numParticipants" : NUMBER_PARTICIPANTS,
	"conferenceID" : ID_OF_COMFERENCE,
	"participants" :[
							{
								"display_name" : NAME_OF_PARTICIPANT,
								"participant_id" : PID_FROM_CLIENT
							},
							{
								"display_name" : NAME_OF_PARTICIPANT,
								"participant_id" : PID_FROM_CLIENT
							},...
						],
	"detail" : {
　					"status" : "200" or SOME_ERROR_CODE,
　					"description" : SOME_MESSAGE_FROM_SERVER
　				}
}
~~~



#####エラー
SOME\_ERROR\_CODE : ポータル内部エラー、Vidyoサーバがエラー、Confrenceサーバのエラー、パラメーターエラー


##action\_UpdateParticipants
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_UpdateParticipants  | Method  |  - | - | -
◯  | token   | Token   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | code   | code   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6



key  | value  |
------------- | ------------- |
action_UpdateParticipants | -  |
type | "add" or "remove" or "lost" or "reconnect"のいずれか<br> Participantの追加<br>削除<br>暫定退出<br>再入室を表す文字列
conferenceID | 対象のConferenceID |
participantList   |  最新のParticipantListを表すJSON文字列  |
token   | 対象となるParticipantを特定するtoken文字列 |
participant_id   | 対象となるParticipantを特定するClientから渡されるPID |
timestamp | 更新された時間 Unixtime （秒）
code		| typeに関連するコード
revision   | 該当する会議内のparticipantListの更新回数 (64bit uint) |


participantList Json文字列は下記の通り

~~~
[
	{
		"display_name" : NAME_OF_PARTICIPANT,
		"participant_id" : PID_FROM_CLIENT
	},
	{
		"display_name" : NAME_OF_PARTICIPANT,
		"participant_id" : PID_FROM_CLIENT                    ]
	},...
]
~~~

code はremoveの場合のみ有効なパラメータで退出理由でのみ利用されそれ以外は一律200
とみなされます。一覧は下記の通りです。


ステータス | code  | 説明
-------|------ | -------------
200 ~ 299 <br> 成功応答| 200  | 正常に切断。RemoveFromConferenceに対する正常な応答
300 ~ 399 <br> 転送応答| 302  | ( Reserved ) <br> カンファレンスサーバの移動が必要になった為切断
400 ~ 499 <br> リクエストエラー| 401  | connect / reconnect 接続に対しtokenが無効
　| 403  | ( Reserved ) 該当するカンファレンスが入室拒否状態
　| 404  | connect / reconnect 接続に対し該当するカンファレンスが存在しない
　| 408  | Connectionが切断されタイムアウトした
　| 410  | Pingに対するクライアントからの応答が一定回数確認出来なかった
　| 412  | PrepareParticipant後、接続が確立されなかった
　| 486  | 該当するカンファレンスが満員
500 ~ 599 <br> サーバエラー| 500  | サーバ内部で何らかのエラーが発生し会議の継続ができない
　| 504  | Portalのaction_NoticeJoinedConferenceがエラーコードを返答した
600 ~ 699 <br> グローバルクローズ| 600  | 予約会議時間を超えた為、会議が終了した
　| 603  | 強制的に会議全体が終了させられた<br>※ConferenceサーバのCloseConferenceを呼び出した
　| 604  | システム側から退出させられた
　
※ConferenceサーバのCloseConferenceを呼び出した際にはこのコールバックは呼びだされませんので
内部的に603をセットします。
非同期処理で、Vidyoサーバに対してLogoutをコールする

removeの場合に、参加者の利用時間として、meeting_uptimeから2人以上になった会議時間を取得し、分単位で、participant.use_count に登録する。

#####レスポンス
最新のParticipantList JSON OBJECT
Participant List Object の構造は下記の通りです

~~~
{
	"result" : "OK" or "ERROR",
	"participants" :[
							{
								"participant_id" : PID_FROM_CLIENT,
								"permission":[
                                            {
                                                "conferenceOperation" : "0xff",
                                                "quickEnquete" : "0xff",
                                                "sharing" : "0xff",
                                                "deviceOperation" : "0xff",
                                                "cameraDevice" : "0xff",
                                                "microphoneDevice" : "0xff",
                                                "speakerDevice" : "0xff"
                                            }
                                        ]
							},
							{
								"participant_id" : PID_FROM_CLIENT,
								"permission":[
                                            {
                                                "conferenceOperation" : "0xff",
                                                "quickEnquete" : "0xff",
                                                "sharing" : "0xff",
                                                "deviceOperation" : "0xff",
                                                "cameraDevice" : "0xff",
                                                "microphoneDevice" : "0xff",
                                                "speakerDevice" : "0xff"
                                            }
                                        ]
							},...
						],
	"detail" : {
　					"status" : "200" or SOME_ERROR_CODE,
　					"description" : SOME_MESSAGE_FROM_SERVER
　				}
}
~~~
permissionには各入室者のタイプに併せてフラグ値を16進数文字列で表現したもの。

##action\_ChangeConferenceName
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_ChangeConferenceName  | Method  |  - | - | -
◯  | conferenceID   | ConferenceID   |  unfixed | - |
◯  | conferenceName   | conferenceName   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6

現在開催中の会議名を変更します。



##action\_ConferenceClosed
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_ConferenceClosed  | Method  |  - | - | -
◯  | conferenceID   | ConferenceID   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | code   | code   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | totalTime   | totalTime   |  int | - | 10

会議室からユーザが全ていなくなった事を通知されます。
終了コードは下記のとおりです


code  | 説明
------------- | -------------
200  | 会議は正常に終了した
412  | PrepareConference後、誰の入室も無くタイムアウトで会議を終了
600  | 予約会議時間を超えた為、会議が終了した
603  | 強制的に会議全体が終了させられた<br>※ConferenceサーバのCloseConferenceを呼び出した
※ 412 600 603 の場合、ConferenceサーバはRemoveParticipantを呼び出しませんので
内部的に参加中のParticipantを退出状態にしてください。

#####レスポンス
~~~
{
	"result" : "OK" or "ERROR",
	"conferenceID" : ID_OF_COMFERENCE,
	"detail" : {
　					"status" : "200" or SOME_ERROR_CODE,
　					"description" : SOME_MESSAGE_FROM_SERVER
　				}
}
~~~

##action\_ sendChatData
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

key名  | 概要
------------- | -------------
conferenceID  | 会議を特定する一意のID
chatData      | チャット履歴のJSONオブジェクト

###引数
**chatData** : チャット履歴のJSONオブジェクト。
構造体は下記のとおりです

~~~
{
	"chatData" :[
                    {
                        "display_name" : NAME_OF_PARTICIPANT,
                        "token" : VALID_TOKEN,
                        "message" : "MESSAGE",
                        "sendTime" : "グリニッジ標準時のUNIX Time"
                    },
                    {
                        "display_name" : NAME_OF_PARTICIPANT,
                        "token" : VALID_TOKEN,
                        "message" : "MESSAGE",
                        "sendTime" : "グリニッジ標準時のUNIX Time"
                    },...
                ]
}
~~~
※ チャットの利用がない場合は、実行なし

##action\_ sendQuickEnqueteAnswer
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_ sendQuickEnqueteAnswer  | Method  |  - | - | -
◯  | conferenceID   | ConferenceID   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | quickEnqueteID   | QuickEnqueteID   |  string(255) | - | juidu0f3dpfa35lv9dsgrrsbkoul9
◯  | QuickEnqueteAnswerData      | アンケート結果のJSONオブジェクト


###引数
**chatData** : アンケート結果のJSONオブジェクト。
構造体は下記のとおりです

~~~
{
	"QuickEnqueteAnswerData" :[
                    {
                        "display_name" : NAME_OF_PARTICIPANT,
                        "token" : VALID_TOKEN,
                        "quickEnqueteAttributeID" : "MESSAGE",
                        "quickEnqueteAnswer" : "yes/no or number",
                        "input_data" : "入力回答データ",
                        "answerDatetime" : "グリニッジ標準時のUNIX Time"
                    },
                    {
                        "display_name" : NAME_OF_PARTICIPANT,
                        "token" : VALID_TOKEN,
                        "quickEnqueteAttributeID" : "MESSAGE",
                        "quickEnqueteAnswer" : "yes/no or number",
                        "input_data" : "入力回答データ",
                        "answerDatetime" : "グリニッジ標準時のUNIX Time"
                    },...
                ]
}
~~~
※ クイックアンケート終了時に実行

key名  | 概要
------------- | -------------
conferenceID  | 会議を特定する一意のID
quickEnqueteID  | アンケートを識別する一意のID
QuickEnqueteData | アンケート結果のJSONオブジェクト

#####レスポンス
~~~
{
	"result" : "OK" or "ERROR",
	"detail" : {
　					"status" : "200" or SOME_ERROR_CODE,
　					"description" : SOME_MESSAGE_FROM_SERVER
　				}
}
~~~

##action\_StartRecord
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  |　key名  | 概要
------------- | ------------- | -------------
◯ | conferenceID  | 会議を特定する一意のID
◯ | tag  | カンファレンスサーバーに発行されたRecID
◯ | token  | 参加者のtoken

###引数
**conferenceID** : 録画されたい会議ID   
**tag** : カンファレンスサーバーに発行されたRecID   
**token** : 開始ボタン押した参加者の情報も一応Portalに保存する   

###レスポンス
~~~
{
	"result" : "OK" or "ERROR",
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~

エラーコード

code  | 説明
----- | -------------
401   | 会議IDに対する会議はない
402   | 該当会議を終了した
803   | 録画をしている
804   | ポータル側からvideo manager serveに通信できない
805   | タグをマッチング失敗
806   | 録画開始失敗
807   | 録画開始処理実行中
800   | サーバーエラー

##action\_CancelRecord
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  |　key名  | 概要
------------- | ------------- | -------------
◯ | conferenceID  | 会議を特定する一意のID
◯ | tag  | カンファレンスサーバーに発行されたRecID
◯ | token  | 参加者のtoken

###引数
**conferenceID** : 録画開始中会議ID   
**tag** : カンファレンスサーバーに発行されたRecID   
**token** : キャンセルボタン押した参加者の情報も一応Portalに保存する   

###レスポンス
~~~
{
	"result" : "OK" or "ERROR",
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~
エラーコード

code  | 説明
----- | -------------
401   | 会議IDに対する会議はない
402   | 該当会議を終了した
802   | 録画を開始させてない
803   | 録画もう開始しました
805   | タグをマッチング失敗
806   | 録画開始失敗
807   | 録画開始処理実行中
800   | サーバーエラー

##action\_StopRecord
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  |　key名  | 概要
------------- | ------------- | -------------
◯ | conferenceID  | 会議を特定する一意のID
◯ | tag  | カンファレンスサーバーに発行されたRecID
◯ | token  | 参加者のtoken

###引数
**conferenceID** : 録画開始された会議ID   
**tag** : カンファレンスサーバーに発行されたRecID   
**token** : 停止ボタン押した参加者の情報も一応Portalに保存する   

###レスポンス
~~~
{
	"result" : "OK" or "ERROR",
　	"detail" : {
　			"status" : "200" or SOME_ERROR_CODE,
　			"description" : SOME_MESSAGE_FROM_SERVER
　	}
}
~~~

エラーコード

code  | 説明
----- | -------------
401   | 会議IDに対する会議はない
402   | 該当会議を終了した
802   | 録画を開始させてない
804   | ポータル側からvideo manager serveに通信できない
805   | タグをマッチング失敗
800   | サーバーエラー

##action\_RemoveParticipant
###URL : http://dev-v5lite.vcube.net/api/v5lite/conference/

Required  | ID  | Name  | Type  | Default  | Example
------------- | ------------- | ------------- | ------------- | ------------- | -------------
◯  | action\_RemoveParticipant  | Method  |  - | - | -
◯  | conferenceID   | ConferenceID   |  string(255) | - | euiu03pja35lv9dsgrrsbkoul6
◯  | participant_id   | participant_id   |  string(255) | - | 強制退室させる参加者ID

会議室からユーザが全ていなくなった事を通知されます。
終了コードは下記のとおりです

#####レスポンス
~~~
{
	"result" : "OK" or "ERROR",
	"conferenceID" : ID_OF_COMFERENCE,
	"detail" : {
　					"status" : "200" or SOME_ERROR_CODE,
　					"description" : SOME_MESSAGE_FROM_SERVER
　				}
}
~~~

code  | 説明
------------- | -------------
200  | 参加者の退室処理が終了した

##下記のAPIは利用されなくなりました

#####URL : http://dev-v5lite.vcube.net/api/v5lite/conference/
####action\_NoticeJoinedConference
####action\_PrepareRemove
####action\_ReconnectParticipant
