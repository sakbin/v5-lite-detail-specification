#Conference Server API

###本ドキュメントでは、録画機能に必要となる追加APIについて説明する

##録画の状態遷移について
サーバは、現在の録画の状態を **IDLE** ,**PREPARE**, **REC** の３つで管理し、その現在の状態とクライアント、カンファレンスからのメッセージを組み合わせて状態を変化させていく

状態名  | 概要 | 遷移可能な状態 |
------------- | --------|-----
IDLE  | 録画を行っていない状態 | PREPARE
PREPARE | 録画要求をPortalに送信し、その応答を待っている状態 | IDLE or REC
REC  | 現在サーバは録画状態にある | IDLE


##1. HTTP  POST で呼ばれるAPI
###2-1. URL : /api/ResultRecordStatus

key名  | 概要
------------- | -------------
conferenceID  | 会議を特定する一意のID
recordID | 録画開始要求時に送信したカンファレンスが生成した一意のID
result  | 録画要求に対する結果を示すJSONオブジェクト

###引数
**result** : 録画要求に対する結果を示すJSONオブジェクトです。
構造体は下記のとおり
{
	“result” : “OK" or "FAILD" or "CANCELED"
	“reason” : resultに対する理由コードです
}

Value of Reason  | 概要
------------- | -------------
200 | 録画成功 かならず result = OK と一緒に渡される
204 | 録画要求がキャンセルされた 必ず result = CANCELEDと一緒に渡される
408 | タイムアウト
500 | V5Liteサーバが処理を正常に処理できなかった
502 | Vidyoサーバが処理を正常に処理できなかった
503 | 録画サーバがBusy状態で録画を開始できなかった
507 | 容量不足により録画を開始することが出来なかった

サーバはこのメッセージを受け取ったあと、該当するParticpant全て（後述）に対し
ResultRecordStatus イベントを通知します



##2. WebSocket API
###WebSocketAPIの構造体

WebSocket APIは基本的に下記の構造体のJSONオブジェクトを  
ConferenceServerに送ることで実現します。

```markup
{
	“event” : EVENT_ID,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : DATA
}
```
###ACK / NACKメッセージ
Conferenceに対して操作要求が送られてきた際に、
Confrenceサーバはその操作要求が正しく処理できるかどうかを ACK ( ACKnowledgement : 確認応答 ) もしくは NACK ( Negative ACKnowledgement：否定応答 ) の形で返答します
この ACK / NACK は 今回の機能拡張から新しく導入される概念となります
#####※ Established の メッセージ構造体（ Sever > Client )
~~~
{
	“event” : “ACK” or "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
					"squence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
					"recievedEvent" : パーティシパント送ってきた操作要求の名称
					"foo": recievedEventに対応された拡張フィールドで
					"bar": サーバは対応したデータを付与して送ることができます
				}
}
~~~

###EVENT : RequestStartRecord
会議に対する録画開始の要求メッセージです
#####RequestStartRecord の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RequestStartRecord”,
	“from” : TARGET_PARTICIPANT_ID
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : { "squence" : パーティシパントが生成する一意なシーケンス番号 }
}
~~~
カンファレンスサーバはメッセージを受け取とった後、状態をPREPAREへ遷移させ
１．現在録画の状態がIDLEであれば新しい録画の状態を示すユニークなID( recordID )を生成し、
送信元に対してACKを返します  
２．現在の録画状態がPREPAREであれば、現在のrecordIDを送信元にACKメッセージとして返します
#####RequestStartRecord に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
					"squence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
					"recievedEvent" : "RequestStartRecord"
					"recordID": 現在最新の録画状態のID
				}
}
~~~

３．現在の録画状態がRECの場合は無効な処理としてNACKを返します
#####RequestStartRecord に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
					"squence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
					"recievedEvent" : "RequestStartRecord"
				}
}
~~~

この後、Portalに対して録画開始要求のHTTPリクエストを送信します（後述）

###EVENT : StopRecord
会議中の録画の停止を行います
#####StopRecord の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “StopRecord”,
	“from” : TARGET_PARTICIPANT_ID
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {  }
}
~~~

カンファレンスサーバは現在のステータスがRECの場合、ステータスをIDLEに変更し
Portalに録画停止要求を投げ（後述） ConferenceStatusのisRecordをfalseに変更し
参加者全員に録画状態の変更を通知します


###EVENT : CancelRecord
会議中の録画要求の取り消しを行います

#####CancelRecord の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “CancelRecord”,
	“from” : "system"
	“dest” : "TARGET_PARTICIPANT_ID",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : { "squence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,	
				 "recordID": 現在最新の録画状態のID
				}
}
~~~
カンファレンスサーバはメッセージを受け取とった後、  
１．現在の recordID の値が正しく、PREPARE状態の場合、RequestStartRecord のACKを投げたParticipant全てに対してResultRecordStatus イベントを通知します
2. recordIDが不正か現在の状態がPREPAREではない場合、NACKを返します

---
###EVENT : ResultRecordStatus
録画開始要求のACKを受け取ったParticipantに対して、最終的な録画状況の結果を通知します
#####ResultRecordStatus の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “ResultRecordStatus”,
	“from” : "system"
	“dest” : "TARGET_PARTICIPANT_ID",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : { 
				 "recordID": 現在最新の録画状態のID,
				 "reason" : “OK" or "FAILD" or "CANCELED"
				 reason” : resultに対する理由コードです
				}
}
~~~
理由コード一覧

Value of Reason  | 概要
------------- | -------------
200 | 録画成功 かならず result = OK と一緒に渡される
204 | 録画要求がキャンセルされた 必ず result = CANCELEDと一緒に渡される
408 | タイムアウト
500 | V5Liteサーバが処理を正常に処理できなかった
502 | Vidyoサーバが処理を正常に処理できなかった
503 | 録画サーバがBusy状態で録画を開始できなかった
507 | 容量不足により録画を開始することが出来なかった


##3. 録画の開始・停止・キャンセル
Conferenceサーバは、パーティシパントからの録画状態の遷移リクエストに応じてPortalにリクエストを送信する必要があります

###3-1.接続時パラメータ
クライアントがサーバに接続するときのURLは下記の方式となります
###url :: wss://CONF\_SERVER\_FQDN:PORT/CONFERENCE_ID/

###3-2. 録画の開始
###URL : https://SERVER_FQDN/api/v5lite/conference/?action\_StartRecord
#####パラメータ、戻り値など詳細はWEBAPIを参照

###3-3. 録画の停止
###URL : https://SERVER_FQDN/api/v5lite/conference/?action\_StopRecord
#####パラメータ、戻り値など詳細はWEBAPIを参照

###3-3. 録画のキャンセル
###URL : https://SERVER_FQDN/api/v5lite/conference/?action\_CancelRecord
#####パラメータ、戻り値など詳細はWEBAPIを参照

