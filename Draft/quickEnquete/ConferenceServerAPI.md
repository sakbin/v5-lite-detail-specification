#Conference Server API

###本ドキュメントでは、クイックアンケート機能に必要となる追加APIについて説明する

##クイックアンケートの状態遷移について
サーバは、現在のアンケートの状態を **WAIT** ,**WATCH**, **RESULT** の３つで管理し、その現在の状態とクライアント、カンファレンスからのメッセージを組み合わせて状態を変化させていく

状態名  | 概要 | 遷移可能な状態 |
------------- | --------|-----
WAIT | クイックアンケートを行っていない状態 | WATCH
WATCH | クイックアンケート集計中 | WAIT or RESULT
RESULT | クイックアンケート集計結果表示中 | WATCH


##1. HTTP  POST で呼ばれるAPI
####N/A


##2. WebSocket API
###WebSocketAPIの構造体

WebSocket APIは基本的に下記の構造体のJSONオブジェクトを  
ConferenceServerに送ることで実現します。

```markup
{
	“event” : EVENT_ID,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : DATA
}
```
###ACK / NACKメッセージ
Conferenceに対して操作要求が送られてきた際に、
Confrenceサーバはその操作要求が正しく処理できるかどうかを ACK ( ACKnowledgement : 確認応答 ) もしくは NACK ( Negative ACKnowledgement：否定応答 ) の形で返答します
この ACK / NACK は 今回の機能拡張から新しく導入される概念となります
#####※ Established の メッセージ構造体（ Sever > Client )
~~~
{
	“event” : “ACK” or "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : パーティシパント送ってきた操作要求の名称
		"foo": recievedEventに対応された拡張フィールドで
		"bar": サーバは対応したデータを付与して送ることができます
	}
}
~~~

###EVENT : RequestStartEnquete
クイックアンケート開始の要求メッセージです
#####RequestStartRecord の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RequestStartEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが生成する一意なシーケンス番号
		"quickEnqueteID" : quickEnqueteID,
		"title" : アンケートタイトル,
		"quickEnqueteQuestion" : 質問内容,
		"quickEnqueteQuestionType" : 回答種別
		"quickEnqueteAttributeCount" : 選択肢数
		"quickEnqueteAttributeList" :[
			{
				"quickEnqueteAttributeID" : "Quick Enquete Attribute ID",
				"quickEnqueteAttributeLabel": "Label",
				"quickEnqueteAttributeSort": Sort Number,
			},
			{
				"quickEnqueteAttributeID" : "Quick Enquete Attribute ID",
				"quickEnqueteAttributeLabel": "Label",
				"quickEnqueteAttributeSort": Sort Number,
			},...
		],
	}
}
~~~
※ quickEnqueteQuestionType: 1:yes/no、2:番号選択（radio）、3:番号＋テキスト（radio）、4:番号選択（checkbox）、5:番号＋テキスト（checkbox）、6:テキスト（入力式） ※重説では1〜3

カンファレンスサーバはメッセージを受け取とった後、 以下の処理を実施する。

1. 現在クイックアンケートの状態が**WAIT**であれば、**WATCH**に遷移させ、参加者全員にクイックアンケート質問内容（後述）を通知し、送信元に対してACKを返す。 

#####RequestStartEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "RequestStartEnquete"
	}
}
~~~

2. 現在のアンケート状態が**WATCH**、または**RESULT**であれば、送信元にNACKメッセージ返します

#####RequestStartEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "RequestStartEnquete"
	}
}
~~~

###EVENT : QuestionEnquete
クイックアンケート質問内容メッセージです
#####QuestionQuickEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “QuestionEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
		"title" : アンケートトタイトル,
		"quickEnqueteQuestion" : 質問内容,
		"quickEnqueteAttributeCount" : 選択肢数,
		"quickEnqueteQuestionType" : 回答種別,
		"quickEnqueteAttributeList" :[
			{
				"0": "quickEnqueteAttributeLabel-1",
			},
			{
				"1": "quickEnqueteAttributeLabel1-2",
			},...
		]
	}
}
~~~
質問者を除く参加者全員に通知します。

###EVENT : AnswerEnquete
クイックアンケート回答内容メッセージです
#####AnswerEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “AnswerEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID,
		"quickEnqueteAttributeID" : "" or 回答番号,
		"input_data" : "" or 回答データ
	}
}
~~~
QuestionEnqueteメッセージの回答種別によってquickEnqueteAttributeID、input_dataのどちらかに値を設定する。  
メッセージ受信に成功した場合、回答者にはACKを返却し、質問者にはAnswerEnqueteを通知する。  

#####AnswerEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "AnswerEnquete"
	}
}
~~~

#####AnswerEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “AnswerEnquete”,
	“from” : "system"
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
		"numAnswers" : 回答者母数,
		"aggregate" : [ 10,20,0,9,...... ] , // 0 ~ n番の解答結果集計情報
		"detail" : [
			{
				"participantID" : 回答者のPARTICIPANT_ID
				"quickEnqueteAttributeID" : "" or 回答番号
				"input_data" : "" or 回答データ
			},
			{
				"participantID" : 回答者のPARTICIPANT_ID
				"quickEnqueteAttributeID" : "" or 回答番号
				"input_data" : "" or 回答データ
			}, ...
		]
	}
}
~~~
※ "aggregate"には回答結果集計数が設定される。
※ 集計できないinput_dataの場合は空の配列を設定する。

また、閲覧者に通知する場合、detailには"null"を設定する。
#####AnswerEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “AnswerEnquete”,
	“from” : "system"
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
		"numAnswers" : 回答者母数,
		"aggregate" : [ 10,20,0,9,...... ] , // 0 ~ n番の解答結果集計情報
		"detail" : "null" // String型
	}
}
~~~

quickEnqueteIDの不一致、または回答種別に対する回答番号、または回答データが誤っていた場合、または質問者からアンケート終了通知受信後はNACKを返却する。

#####AnswerEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "AnswerEnquete"
	}
}
~~~

###EVENT : StopEnquete
クイックアンケート回答受付停止の要求メッセージです
#####StopEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “StopEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、回答者にはStopEnqueteを通知する。  

#####StopEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "StopEnquete"
	}
}
~~~

#####StopEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “StopEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~
当メッセージを受信後は、AnswerEnqueteメッセージを受け付けない。  
クイックアンケートの状態を**RESULT**に遷移させる。

quickEnqueteIDの不一致だった場合、NACKを返却する。

#####StopEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "StopEnquete"
	}
}
~~~

###EVENT : RestartEnquete
会議に対するクイックアンケート再開要求メッセージです
#####RestartEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RestartEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、回答者にはRestartEnqueteを通知する。  

#####RestartEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "RestartEnquete"
	}
}
~~~

#####RestartEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “RestartEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID
	}
}
~~~
当メッセージを受信後は、AnswerEnqueteメッセージを受け付ける。  
クイックアンケートの状態を**WATCH**に遷移させる。

quickEnqueteIDの不一致だった場合、NACKを返却する。

#####RestartEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "RestartEnquete"
	}
}
~~~

###EVENT : NoticeAnswerEnquete
クイックアンケートの集計結果を全員に通知するメッセージです
#####StopEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “NoticeAnswerEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"quickEnqueteID" : quickEnqueteID,
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、AnswerEnqueteを閲覧権限のある参加者に送信する。  

#####NoticeAnswerEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "NoticeAnswerEnquete"
	}
}
~~~
quickEnqueteIDの不一致だった場合、NACKを返却する。

#####NoticeAnswerEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "NoticeAnswerEnquete"
	}
}
~~~

###EVENT : ReleaseEnquete
会議に対するクイックアンケートの終了メッセージです
#####ReleaseEnquete の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “ReleaseEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
	}
}
~~~
メッセージ受信に成功した場合はACKを返却し、ReleaseEnqueteを回答権限、または閲覧権限のある参加者に送信する。  

#####ReleaseEnquete に対するACK メッセージ（ Server > Client )
~~~
{
	“event” : "ACK",
	“from” : “system”,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "ReleaseEnquete"
	}
}
~~~

#####ReleaseEnquete の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “ReleaseEnquete”,
	“from” : SENDER_PARTICIPANT_ID,
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"quickEnqueteID" : quickEnqueteID,
	}
}
~~~
現在のステータスを**WAIT**に遷移させ、Portalにクイックアンケート集計結果通知を投げる（後述）

quickEnqueteIDの不一致だった場合、NACKを返却する。

#####NoticeAnswerEnquete に対するNACK メッセージ（ Server > Client )
~~~
{
	“event” : "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
		"sequence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
		"recievedEvent" : "ReleaseEnquete"
	}
}
~~~

##3. クイックアンケート集計結果通知
Conferenceサーバは、パーティシパントからのクイックアンケート状態の遷移リクエストに応じてPortalにリクエストを送信する必要があります

###3-1.接続時パラメータ
クライアントがサーバに接続するときのURLは下記の方式となります
###url :: wss://CONF\_SERVER\_FQDN:PORT/CONFERENCE_ID/

###3-2. クイックアンケート集計結果通知
###URL : https://SERVER_FQDN/api/v5lite/conference/?action\_sendQuickEnqueteAnswer
#####パラメータ、戻り値など詳細はWEBAPIを参照
