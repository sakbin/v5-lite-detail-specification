#Conference Server API

###本ドキュメントでは、リモートポインティング機能に必要となる追加APIについて説明する
####用語説明
#####DesktopSharer ( デスクトップシェアラ / Sharer )
・会議参加者でデスクトップ画面を共有している拠点
#####PointSender ( ポイントセンダー / Sender )
・会議参加者でSharerに対して座標情報をおくる資格を有している拠点
#####バインディング
・会議内でSharerに対してSenderの組み合わせを行う処理。またはその組み合わせ。
　Senderからの座標情報はバインディングが成立しているSharerに対してのみ送信され、ペアリングが成立していない場合はその要求は無効なものとして処理される。
　Sharerに座標情報を送信可能なSenderは複数許容され、1:Nの形でバインドが可能。その逆で、Senderが複数のSharerとバインドすることは出来ない
##1. HTTP  GET で呼ばれるAPI
####N/A
##2. WebSocket API
###WebSocketAPIの構造体

WebSocket APIは基本的に下記の構造体のJSONオブジェクトを  
ConferenceServerに送ることで実現します。

```markup
{
	“event” : EVENT_ID,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : DATA
}
```
###ACK / NACKメッセージ
Conferenceに対して操作要求が送られてきた際に、
Confrenceサーバはその操作要求が正しく処理できるかどうかを ACK ( ACKnowledgement : 確認応答 ) もしくは NACK ( Negative ACKnowledgement：否定応答 ) の形で返答します
この ACK / NACK は 今回の機能拡張から新しく導入される概念となります
#####※ Established の メッセージ構造体（ Sever > Client )
~~~
{
	“event” : “ACK” or "NACK",
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
					"squence" : パーティシパントが送ってきた操作要求に含まれていた一意なシーケンス番号,
					"recievedEvent" : パーティシパント送ってきた操作要求の名称
					"foo": recievedEventに対応された拡張フィールドで
					"bar": サーバは対応したデータを付与して送ることができます
				}
}
~~~

###EVENT : StartDesktopSharing
あるパーティシパントがデスクトップ共有を開始した際、それをConferenceに通知します。
#####ParticipantJoin の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “StartDesktopSharing”,
	“from” : TARGET_PARTICIPANT_ID
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {}
}
~~~
カンファレンスサーバはメッセージを受け取とった後、リモートポインティングの送信可能な先として
リストに追加をします。

###EVENT : StopDesktopSharing
あるパーティシパントがデスクトップ共有を停止した際、それをConferenceに通知します。
#####StopDesktopSharing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “StartDesktopSharing”,
	“from” : TARGET_PARTICIPANT_ID
	“dest” : "system",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {}
}
~~~
#####StopDesktopSharing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “StartDesktopSharing”,
	“from” : "system"
	“dest” : "TARGET_PARTICIPANT_ID",
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {}
}
~~~
カンファレンスサーバはメッセージを受け取とった後、リモートポインティングの送信可能な先として
リストから排除します。
サーバからクライアントへのイベントの通知は
１．Sharerとバインディング状態のSenderがいた場合はそのSenderに対して送信
２．SharerがStopDesktopSharingを呼ばずにConnectionLostもしくは退出した場合バインディング状態のSenderに送信のいずれかのタイミングで送信されます

---
###EVENT : RequestRemotePointing
SenderからSharerに対してリモートポインティングのバインディング要求を出します
#####RequestRemotePointing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RequestRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHAREE_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {　
				"squence" : パーティシパントから送られる Int 型のシーケンス番号　
				}
}
~~~
サーバは SHAREE\_PARTICIPANT\_ID が Sharerとして有効な場合（RemoteDesktopShaingの開始を申告していた場合）はSenderに対してACKを送信し
#####RequestRemotePointing に対する ACK応答（ Server > Client )
~~~
{
	“event” : “ACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
					"squence" : パーティシパントから送られる Int 型のシーケンス番号,
					"recievedEvent" : "RequestRemotePointing"
				}
}
~~~

 SHAREE\_PARTICIPANT\_ID が Sharerとして無効な場合はNACKを返します
#####RequestRemotePointing に対する NACK応答（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
					"squence" : パーティシパントから送られる Int 型のシーケンス番号,
					"recievedEvent" : "RequestRemotePointing"
				}
}
~~~

Senderの要求通りのバインディングが成立した場合、SharerにもRequestRemotePointingメッセージを送信します

#####RequestRemotePointing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “RequestRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHAREE_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {　
				"squence" : パーティシパントから送られる Int 型のシーケンス番号　
			}
}
~~~

---

###EVENT : RemotePointing
SenderからSharerに対してリモートポインティングの座標情報を送ります
#####RequestRemotePointing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “RemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHARER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {　
				"squence" : パーティシパントから送られる Int 型のシーケンス番号,
				"width" : Senderの画面解像度を示すInt型の値,
				"height" : Senderの画面解像度を示すInt型の値,
				"x" : 座標X
				"y" : 座標Y 
				}
}
~~~
サーバは SHARER\_PARTICIPANT\_ID が Senderのバインディング対象では無い場合、無効な要求とみなしNACK応答を返します
#####RequestRemotePointing に対する NACK応答（ Server > Client )
~~~
{
	“event” : “NACK”,
	“from” : “system”
	“dest” : TARGET_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {
					"squence" : パーティシパントから送られる Int 型のシーケンス番号,
					"recievedEvent" : "RemotePointing"
				}
}
~~~

Senderに対する有効なバインド先であった場合、Sharerに対してRequestRemotePointingメッセージを送信します

#####RequestRemotePointing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “RequestRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHARER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {　
				"squence" : パーティシパントから送られる Int 型のシーケンス番号,
				"width" : Senderの画面解像度を示すInt型の値,
				"height" : Senderの画面解像度を示すInt型の値,
				"x" : 座標X
				"y" : 座標Y　
			}
}
~~~

---

###EVENT : ReleaseRemotePointing
Sharerに対してリモートポインティングの解除を申請します
#####ReleaseRemotePointing の メッセージ構造体（ Client > Server )
~~~
{
	“event” : “ReleaseRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHARER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {}
}
~~~

サーバは Senderに対する有効なバインド先であった場合のみSharerに対してReleaseRemotePointingメッセージを送り、Sharer Sender 間のバインドを解除します。

#####ReleaseRemotePointing の メッセージ構造体（ Server > Client )
~~~
{
	“event” : “ReleaseRemotePointing”,
	“from” : SENDER_PARTICIPANT_ID
	“dest” : SHARER_PARTICIPANT_ID,
	“conference” : TARGET_CONFERENCE_ID,
	“data” : {}
}
~~~

このイベントでは無効なバインド対象へのReleaseRemotePointingが通知されてもNACKは特に返答をしません